<?
include 'inicio.php';



if(Blogs::check_login() == false){
    alert("You need to be logged in");
    go_to("welcome?url=help");
}else{

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log = '$cookie' AND status = '1'");

    $user_id = $user[0]["id"];
    $user_img = $user[0]["image"];
    $username = $user[0]["user"];

    if(isset($_POST["form_ticket"])){

        $post = $_POST;
        $data = date("Y-m-d H:i:s");

        $fields = array(
            "problem_urgency" => $post["problem_urgency"],
            "problem_title" => $post["problem_title"],
            "other_problem_title" => ucfirst($post["other_problem_title"]),
            "anime_name" => ucfirst($post["anime_name"]),
            "page_name" => $post["page_name"],
            "username" => $post["username"],
            "around_time" => $post["around_time"],
            "description" => ucfirst($post["description"]),
            "created_at" => $data,
            "status" => 0,
        );
        //debug($fields);

        $add_ticket = Main::add("anime_tickets", $fields, true);

        if($add_ticket){
            $msg = "Your ticket was added and will be reviewed.";
        }else{
            $msg = "Something went wrong and your ticket wasn't added. Try again please";
        }

        ?>
        <div class="fakebanner" style="background-image: url('images/kimi2.jpg')">
            <div class="luffy wow fadeInLeft" data-wow-delay="1.5s">
                <img src="images/luffy_smiling.png" />
            </div>
            <div class="uraraka wow fadeInRight" data-wow-delay="1.5s">
                <img src="images/uraraka_smiling.png" />
            </div>
            <div class="active_label wow fadeInLeft" data-wow-delay="1s" data-toggle="tooltip" data-placement="bottom" title="What a relief!!!">Yokatta!!!</div>
        </div>

        <div class="bloco" id="help">
            <div class="container">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title"><?= $msg?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?

        $_POST["form_ticket"] = false;
        $_POST = array();
    }else{

        ?>
        <div class="fakebanner" style="background-image: url('images/kimi2.jpg')">
            <div class="luffy wow fadeInLeft" data-wow-delay="1s">
                <img src="images/luffy_crying.png" />
            </div>
            <div class="uraraka wow fadeInRight" data-wow-delay="1s">
                <img src="images/uraraka_crying.png" />
            </div>
            <div class="active_label wow fadeInUp" data-wow-delay="1.5s" data-toggle="tooltip" data-placement="bottom" title="Help me..!">Tasukete..!</div>
        </div>

        <div class="bloco" id="help">
            <div class="container">

                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title">Having trouble with something? Send us a ticket</div>
                        </div>

                        <form id="form_ticket" method="post" action="">
                            <input type="hidden" name="form_ticket" value="true" />
                            <input type="hidden" name="username" value="<?= $username?>" />

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Urgency of the problem <span class="warnings">(*)(**)</span>:</label>
                                    <select class="problem_urgency form-control" name="problem_urgency" required>
                                        <option selected disabled value="">Urgency</option>
                                        <option value="Low">Low (Something that needs fixing whenever there's time)</option>
                                        <option value="Medium">Medium (Something that needs fixing as soon as possible)</option>
                                        <option value="High">High (Something that needs fixing right away)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Select the problem in hand <span class="warnings">(*)</span>:</label>
                                    <select class="problem_title form-control" name="problem_title" required>
                                        <option selected disabled value="">Problem</option>

                                        <!--anime problems-->
                                        <option value="Can't find an anime">Can't find an anime</option>
                                        <option value="Missing details in anime detail">Missing details in anime detail</option>
                                        <option value="Typo on an anime detail">Typo on an anime detail</option>
                                        <option value="Missing prequels/sequels in anime detail">Missing prequels/sequels in anime detail</option>

                                        <!--overall problem-->
                                        <option value="Found a bug">Found a bug</option>
                                        <option value="Typo on a page">Typo on a page</option>
                                        <option value="Page/Link doesn't work">Page/Link doesn't work</option>
                                        <option value="Can't find page">Can't find page</option>
                                        <option value="Can't login">Can't login</option>
                                        <option value="Can't logout">Can't logout</option>
                                        <option value="Problem with my lists">Problem with my lists</option>

                                        <!--not listed-->
                                        <option value="Other...">Other...</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>If "Other...", describe the type:</label>
                                    <input type="text" class="form-control" name="other_problem_title" placeholder="Problem Type..." />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>If necessary, the name of the anime:</label>
                                    <input type="text" class="form-control" name="anime_name" placeholder="Anime Name..." />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>If necessary, the page that doesn't work:</label>
                                    <input type="text" class="form-control" name="page_name" placeholder="Page Name..." />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Your username <span class="warnings">(*)</span>:</label>
                                    <input disabled type="text" class="form-control" name="f_username" value="<?= $username?>" required />
                                </div>

                                <div class="col-sm-6">
                                    <label>If necessary, when it happened:</label>
                                    <input type="text" class="form-control" name="around_time" placeholder="Ex: 20-01-2019 around 04h30pm"..." />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Description of the situation <span class="warnings">(*)</span>:</label>
                                    <textarea rows="6" class="form-control" name="description" placeholder="Describe here..." required></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 center">
                                    <button type="submit">Report problem</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="contactos_response"></div>
                                </div>
                            </div>
                        </form>

                        <br>

                        <div class="col-md-12">
                            <div class="warnings">(*) - Required fields.</div>
                            <div class="warnings">(**) - Keep in mind that there's only one person (at the moment) reviewing these tickets, so, please be truthful.</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?

    }


}


include 'fim.php';
?>
