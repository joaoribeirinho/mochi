<?
include 'inicio.php';
?>

<div class="fakebanner" style="background-image: url('images/kimi2.jpg')"></div>


<?
//https://charat.me/en/

if($login == false){
    go_to("animes");
}else{

    $cookie = $_COOKIE["anime_log"];

    if($_POST["edit_profile"]){
        $post = $_POST;
        //debug($post);

        $_POST["edit_profile"] = false;
        $_POST["change_profile_pic"] = false;
        $_FILES["file_image"] = false;
        //go_to("profile");
    }

    if($_POST["change_profile_pic"]){
        $post = $_POST;
        //debug($post);

        if(isset($_FILES["file_image"])){

            $user = Blogs::get_users("key_log = '$cookie' AND status = '1'");
            $user_id = $user["id"];
            $user_img_old = $user["image"];

            if(!empty($_FILES['file_image']['name'][0])) {
                $flag_anexo_documento = true;
            }else {
                $flag_anexo_documento = false;
            }

            if($flag_anexo_documento == true) {

                // 5MB
                if ($_FILES['file_image']['size'] < 5242880) {

                    if (verifica_tipo_ext_imagem($_FILES['file_image']['type'], $_FILES['file_image']['name'])) {

                        $f_target_path = "/backoffice/images/backend/blogs/users/";
                        $imagem_ext = pathinfo(basename($_FILES['file_image']['name']), PATHINFO_EXTENSION);
                        $codigo_imagem = random_code();
                        $nome_imagem = $codigo_imagem . "." . $imagem_ext;
                        $target_path = $f_target_path . $nome_imagem;
                        //debug($target_path);

                        if (move_uploaded_file($_FILES['file_image']['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . $target_path)) {

                            unlink($user_img_old);
                            $new_image_path = $nome_imagem;

                            $fields2 = array(
                                "image" => $new_image_path,
                            );
                            $update_user = Blogs::update_user($fields2, $user_id);

                        } else{
                            $warning = "Error uploading image due to: " . $_FILES["file_image"]["error"];
                            ?>
                            <script>
                                alert("<?= $warning?>");
                            </script>
                            <?
                        }
                    } else {
                        $warning ="Image file type not allowed. (Allowed: .jpg, .png and .bmp)";
                        ?>
                        <script>
                            alert("<?= $warning?>");
                        </script>
                        <?
                    }
                } else {
                    $warning = "Image size has to be smaller than 5 MB";
                    ?>
                    <script>
                        alert("<?= $warning?>");
                    </script>
                    <?
                }
            }
        }

        $_POST["edit_profile"] = false;
        $_POST["change_profile_pic"] = false;
        $_FILES["file_image"] = false;
        go_to("profile");
    }


    //details of user
    $user_id = $user["id"];
    $user_img = $user["image"];
    $username = $user["user"];
    $email = $user["email"];
    $start_date = $user["created_at"];
    $today_date = date("Y-m-d H:i:s");

    $diff = abs(strtotime($today_date) - strtotime($start_date));
    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
    $user_age = "User age: ";

    if($years >= 1){
        if($years == 1){
            $user_age .= $years . " year, ";
        }else{
            $user_age .= $years . " years, ";
        }
    }
    if($months >= 1){
        if($months == 1){
            $user_age .= $months . " month and ";
        }else{
            $user_age .= $months . " months and ";
        }
    }
    if($days >= 1){
        if($days == 1){
            $user_age .= $days . " day!";
        }else{
            $user_age .= $days . " days!";
        }
    }else{
        $user_age .= "Started today!";
    }


    if($user_img == ""){
        $user_img_path = "images/user_unknown.png";
    }else{
        $user_img_path = Blogs::user_image_path($user_img);
    }


    //USER PREFERENCES
    $preferences = Main::get("blogs_users_preferences", "id_user = $user_id");
    $theme = $preferences[0]["theme"];
    $anime_page_type = $preferences[0]["anime_page_type"];


    ?>

    <style>
        body {
            background-color: #292929;
        }
    </style>

    <div class="bloco first_bloco">
        <div class="container">
            <div class="row anime_detail" style="color: #fff !important">

                <div class="col-sm-3 relative">
                    <form id="change_profile_pic" method="post" action="" enctype="multipart/form-data">
                        <input name="change_profile_pic" type="hidden" value="true" />

                        <a href="<?= $user_img_path?>" class="fancybox" rel="<?= $name?>">
                            <div id="profile_pic" class="image2" style="background-image: url('<?= $user_img_path?>')"></div>
                        </a>

                        <div id="file_image_clone" onclick="document.getElementById('file_image').click();">
                            <i class="fa fa-refresh" aria-hidden="true"></i> Change Img
                        </div>

                        <input type="file" style="display: none;" id="file_image" name="file_image"/>
                    </form>
                </div>

                <div class="col-sm-9">
                    <div class="anime_info">

                        <div class="name"><?= $username?></div>
                        <div class="start_date"><?= $user_age?></div>

                        <br>

                        <form id="edit_profile" method="post" action="">

                            <input name="edit_profile" type="hidden" value="true" />

                            <div class="each_field">
                                <label>Username</label>
                                <input class="form-control" type="" placeholder="Username" value="<?= $username?>" />
                            </div>

                            <br>

                            <div class="each_field">
                                <label>Email</label>
                                <input class="form-control" disabled type="" placeholder="Email" value="<?= $email?>" />
                            </div>
                            <br><br>

                            <div class="each_field">
                                <label>Themes</label>

                                <div class="row">
                                    <div class="">
                                        <div class="col-md-3 col-xs-6 each_theme_type" theme_type="default">
                                            <div class="theme_type_img default <?= ($theme == "default") ? "active" : ""?>">
                                                <img src="images/themes/mochi_default.png" />
                                            </div>
                                            <div class="theme_type_name">Default Mochi</div>
                                        </div>

                                        <div class="col-md-3 col-xs-6 each_theme_type" theme_type="durian">
                                            <div class="theme_type_img durian <?= ($theme == "durian") ? "active" : ""?>">
                                                <img src="images/themes/mochi_durian.png" />
                                            </div>
                                            <div class="theme_type_name durian">Durian Mochi</div>
                                        </div>

                                        <div class="col-md-3 col-xs-6 each_theme_type" theme_type="matcha">
                                            <div class="theme_type_img matcha <?= ($theme == "matcha") ? "active" : ""?>">
                                                <img src="images/themes/mochi_matcha.png" />
                                            </div>
                                            <div class="theme_type_name matcha">Matcha Mochi</div>
                                        </div>

                                        <div class="col-md-3 col-xs-6 each_theme_type" theme_type="redbeanpaste">
                                            <div class="theme_type_img redbeanpaste <?= ($theme == "redbeanpaste") ? "active" : ""?>">
                                                <img src="images/themes/mochi_redbeanpaste.png" />
                                            </div>
                                            <div class="theme_type_name redbeanpaste">RedBeanPaste Mochi</div>
                                        </div>

                                        <div class="col-md-3 col-xs-6 each_theme_type" theme_type="sesame">
                                            <div class="theme_type_img sesame <?= ($theme == "sesame") ? "active" : ""?>">
                                                <img src="images/themes/mochi_sesame.png" />
                                            </div>
                                            <div class="theme_type_name sesame">Sesame Mochi</div>
                                        </div>

                                        <div class="col-md-3 col-xs-6 each_theme_type" theme_type="iris">
                                            <div class="theme_type_img iris <?= ($theme == "iris") ? "active" : ""?>">
                                                <img src="images/themes/mochi_iris.png" />
                                            </div>
                                            <div class="theme_type_name iris">Iris Mochi</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br><br>

                            <div class="each_field">
                                <label>Anime Page Type</label>

                                <div class="row">
                                    <div class="">
                                        <div class="col-md-6 each_anime_page_type" page_type="1">
                                            <a href="images/body_no_back.png" class="fancybox">
                                                <div class="anime_page_type_img <?= ($anime_page_type == "1") ? "active" : ""?>" style="background-image: url('images/body_no_back.png')"></div>
                                            </a>

                                            <div class="anime_page_type_name">Banner</div>
                                        </div>

                                        <div class="col-md-6 each_anime_page_type" page_type="2">
                                            <a href="images/body_back.png" class="fancybox">
                                                <div class="anime_page_type_img <?= ($anime_page_type == "2") ? "active" : ""?>" style="background-image: url('images/body_back.png')"></div>
                                            </a>
                                            <div class="anime_page_type_name">Full Body Cover</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br>


                            <input type="hidden" name="theme_type" value="<?= $theme?>" />
                            <input type="hidden" name="anime_page_type" value="<?= $anime_page_type?>" />

                            <br>

                            <div class="each_field right">
                                <button type="submit">Save Changes</button>
                            </div>

                        </form>

                        <div id="target"></div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <?

}

?>




<?
include 'fim.php';
?>
