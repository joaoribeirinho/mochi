<?
include 'inicio.php';

$id_url = $_GET["anime"];
if($id_url){
    if(Blogs::check_login() == true){
        $login = true;

        $anime = Pages::get_by_id($id_url);
        //debug($anime);
        $id_mal = $anime["id_mal"];
        $name_romanji = $anime["name"];
        $name_english = $anime["name_english"];
        $name_original = $anime["name_original"];

        $total_eps = $anime["nb_eps"];

        $start_date = $anime["start_date"];
        $end_date = $anime["end_date"];

        $season = $anime["season"];
        $season_of_year = end(explode("]", $season));
        $year = reset(explode("-[", $season));

        $anime_type = $anime["type"];

        $studios_of_anime = explode(";", $anime["studios_id"]);
        $genres_of_anime = explode(";", $anime["genres_id"]);

        $studios_of_anime_all = $anime["studios_id"];
        $studios_of_anime_all = "[" . str_replace(";", "][", $anime["studios_id"]) . "]";
        $genres_of_anime_all = $anime["genres_id"];
        $genres_of_anime_all = "[" . str_replace(";", "][", $anime["genres_id"]) . "]";

        $image = Pages::get_image($id_url,"description ASC");
        $image_path = $image[0]["image"];

        $get_banner = Banners::get("link = $id_url AND status = 1");
        if($get_banner){
            $id = $get_banner[0]["id"];
            $banner = Banners::get_image($id);
            $banner = Banners::image_path($banner[0]["image"]);
            $source = Main::get("additional_contents", "additional_id = 1 AND module_content_id = $id");
            $link_source = $source[0]["content"];
        }else{
            $banner = "images/kimi2.jpg";
        }



        //details of user
        $cookie = $_COOKIE["anime_log"];
        $user = Blogs::get_users("key_log = '$cookie' AND status = '1'");
        $user_id = $user[0]["id"];

        //get preferences
        $get_preferences = Main::get("blogs_users_preferences", "id_user = $user_id");

        $anime_page_type = $get_preferences[0]["anime_page_type"];
        if($anime_page_type == 2){
            ?>
            <script>
                $(document).ready(function(){
                    $("body").css("background-image", "url(<?= $banner?>)");
                });
            </script>
            <style>
                .anime_detail{
                    background-color: #d0d0d0;
                }
            </style>
            <?
        }


        $link_nyaa = "https://nyaa.si/?f=0&c=0_0&q=" . str_replace(" ", "+", $name) . "+1080";
        $link_filler_list = "https://www.animefillerlist.com/shows/" . str_replace(" ", "-", strtolower(str_replace(":", "", $name)));
    }else{
        go_to("animes");
    }






}else{
    go_to("animes");
}

if($link_source){
    ?>
    <!--Image Source:: <?= $link_source?> -->
    <?
}


?>

<div class="fakebanner" style="background-image: url('<?= $banner?>')"></div>

<div class="bloco first_bloco">
    <div class="container">

        <form id="edit_submission" method="post" action="">

            <div class="row anime_detail">

                <div class="col-sm-3 no_hover">
                    <a href="<?= $image_path?>" class="fancybox" rel="<?= $name?>">
                        <div class="image2" style="background-image: url('<?= $image_path?>')"></div>
                    </a>
                    <br>

                </div>

                <div class="col-sm-9">
                    <div style="margin-top: 58px;">

                        <?/*Romanji*/?>
                        <div class="row each_row">
                            <div class="col-md-12">
                                <label>Name - Romanji</label>
                                <input type="text" class="form-control" name="name_romanji" placeholder="Romanji name here..." value="<?= $name_romanji?>" />
                            </div>
                        </div>

                        <?/*English*/?>
                        <div class="row each_row">
                            <div class="col-md-12">
                                <label>Name - English</label>
                                <input type="text" class="form-control" name="name_english" placeholder="English name here..." value="<?= $name_english?>" />
                            </div>
                        </div>

                        <?/*Original*/?>
                        <div class="row each_row">
                            <div class="col-md-12">
                                <label>Name - Original</label>
                                <input type="text" class="form-control" name="name_original" placeholder="Original name here..." value="<?= $name_original?>" />
                            </div>
                        </div>

                        <?/*Type 7 Eps*/?>
                        <div class="row each_row">
                            <div class="col-md-6">
                                <label>Anime Type</label>
                                <select class="problem_title form-control" name="anime_type" required>
                                    <option <?= ($anime_type == "") ? "selected" : ""?> disabled value="">Anime Type</option>
                                    <option <?= ($anime_type == "Movie") ? "selected" : ""?> value="Movie">Movie</option>
                                    <option <?= ($anime_type == "ONA") ? "selected" : ""?> value="ONA">ONA</option>
                                    <option <?= ($anime_type == "OVA") ? "selected" : ""?> value="OVA">OVA</option>
                                    <option <?= ($anime_type == "Special") ? "selected" : ""?> value="Special">Special</option>
                                    <option <?= ($anime_type == "TV") ? "selected" : ""?> value="TV">TV</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label>Number of Episodes</label>
                                <input type="text" class="form-control" name="total_eps" placeholder="Number of Episodes" value="<?= $total_eps?>" />
                            </div>
                        </div>

                        <?/*Start & End Dates*/?>
                        <div class="row each_row">
                            <div class="col-md-6">
                                <label>Start Date</label>
                                <input type="text" class="form-control" name="start_date" placeholder="Start Date" value="<?= $start_date?>" />
                            </div>

                            <div class="col-md-6">
                                <label>End Date</label>
                                <input type="text" class="form-control" name="end_date" placeholder="End Date" value="<?= $end_date?>" />
                            </div>
                        </div>

                        <?/*Season*/?>
                        <div class="row each_row">
                            <div class="col-md-6">
                                <label>Season</label>
                                <select class="problem_title form-control" name="season_of_year" required>
                                    <option <?= ($season_of_year == "") ? "selected" : ""?> disabled value="">Season</option>
                                    <option <?= ($season_of_year == "Winter") ? "selected" : ""?> value="Winter">Winter</option>
                                    <option <?= ($season_of_year == "Spring") ? "selected" : ""?> value="Spring">Spring</option>
                                    <option <?= ($season_of_year == "Summer") ? "selected" : ""?> value="Summer">Summer</option>
                                    <option <?= ($season_of_year == "Fall") ? "selected" : ""?> value="Fall">Fall</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label>Year</label>
                                <input type="text" class="form-control" name="year" placeholder="Year" value="<?= $year?>" />
                            </div>
                        </div>

                        <?/*Images*/?>
                        <div class="row each_row">
                            <div class="col-md-6">
                                <label>Image - Cover</label>
                                <input type="text" class="form-control" name="img_cover" placeholder="URL link here..." />
                            </div>

                            <div class="col-md-6">
                                <label>Image - Banner</label>
                                <input type="text" class="form-control" name="img_banner" placeholder="URL link here..." />
                            </div>
                        </div>

                        <?/*Studios*/?>
                        <div class="row each_row">
                            <div class="col-md-12">
                                <label>Studios</label>
                                <input type="hidden" name="studios_of_anime_all" value="<?= $studios_of_anime_all?>" />
                                <br>

                                <div id="studios">
                                    <?
                                    foreach($studios_of_anime as $studio){
                                        if($studio != ""){
                                            $get_studio_name = Main::get("anime_studios", "id = $studio");
                                            if($get_studio_name){
                                                $each_studio_name = $get_studio_name[0]["name"];
                                                ?>
                                                <div class="studio">
                                                    <?= $each_studio_name?>
                                                    <i class="fa fa-close redbeanpaste remove_studio" aria-hidden="true" studio="[<?= $studio["id"]?>]" data-toggle="tooltip" data-placement="top" title="Remove Studio"></i>
                                                </div>
                                                <?
                                            }
                                        }
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>

                        <div class="row each_row list_table" id_list="table_id_1">
                            <div class="col-md-12">
                                <table id="table_id_1" data-order='[[ 1, "asc" ]]' class="table_id_datatables_submissions durian responsive">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Add</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?
                                    $studios = Main::get("anime_studios");
                                    foreach($studios as $studio){
                                        ?>
                                        <tr class="row_anime" studio="[<?= $studio["id"]?>]">
                                            <td><?= $studio["id"]?></td>

                                            <td><?= $studio["name"]?></td>

                                            <td class="actions">
                                                <i class="fa fa-plus-circle durian add_studio" aria-hidden="true" studio="[<?= $studio["id"]?>]" data-toggle="tooltip" data-placement="left" title="Add Studio"></i>
                                            </td>

                                        </tr>
                                        <?
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <?/*Genres*/?>
                        <div class="row each_row">
                            <div class="col-md-12">
                                <label>Genres</label>
                                <input type="hidden" name="genres_of_anime_all" value="<?= $genres_of_anime_all?>" />
                                <br>

                                <div id="genres">
                                    <?
                                    foreach($genres_of_anime as $genre){
                                        if($genre != ""){
                                            $get_genre_name = Main::get("anime_genres", "id = $genre");
                                            if($get_genre_name){
                                                $each_genre_name = $get_genre_name[0]["name"];
                                                ?>
                                                <div class="genre">
                                                    <?= $each_genre_name?>
                                                    <i class="fa fa-close redbeanpaste remove_genre" aria-hidden="true" genre="[<?= $genre["id"]?>]" data-toggle="tooltip" data-placement="top" title="Remove Genre"></i>
                                                </div>
                                                <?
                                            }
                                        }
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>

                        <div class="row each_row list_table" id_list="table_id_2">
                            <div class="col-md-12">
                                <table id="table_id_2" data-order='[[ 1, "asc" ]]' class="table_id_datatables_submissions iris responsive">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Add</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?
                                        $genres = Main::get("anime_genres");
                                        foreach($genres as $genre){
                                            ?>
                                            <tr class="row_anime" studio="[<?= $genre["id"]?>]">
                                                <td><?= $genre["id"]?></td>

                                                <td><?= $genre["name"]?></td>

                                                <td class="actions">
                                                    <i class="fa fa-plus-circle iris add_genre" aria-hidden="true" genre="[<?= $genre["id"]?>]" data-toggle="tooltip" data-placement="left" title="Add Genre"></i>
                                                </td>

                                            </tr>
                                            <?
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>



                        <br>
                        <br>
                        <br>




                        <?
                        $prequels = Main::get("additional_contents", "additional_id = 2 AND module_content_id = $id_url");
                        $prequels = $prequels[0]["content"];
                        $prequels = explode(";", $prequels);

                        $sequels = Main::get("additional_contents", "additional_id = 3 AND module_content_id = $id_url");
                        $sequels = $sequels[0]["content"];
                        $sequels = explode(";", $sequels);

                        $others = Main::get("additional_contents", "additional_id = 4 AND module_content_id = $id_url");
                        $others = $others[0]["content"];
                        $others = explode(";", $others);

                        if($prequels[0] != "" || $sequels[0] != "" || $others[0] != ""){
                            ?>
                            <div class="similar_animes">
                                <div class="title">Related Animes</div>
                                <br>

                                <div class="row">
                                    <?
                                    $related_label = "Prequel";
                                    foreach($prequels as $prequel){
                                        $prequel = str_replace("[", "", $prequel);
                                        $prequel = str_replace("]", "", $prequel);
                                        if($prequel != ""){
                                            $anm = Pages::get_by_id($prequel);
                                            $other_image = Pages::get_image($prequel,"description ASC");
                                            $other_image_path = $other_image[0]["image"];
                                            switch($anm["type"]) {
                                                case "TV":
                                                    $class = "matcha_t";
                                                    break;
                                                case "ONA":
                                                    $class = "durian_t";
                                                    break;
                                                case "OVA":
                                                    $class = "sesame_t";
                                                    break;
                                                case "Special":
                                                    $class = "redbeanpaste_t";
                                                    break;
                                                case "Movie":
                                                    $class = "iris_t";
                                                    break;
                                                default:
                                                    $class = "matcha_t";
                                            }
                                            ?>
                                            <div class="col-md-3">
                                                <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                    <a href="anime?id=<?= $anm["id"]?>">
                                                        <div class="related_label"><?= $related_label?></div>
                                                        <div class="other_overlay">
                                                            <div class="other_name">
                                                                <span><?= $anm["name"]?></span>
                                                                <br><br>
                                                                <span class="other_type <?= $class?>"><?= $anm["type"]?></span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <?
                                        }
                                    }

                                    $related_label = "Sequel";
                                    foreach($sequels as $sequel){
                                        $sequel = str_replace("[", "", $sequel);
                                        $sequel = str_replace("]", "", $sequel);
                                        if($sequel != ""){
                                            $anm = Pages::get_by_id($sequel);
                                            $other_image = Pages::get_image($sequel,"description ASC");
                                            $other_image_path = $other_image[0]["image"];
                                            switch($anm["type"]) {
                                                case "TV":
                                                    $class = "matcha_t";
                                                    break;
                                                case "ONA":
                                                    $class = "durian_t";
                                                    break;
                                                case "OVA":
                                                    $class = "sesame_t";
                                                    break;
                                                case "Special":
                                                    $class = "redbeanpaste_t";
                                                    break;
                                                case "Movie":
                                                    $class = "iris_t";
                                                    break;
                                                default:
                                                    $class = "matcha_t";
                                            }
                                            ?>
                                            <div class="col-md-3">
                                                <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                    <a href="anime?id=<?= $anm["id"]?>">
                                                        <div class="related_label"><?= $related_label?></div>
                                                        <div class="other_overlay">
                                                            <div class="other_name">
                                                                <span><?= $anm["name"]?></span>
                                                                <br><br>
                                                                <span class="other_type <?= $class?>"><?= $anm["type"]?></span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <?
                                        }
                                    }

                                    $related_label = "Others";
                                    foreach($others as $other){
                                        $other = str_replace("[", "", $other);
                                        $other = str_replace("]", "", $other);
                                        if($other != ""){
                                            $anm = Pages::get_by_id($other);
                                            $other_image = Pages::get_image($other,"description ASC");
                                            $other_image_path = $other_image[0]["image"];
                                            switch($anm["type"]) {
                                                case "TV":
                                                    $class = "matcha_t";
                                                    break;
                                                case "ONA":
                                                    $class = "durian_t";
                                                    break;
                                                case "OVA":
                                                    $class = "sesame_t";
                                                    break;
                                                case "Special":
                                                    $class = "redbeanpaste_t";
                                                    break;
                                                case "Movie":
                                                    $class = "iris_t";
                                                    break;
                                                default:
                                                    $class = "matcha_t";
                                            }
                                            ?>
                                            <div class="col-md-3">
                                                <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                    <a href="anime?id=<?= $anm["id"]?>">
                                                        <div class="related_label"><?= $related_label?></div>
                                                        <div class="other_overlay">
                                                            <div class="other_name">
                                                                <span><?= $anm["name"]?></span>
                                                                <br><br>
                                                                <span class="other_type <?= $class?>"><?= $anm["type"]?></span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <?
                                        }
                                    }

                                    /*$two_piece_name = $name;
                                    $two_piece_name = explode(" ", $two_piece_name);
                                    $two_piece_name = $two_piece_name[0] . " " . $two_piece_name[1];

                                    $similar = Pages::get("name LIKE '%$two_piece_name%' AND status = 1", "start_date ASC");
                                    foreach ($similar as $other){
                                        if($other["id"] != $id_url){
                                            $other_image = Pages::get_image($other["id"],"description ASC");
                                            $other_image_path = $other_image[0]["image"];

                                            switch($other["type"]) {
                                                case "TV":
                                                    $class = "matcha_t";
                                                    break;
                                                case "ONA":
                                                    $class = "durian_t";
                                                    break;
                                                case "OVA":
                                                    $class = "sesame_t";
                                                    break;
                                                case "Special":
                                                    $class = "redbeanpaste_t";
                                                    break;
                                                case "Movie":
                                                    $class = "iris_t";
                                                    break;
                                                default:
                                                    $class = "matcha_t";
                                            }

                                            ?>
                                            <div class="col-md-3">
                                                <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                    <a href="anime?id=<?= $other["id"]?>">
                                                        <div class="other_overlay">
                                                            <div class="other_name">
                                                                <span><?= $other["name"]?></span>
                                                                <br><br>
                                                                <span class="other_type <?= $class?>"><?= $other["type"]?></span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <?

                                        }
                                    }*/
                                    ?>
                                </div>
                            </div>
                            <br>
                            <?
                        }
                        ?>


                        <div id="target"></div>
                    </div>
                </div>

            </div>

        </form>



    </div>
</div>






<?
include 'fim.php';
?>



