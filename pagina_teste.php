<?
include 'inicio.php';
?>

<div class="bloco">
    <div class="container">

        <br>
        <?
        $data = date("Y-m-d H:i:s");

        /********************/
        /*  GET ALL GENRES  */
        /********************/

        /*
        $html_genres = file_get_html("files_to_scrape/genres.html");
        $myGenres = array();

        foreach($html_genres->find("div.genre-list.al a.genre-name-link") as $index => $element){

            $genre_id_mal = $element->href;
            $genre_id_mal = str_replace("/anime/genre/", "", $genre_id_mal);
            $genre_id_mal = reset(explode("/", $genre_id_mal));

            $genre_name = $element->plaintext;
            $genre_name = reset(explode(" (", $genre_name));
            $genre_name = trim($genre_name);

            //$anime["id"] = $i;
            $genre["genre_name"] = $genre_name;
            $genre["genre_id_mal"] = $genre_id_mal;

            array_push($myGenres, $genre);

            $fields = array(
                    "id_mal" => $genre_id_mal,
                    "name" => $genre_name,
                    "created_at" => $data,
            );

            //$add = Main::add("anime_genres", $fields, true);


        }//debug($genre);
        //debug("added genres");
        */

        /*********************/
        /*  GET ALL STUDIOS  */
        /*********************/

        /*
        $html_studios = file_get_html("files_to_scrape/studios.html");
        $myStudios = array();

        foreach($html_studios->find("div.genre-list.al a.genre-name-link") as $index => $element){

            $studio_id_mal = $element->href;
            $studio_id_mal = str_replace("/anime/producer/", "", $studio_id_mal);
            $studio_id_mal = reset(explode("/", $studio_id_mal));

            $studio_name = $element->plaintext;
            $studio_name = reset(explode(" (", $studio_name));
            $studio_name = trim($studio_name);

            //$anime["id"] = $i;
            $studio["studio_name"] = $studio_name;
            $studio["studio_id_mal"] = $studio_id_mal;

            array_push($myStudios, $studio);

            $fields = array(
                "id_mal" => $studio_id_mal,
                "name" => $studio_name,
                "created_at" => $data,
            );

            $add = Main::add("anime_studios", $fields, true);

        }//debug($studio);
        debug("added studios");
        */

        /********************/
        /*  GET ALL ANIMES  */
        /********************/

        // add classes to elements
        // qwer

        // new_anime
        // continuing_anime
        // ona_anime
        // ova_anime
        // movie_anime
        // special_anime

        $html = file_get_html("files_to_scrape/import.html");
        $anime_season = "Fall 1988";
        $can_add = "no";


        $myAnimes = array();
        $i = -1;
        $b = 0;
        foreach($html->find("div.clearfix .seasonal-anime.js-seasonal-anime") as $index => $element){

            //region DATA
            $anime_link = $element->find("a.link-title", 0)->href;
            $anime_id = end(explode("https://myanimelist.net/anime/", $anime_link));
            $anime_id = reset(explode("/", $anime_id));

            $anime_studio_id_mal = $element->find("span.producer a", 0)->href;
            $anime_studio_id_mal = str_replace("/anime/producer/", "", $anime_studio_id_mal);
            $anime_studio_id_mal = reset(explode("/", $anime_studio_id_mal));

            $anime_image = $element->find("div.image img", 0)->src;

            if($anime_image == ""){
                $anime_image = $element->find("div.image img", 0)->attr['data-src'];
            }

            //https://cdn.myanimelist.net/r/167x242/images/qm_50.gif?s=ae2bf25a5159e965ebb3aa7f9442fa80
            if (strpos($anime_image, 'qm_50.gif') !== false) {
                $anime_image = null;
            }

            $anime_start_date = $element->find("span.remain-time", 0)->plaintext;
            $anime_start_date = explode(", ", $anime_start_date);
            $anime_start_date = $anime_start_date[0] . ", " . $anime_start_date[1];
            $anime_start_date = trim($anime_start_date);

            $date = $anime_start_date;
            $date = explode(" ", $date);
            $month = $date[0];
            $day = str_replace(",", "", $date[1]);
            $year = $date[2];

            switch($month){
                case "Jan";
                    $new_date = $year . "-01-" . $day;
                    break;
                case "Feb";
                    $new_date = $year . "-02-" . $day;
                    break;
                case "Mar";
                    $new_date = $year . "-03-" . $day;
                    break;
                case "Apr";
                    $new_date = $year . "-04-" . $day;
                    break;
                case "May";
                    $new_date = $year . "-05-" . $day;
                    break;
                case "Jun";
                    $new_date = $year . "-06-" . $day;
                    break;
                case "Jul";
                    $new_date = $year . "-07-" . $day;
                    break;
                case "Aug";
                    $new_date = $year . "-08-" . $day;
                    break;
                case "Sep";
                    $new_date = $year . "-09-" . $day;
                    break;
                case "Oct";
                    $new_date = $year . "-10-" . $day;
                    break;
                case "Nov";
                    $new_date = $year . "-11-" . $day;
                    break;
                case "Dec";
                    $new_date = $year . "-12-" . $day;
                    break;
                default;
                    $new_date = $anime_start_date;
                    break;
            }

            $anime_type = $element->find("div.info", 0)->plaintext;
            $anime_type = reset(explode(" - ", $anime_type));
            $anime_type = trim($anime_type);

            $anime_name = $element->find("a.link-title", 0)->plaintext;
            $anime_genres_id = $element->attr['data-genre'];
            $anime_genres_id = explode(",", $anime_genres_id);

            $all_genres_id = "";
            foreach($anime_genres_id as $each_genres_id){
                $get_genre = Main::get("anime_genres", "id_mal = '$each_genres_id'");
                $genre_id = $get_genre[0]["id"];
                $all_genres_id .= $genre_id . ";";
            }

            $anime_genres_id = str_replace(",", ";", $anime_genres_id);
            $anime_genres_id .= ";";

            $anime_eps = $element->find("div.prodsrc div.eps", 0)->plaintext;
            $anime_eps = trim(str_replace(" eps", "", $anime_eps));

            if($anime_eps == "?"){
                $anime_eps = "Unknown";
            }

            $studios = $element->find("span.producer", 0)->plaintext;
            $studios = str_replace("          -", "", $studios);
            $studios = str_replace("'", "\'", $studios);
            $studios = str_replace("&", "&amp;", $studios);

            if($studios == "-"){
                $studios = "";
            }

            if (strpos($studios, ', ') !== false) {
                $studios = explode(", ", $studios);
                $all_studio_id = "";

                foreach($element->find("span.producer a") as $studio){
                    $studio_id_mal = $studio->href;
                    $studio_id_mal = str_replace("/anime/producer/", "", $studio_id_mal);
                    $studio_id_mal = reset(explode("/", $studio_id_mal));

                    $get_studio = Main::get("anime_studios", "id_mal = $studio_id_mal");
                    if($get_studio){
                        $studio_id = $get_studio[0]["id"];
                        $all_studio_id .= $studio_id . ";";
                    }
                }

            }else{
                if($studios != ""){
                    $get_studio = Main::get("anime_studios", "id_mal = $anime_studio_id_mal");
                    if($get_studio){
                        $all_studio_id = $get_studio[0]["id"];
                    }
                }else{
                    $all_studio_id = "";
                }


            }

            $season = explode(" ", $anime_season);
            $season_of_year = $season[0];
            $year = $season[1];

            switch($season_of_year){
                case "Winter";
                    $new_season = $year . "-[A]" . $season_of_year;
                    break;
                case "Spring";
                    $new_season = $year . "-[B]" . $season_of_year;
                    break;
                case "Summer";
                    $new_season = $year . "-[C]" . $season_of_year;
                    break;
                case "Fall";
                    $new_season = $year . "-[D]" . $season_of_year;
                    break;
                default;
                    $new_season = $anime_season;
                    break;
            }

            $anime_name = mal_weird_char($anime_name);
            $anime_name2 = str_replace("'", "\'", $anime_name);
            $check_if_already_exists = Pages::get("name = '$anime_name2'");
            //endregion DATA

            if(!$check_if_already_exists){
                $fields = array(
                    "id_category" => 1,
                    "name" => $anime_name,
                    "id_mal" => $anime_id,
                    "genres_id" => $all_genres_id,
                    "studios_id" => $all_studio_id,
                    "start_date" => $new_date,
                    "season" => $new_season,
                    "nb_eps" => $anime_eps,
                    "type" => $anime_type,
                    "created_at" => $data,
                );
                //debug($fields);
                if($can_add == "yes"){
                    $add = Pages::add($fields, true);
                }

                $fields_image = array(
                    "id_page" => $add,
                    "image" => $anime_image,
                    "description" => "profile",
                    "created_at" => $data,
                );
                //debug($fields_image);
                if($can_add == "yes" && $anime_image != null){
                    $add_image = Pages::add_image($fields_image, true);
                }

                $i++;
            }else{

                $fields2 = array(
                    "name" => $anime_name,
                    "season/type" => $anime_season . " / " . $anime_type,
                    "created_at" => $data,
                );
                debug($fields2);

                $b++;
            }

            //debug($fields);

            //debug($anime);
        }//debug($anime);


        debug( "Checked: " . $index . "   ->(plus 1)");
        debug( "Added: " . $i . "   ->(plus 1)");
        debug( "Not Added: " . $b);



        ?>


    </div>
</div>


<?
include 'fim.php';
?>



