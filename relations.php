<?
include 'inicio.php';

$id_url = $_GET["id"];
if($id_url){
    $anime = Pages::get_by_id($id_url);
    $id_mal = $anime["id_mal"];
    $name = $anime["name"];


    $get_banner = Banners::get("link = $id_url AND status = 1");
    if($get_banner){
        $id = $get_banner[0]["id"];
        $banner = Banners::get_image($id);
        $banner = Banners::image_path($banner[0]["image"]);
        $source = Main::get("additional_contents", "additional_id = 1 AND module_content_id = $id");
        $link_source = $source[0]["content"];
    }else{
        $banner = "images/kimi2.jpg";
    }

}else{
    go_to("animes");
}

if($link_source){
    ?>
    <!--Image Source:: <?= $link_source?> -->
    <?
}





?>

<div class="fakebanner" style="background-image: url('<?= $banner?>')">
    <div class="update_msg"><?= $name?></div>
</div>

<div class="bloco first_bloco" style="overflow:unset;">
    <div class="container">
        <div class="">
            <form id="get_animes_form" method="post" action="">
                <input id="get_animes" iu="<?= $id_url?>" name="get_animes" type="text" placeholder="Search By...">
            </form>
        </div>

        <div class="row">
            <div id="get_animes_target">
                <?
                if(isset($_GET["s"])){
                    $search = urldecode($_GET["s"]);

                    $get_animes = Pages::get("status = 1 AND id_category = 1 AND name LIKE '%$search%'", "name ASC");
                    foreach($get_animes as $anime){
                        $name = $anime["name"];
                        $big_name = $anime["name"];
                        if(strlen($name) > 42){
                            $name = substr($name,0, 42) . "...";
                        }

                        $id = $anime["id"];

                        $image = Pages::get_image($anime["id"],"description ASC");
                        $image_path = $image[0]["image"];

                        $check = Main::get("additional_contents", "module_content_id = $id_url AND content LIKE '%[$id];%'");
                        if($check){
                            if($check[0]["additional_id"] == 2){
                                $relations_class = "is_prequel";
                            }elseif($check[0]["additional_id"] == 3){
                                $relations_class = "is_sequel";
                            }elseif($check[0]["additional_id"] == 4){
                                $relations_class = "is_other";
                            }
                        }else{
                            $relations_class = "";
                        }

                        ?>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <div class="each_holder <?= $relations_class?>" ia="<?= $anime["id"]?>">
                                <div class="cover" style="background-image: url('<?= $image_path?>')"></div>
                                <div class="name" title="<?= $big_name?>"><?= $name?></div>
                            </div>
                        </div>
                        <?
                    }
                }
                ?>
            </div>
        </div>

        <br><br>

        <div id="target"></div>

        <div class="row btn_relations_actions">
            <div class="col-md-4">
                <button class="btn_relations add_prequels" add_type="prequels" ia="<?= $id_url?>">Add Prequels</button>
            </div>

            <div class="col-md-4">
                <button class="btn_relations add_sequels" add_type="sequels" ia="<?= $id_url?>">Add Sequels</button>
            </div>

            <div class="col-md-4">
                <button class="btn_relations add_others" add_type="others" ia="<?= $id_url?>">Add Others</button>
            </div>
        </div>






    </div>
</div>




<?
include 'fim.php';
?>



