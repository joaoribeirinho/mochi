<?
include 'inicio.php';
?>

<div class="fakebanner" style="background-image: url('images/kimi2.jpg')"></div>


<div class="bloco">
    <div class="container">
        <div class="col-md-12 center">
            <h1>:: ANIME LIST PROJECT ::</h1>
        </div>
        <br>

        <?
        $icons = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>
            <i class=\"fa fa-close\" aria-hidden=\"true\"></i>
            <i class=\"fa fa-exclamation\" aria-hidden=\"true\"></i>
            <i class=\"fa fa-pause\" aria-hidden=\"true\"></i>";
        ?>

        <style>
            .collap h3{
                cursor: pointer;
                margin-top: 20px;
                margin-bottom: 0;
                border-bottom: 2px solid #9a9a9a;
            }

            .collap ul{
                margin-top: 20px;
            }
            .collap li{
                position: relative;
                width: fit-content;
                border-radius: 5px;
                margin: 4px 0;
                padding: 4px 30px;
            }
            .collap li:hover{
                background-color: #ffffff4d;
            }
            .collap li i{
                display: none;
                position: absolute;
                left: 10px;
                top: 7px;
                color: #000 !important;
            }

            .collap li.done{
                color: green;
                background-color: #81ec8185;
            }
            .collap li.done i.fa-check{
                display: block;
            }

            .collap li.impossible{
                color: red;
                background-color: #d49e9e70;
            }
            .collap li.impossible i.fa-close{
                display: block;
            }

            .collap li.incomplete{
                color: yellow;
                background-color: #bfbfa5cc;
            }
            .collap li.incomplete i.fa-pause{
                display: block;
            }

            .collap li.important{
                color: white;
                background-color: #bfbfa5cc;
            }
            .collap li.important i.fa-exclamation{
                display: block;
            }
        </style>

        <div class="col-md-12">

            <div class="collap">
                <h3 target="usable_links">#usable links</h3>
                <ul target="usable_links">
                    <li class="">https://anilist.co/edit/anime/new (form to add anime to list. get idea of the fields needed) <?= $icons?></li>
                    <li class="important">https://charat.me/en/ (create an avatar) <?= $icons?></li>
                    <li class="">https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage <?= $icons?></li>
                </ul>
            </div>
            <br>

            <div class="collap">
                <h3 target="ideas_for_themes">#ideas for themes</h3>
                <ul target="ideas_for_themes">
                    <li class="done">create logos for themes and choose their own colors (matcha/sesame/red bean paste/durian/iris) <?= $icons?></li>
                    <li class="important">themes based on mochi flavours/types <?= $icons?></li>
                </ul>
            </div>
            <br>

            <div class="collap">
                <h3 target="general_thoughts">#general thoughts</h3>
                <ul target="general_thoughts">
                    <li class="done">criar tabela BD para associar as definicoes de perfil a um user atraves do ID <?= $icons?></li>
                    <li class="important">no sign up de um user, adicionar o recaptcha just in case <?= $icons?></li>
                    <li class="">user preferences - store in local storage of browser with .js - localStorage.setItem('key', 'value') <?= $icons?></li>
                    <li class="">premium features for subscribers; <?= $icons?></li>
                    <li class="incomplete">users win points and there will be pols to see who has more, maybe win something?? <?= $icons?></li>
                    <li class="done">bootstrap popover <?= $icons?></li>
                    <li class="done">trocar ajax por links nas tabs de my lists <?= $icons?></li>
                    <li class="done">every friday check anime detail to be updated. only update once per friday <?= $icons?></li>
                    <li class="done">buttons for external links <?= $icons?></li>
                    <li class="done">change colors of studio/genres tags on page anime.php <?= $icons?></li>
                    <li class="done">homepage need to have a feed of the latest feedbacks posted <?= $icons?></li>
                    <li class="done">homepage need to have a feed of the latest updated animes <?= $icons?></li>
                    <li class="">page to see all feedbacks from a certain user?? <?= $icons?></li>
                    <li class="incomplete">simple footer with links that are not on the menu-header <?= $icons?></li>
                    <li class="important">paint white the menu-hamburger toggle <?= $icons?></li>
                    <li class="important">when mobile, profile link on menu-header should be like the link "my_lists" <?= $icons?></li>
                    <li class="important">when mobile, on menu-header, links like "my_lists" should have with: fit-content <?= $icons?></li>
                    <li class="important">get some ideas for the "schedule page" <?= $icons?></li>
                    <li class="">develop chrome extension -> .js Web Worker ??? <?= $icons?></li>
                </ul>
            </div>
            <br>

            <div class="collap">
                <h3 target="points_rules">#points rules</h3>
                <ul target="points_rules">
                    <li class="done">add a comment = (+)2 points <?= $icons?></li>
                    <li class="done">remove a comment = (-)2 points <?= $icons?></li>
                    <li class="done">removing a comment will take back the points you got by posting it, but the points you got for the likes will be kept <?= $icons?></li>
                    <li class="done">like a comment = (+)1 point <?= $icons?></li>
                    <li class="done">remove like from comment = (-)1 point <?= $icons?></li>
                    <li class="incomplete">you can not like your own post (this will prevent cheating) <?= $icons?></li>
                    <li class="">points for opening an anime for the first time = (+)2 points <?= $icons?></li>
                    <li class="">points for updating an anime = (+)1 points <?= $icons?></li>
                </ul>
            </div>
            <br>


        </div>
    </div>
</div>





<?
include 'fim.php';
?>





