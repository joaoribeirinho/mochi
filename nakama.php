<?
include 'inicio.php';

if(!isset($_GET["id"])){
    go_to("home");
}else{

    //details of user
    $user_id = $_GET["id"];
    $user = Blogs::get_users("id = $user_id AND status = '1'");
    $username = $user[0]["user"];
    $user_img = $user[0]["image"];
    $user_img_path = Blogs::user_image_path($user_img);

    if($_GET["list"]){
        $active_list_id = $_GET["list"];
    }else{
        $active_list_id = "1";
    }

    ?>
    <div class="fakebanner" style="background-image: url('images/kimi2.jpg')">
        <div class="nakama_face wow fadeInRight" data-wow-delay="1.5s">
            <img src="<?= $user_img_path?>" />
        </div>
        <div class="active_label"><?= $username?></div>
    </div>

    <div class="bloco first_bloco">
        <div class="container">
            <div class="row">
                <input type="hidden" id="nakama" value="<?= $user_id?>">
                <div class="lists_names nakama">
                    <?
                    $get_lists = Main::get("anime_lists");
                    foreach($get_lists as $list){
                        $list_id = $list["id"];
                        $list_name = $list["name"];

                        switch ($list["id"]){
                            case "1":
                                $bg = "matcha";
                                break;
                            case "2":
                                $bg = "durian";
                                break;
                            case "3":
                                $bg = "iris";
                                break;
                            case "4":
                                $bg = "redbeanpaste";
                                break;
                            default:
                                $bg = "matcha";
                                break;
                        }

                        if($active_list_id == $list_id){
                            $active = "active";
                        }else{
                            $active = "";
                        }

                        ?>
                        <span class="each_list <?= $bg?> <?= $active?>" id_list="<?= $list_id?>"><?= $list_name?></span>
                        <?
                    }
                    ?>
                </div>
            </div>

            <?
            foreach($get_lists as $list){
                $list_id = $list["id"];

                switch ($list["id"]){
                    case "1":
                        $bg = "matcha";
                        break;
                    case "2":
                        $bg = "durian";
                        break;
                    case "3":
                        $bg = "iris";
                        break;
                    case "4":
                        $bg = "redbeanpaste";
                        break;
                    default:
                        $bg = "matcha";
                        break;
                }

                if($active_list_id == $list_id){
                    $active = "";
                }else{
                    $active = "disp_none";
                }

                ?>
                <div class="row list_table <?= $active?>" id_list="<?= $list_id?>">
                    <table id="table_id_<?= $list_id?>" data-order='[[ 2, "asc" ]]' class="table_id_datatables <?= $bg?> responsive">
                        <thead>
                        <tr>
                            <th class="mobile_disp_none">Number</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th class="mobile_disp_none">Type</th>
                            <th>Score</th>
                            <th>Episodes</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?
                        $i = 1;
                        $get_animes = Main::get("blogs_users_animes", "id_user = $user_id AND id_list = $list_id");
                        foreach($get_animes as $get_anime){
                            $id_anime = $get_anime["id_anime"];
                            $eps_seen = $get_anime["eps_seen"];
                            $score = $get_anime["score"];

                            $anime = Pages::get_by_id($id_anime);
                            $name = $anime["name"];
                            $type = $anime["type"];
                            $total_eps = $anime["nb_eps"];
                            $total_eps = reset(explode(" ", $total_eps));
                            $image = Pages::get_image($id_anime,"description ASC");
                            $image_path = $image[0]["image"];

                            switch ($score){
                                case "1":
                                    $score = "(1) Appalling";
                                    break;
                                case "2":
                                    $score = "(2) Horrible";
                                    break;
                                case "3":
                                    $score = "(3) Very Bad";
                                    break;
                                case "4":
                                    $score = "(4) Bad";
                                    break;
                                case "5":
                                    $score = "(5) Average";
                                    break;
                                case "6":
                                    $score = "(6) Fine";
                                    break;
                                case "7":
                                    $score = "(7) Good";
                                    break;
                                case "8":
                                    $score = "(8) Very Good";
                                    break;
                                case "9":
                                    $score = "(9) Great";
                                    break;
                                case "10":
                                    $score = "(10) Masterpiece";
                                    break;
                                default:
                                    $score = "Not Set";
                                    break;
                            }

                            $anime_link = "anime?id=" . $id_anime;
                            ?>
                            <tr class="row_anime" anime="<?= $id_anime?>">
                                <td class="mobile_disp_none"><?= $i?></td>
                                <td>
                                    <a href="<?= $anime_link?>">
                                        <img style="height:50px" src="<?= $image_path?>" />
                                    </a>
                                </td>

                                <td class="no_hover listed_anime_name">
                                    <a href="<?= $anime_link?>"><?= $name?></a>
                                </td>

                                <td class="listed_anime_type mobile_disp_none"><?= $type?></td>

                                <td class="listed_anime_score"><?= $score?></td>

                                <td class="listed_anime_eps_seen"><?= $eps_seen?> / <?= $total_eps?></td>

                            </tr>
                            <?
                            $i++;
                        }

                        ?>
                        </tbody>
                    </table>
                </div>
                <?
            }

            ?>

            <div class="row">
                <div id="target"></div>
            </div>

        </div>
    </div>
    <?

}

?>




<?
include 'fim.php';
?>
