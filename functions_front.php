<?php

if (!defined("modules"))
    define("modules", "modules");
if (!defined("banners"))
    define("banners", "banners");
if (!defined("pages"))
    define("pages", "pages");
if (!defined("news"))
    define("news", "news");
if (!defined("products"))
    define("products", "products");
if (!defined("services"))
    define("services", "services");
if (!defined("menus"))
    define("menus", "menus");
if (!defined("projects"))
    define("projects", "projects");
if (!defined("users"))
    define("users", "users");
if (!defined("faqs"))
    define("faqs", "faqs");
if (!defined("forms"))
    define("forms", "forms");
if (!defined("blogs"))
    define("blogs", "blogs");


require_once $_SERVER['DOCUMENT_ROOT'] . "/PHPMailer/PHPMailerAutoload.php";
include($_SERVER['DOCUMENT_ROOT'] . "/database_conection.php");
include($_SERVER['DOCUMENT_ROOT'] . "/simple_html_dom.php");
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';


/*------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------- LOOSE FUNCTIONS ----------------*/

function getPlaceholders($fields,$return="placeholders"){
    $temp_placeholders = "";
    $temp_reference = "";
    $temp_update = "";
    $temp_values = array();
    $count_placeholders = 0;

    foreach($fields as $field_placeholder=>$field_value){

        if($count_placeholders < sizeof($fields)-1){
            $temp_placeholders.=":$field_placeholder,";
            $temp_reference.= "`$field_placeholder`,";
            $temp_update.= "`$field_placeholder`=:$field_placeholder,";
        }else{
            $temp_placeholders.=":$field_placeholder";
            $temp_reference.= "`$field_placeholder`";
            $temp_update.= "`$field_placeholder`=:$field_placeholder";
        }

        $temp_values[":$field_placeholder"] = $field_value;
        $count_placeholders++;
    }

    if($return == "placeholders"){
        return $temp_placeholders;
    }else if($return == "values"){
        return $temp_values;
    }else if($return == "update"){
        return $temp_update;
    }else{
        return $temp_reference;
    }
}

function debug($object) {
    echo "<pre>";
    print_r($object);
    echo "</pre>";
}

function getIp() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function get_url() {

    $get_url = $_SERVER["REDIRECT_URL"];
    $get_url = str_replace("/backoffice/", "", $get_url);
    $get_url = str_replace("/", "", $get_url);

    $url = reset(explode("_", $get_url));

    /*$url = str_replace("bo_", "", $url);
    $url = str_replace("_show", "", $url);
    $url = str_replace("_edit", "", $url);*/

    return $url;
}

function go_to($url){
    echo "<script>window.location.replace('" . $url . "');</script>";
}

function alert($msg){
    echo "<script>alert('" . $msg . "');</script>";
}

function reload_page(){
    echo "<script>window.location.reload();</script>";
}

function PasswordGenerator($l = 10, $c = 4, $n = 4, $s = 2) {
    $count = $c + $n + $s;

    if (!is_int($l) || !is_int($c) || !is_int($n) || !is_int($s)) {
        trigger_error('Argument(s) not an integer', E_USER_WARNING);
        return false;
    } elseif ($l < 0 || $l > 20 || $c < 0 || $n < 0 || $s < 0) {
        trigger_error('Argument(s) out of range', E_USER_WARNING);
        return false;
    } elseif ($c > $l) {
        trigger_error('Number of password capitals required exceeds password length', E_USER_WARNING);
        return false;
    } elseif ($n > $l) {
        trigger_error('Number of password numerals exceeds password length', E_USER_WARNING);
        return false;
    } elseif ($s > $l) {
        trigger_error('Number of password capitals exceeds password length', E_USER_WARNING);
        return false;
    } elseif ($count > $l) {
        trigger_error('Number of password special characters exceeds specified password length', E_USER_WARNING);
        return false;
    }
    $chars = "abcdefghijklmnopqrstuvwxyz";
    $caps = strtoupper($chars);
    $nums = "0123456789";
    $syms = "!@#$%^&*()-+?";
    $out = '';

    for ($i = 0; $i < $l; $i++) {
        $out .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }

    if ($count) {
        $tmp1 = str_split($out);
        $tmp2 = array();

        for ($i = 0; $i < $c; $i++) {
            array_push($tmp2, substr($caps, mt_rand(0, strlen($caps) - 1), 1));
        }
        for ($i = 0; $i < $n; $i++) {
            array_push($tmp2, substr($nums, mt_rand(0, strlen($nums) - 1), 1));
        }
        for ($i = 0; $i < $s; $i++) {
            array_push($tmp2, substr($syms, mt_rand(0, strlen($syms) - 1), 1));
        }

        $tmp1 = array_slice($tmp1, 0, $l - $count);
        $tmp1 = array_merge($tmp1, $tmp2);
        shuffle($tmp1);
        $out = implode('', $tmp1);
    }

    return $out;
}

function sendmail($to, $from, $subject, $message, $replyto = false) {

    $message = str_replace('<br /><br />', '<br />', $message);

    $mail = new PHPMailer(true);
    $mail->isSendmail();
    $mail->CharSet = 'UTF-8';

    $emails_to = explode(',', $to);
    $emails_to = array_filter($emails_to);

    $from = explode("<", $from);
    $email_name = $from[0];
    $email_from = $from[1];
    $email_from = str_replace(">", "", $email_from);

    //$mail->setFrom("no-reply@jr-dev.pt", "JR-Dev");
    $mail->setFrom($email_from, $email_name);
    //$mail->setFrom($from);

    foreach($emails_to as $email_to){
        $email_to = trim($email_to);
        $mail->addAddress($email_to);
    }

    if($replyto != false){
        $mail->addReplyTo($replyto);
    }

    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body    = $message;

    if(!$mail->send()){
        echo 'Message could not be sent. Mailer Error: ';
        debug($mail->ErrorInfo);
        return false;
    }else{
        return true;
    }
}

/*function sendmail($to, $from, $subject, $message, $mime_version = "1.0", $content_type = "text/html", $charset = 'UTF-8', $replyto = false) {
    $headers = "From: " . $from . "\r\n";
    if ($replyto != false) {
        $headers .= "Reply-To: " . strip_tags($replyto) . "\r\n";
    }
    $headers .= "MIME-Version: $mime_version\r\n";
    $headers .= "Content-Type: $content_type; charset=$charset\r\n";

    $message=str_replace('<br /><br />', '<br />', $message);

    if (mail($to, $subject, $message, $headers)) {
        return true;
    } else {
        return false;
    }
}*/


/*------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------- BASE DE DADOS ----------------*/
function get($table, $where = false, $order = false) {

    global $conn;

    $sql = "SELECT * FROM $table";

    if($where != false){
        $sql .= " $where";
    }

    if($order != false){
        $sql .= " $order";
    }

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    //$query = $query->fetchAll();

    if ($query->rowCount() > 0) {
        return $query->fetchAll();
    } else {
        return false;
    }

}

function get_by_id($table, $id) {

    global $conn;

    $sql = "SELECT * FROM $table WHERE id = $id";

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    if ($query->rowCount() > 0) {
        return $query->fetch();
    } else {
        return false;
    }
}

function get_pagina($id) {

    global $conn;

    $sql = "SELECT * FROM paginas WHERE id = $id";

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    if ($query->rowCount() > 0) {
        return $query->fetch();
    } else {
        return false;
    }
}

function actualizar($table, $fields, $id) {

    global $conn;

    $sql = "UPDATE $table SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
    //debug($sql);

    $query = $conn->prepare($sql);

    if ($query -> execute(getPlaceholders($fields, "values"))) {
        return true;
    } else {
        return false;
    }
}

function actualizar_login($table, $resgisto_log, $where) {

    global $conn;

    $sql = "UPDATE $table SET last_log = '$resgisto_log' $where";

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    if($query -> execute()) {
        return true;
    }else {
        return false;
    }
}

function tables($db_name) {

    global $conn;

    $sql = "SHOW TABLES FROM $db_name";

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    if ($query->rowCount() > 0) {
        return $query->fetchAll();
    } else {
        return false;
    }
}

function count_rows($table) {

    global $conn;

    $sql = "SELECT COUNT(id) AS count_rows FROM $table; ";

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    if ($query->rowCount() > 0) {
        return $query->fetchAll();
    } else {
        return false;
    }
}

function sql($custom) {

    global $conn;

    $sql = $custom;

    //debug($sql);

    $query = $conn->prepare($sql);
    $query->execute();

    if ($query->rowCount() > 0) {
        return $query->fetchAll();
    } else {
        return false;
    }
}

function add($table, $fields, $return_last_inserted = false) {

    global $conn;

    $sql = "INSERT INTO $table (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

    $query = $conn->prepare($sql);

    if ($query -> execute(getPlaceholders($fields, "values"))) {

        if ($return_last_inserted) {
            return $conn->lastInsertId();
        } else {
            return true;
        }
    } else {
        //debug($query->errorInfo());
        return false;
    }
}

function delete($table, $where){

    global $conn;

    $sql = "DELETE FROM $table $where";

    $query = $conn->prepare($sql);

    if ($query -> execute()) {
        return true;
    } else {
        return false;
    }

}

function image_path($image, $path = false) {

    if($path == false){
        $full_path = "images/" . $image;
    }else{
        $full_path = "images/" . $path . "/" . $image;
    }

    return $full_path;
}

function file_path($file, $path = false) {

    if($path == false){
        $full_path = "documentos/" . $file;
    }else{
        $full_path = "documentos/" . $path . "/" . $file;
    }

    return $full_path;
}

function string_contains($needle, $string){
    if (strpos($string, $needle) !== false) {
        return true;
    }
}

function mal_weird_char($string){

    $keys = array(
        "☆" => "&#x2606;",
        "★" => "&#x2605;",
        "♥" => "&#x2665;",
        "♡" => "&#x2661;",
        "Ψ" => "&Psi;",
        "Δ" => "&Delta;",
        "△" => "&#x25b3;",
        "∞" => "&infin;",
        "ē" => "&#x113;",
        "♪" => "&#x266a;",
        "♂" => "&#x2642;",
        "♀" => "&#x2640;",
        "⚥" => "&#x26a5;",
        "χ" => "&chi;",
        "♭" => "&#x266d;",
        "○" => "&#x25cb;",
        "↑" => "&uarr;",
        "→" => "&rarr;",
        "←" => "&larr;",
        "@" => "&commat;",
        "&" => "&amp;",
        "⅙" => "&#x2159;",
        "␣" => "&#x2423;",
        "μ" => "&mu;",
        "＊" => "&#xff0a;",
        "◎" => "&#x25ce;",
        "√" => "&radic;",
        "◯" => "&#x25ef;",
        "℃" => "&#x2103;",
        "∽" => "&#x223d;",
    );

    //weird symbols
    $string = str_replace("☆", " ", $string);
    $string = str_replace("★", " ", $string);
    $string = str_replace("♥", " ", $string);
    $string = str_replace("♡", " ", $string);
    $string = str_replace("Ψ", "Psi", $string);
    $string = str_replace("Δ", " ", $string);
    $string = str_replace("△", " ", $string);
    $string = str_replace("∞", " ", $string);
    $string = str_replace("♪", " ", $string);
    $string = str_replace("♂", " ", $string);
    $string = str_replace("♀", " ", $string);
    $string = str_replace("⚥", " ", $string);
    $string = str_replace("χ", "X", $string);
    $string = str_replace("♭", "Flat", $string);
    $string = str_replace("○", " ", $string);
    $string = str_replace("↑", " ", $string);
    $string = str_replace("→", " ", $string);
    $string = str_replace("←", " ", $string);
    $string = str_replace("@", "&commat;", $string);
    $string = str_replace("⅙", " ", $string);
    $string = str_replace("␣", " ", $string);
    $string = str_replace("μ", " ", $string);
    $string = str_replace("＊", " ", $string);
    $string = str_replace("◎", " 2", $string);
    $string = str_replace("√", " ", $string);
    $string = str_replace("◯", " ", $string);
    $string = str_replace("℃", "ºC", $string);
    $string = str_replace("∽", "", $string);

    //weird punctuation
    $string = str_replace("ē", "e", $string);

    return $string;
}

function profanity($string){
    $string = strtolower($string);
    $words = explode(" ", $string);

    $bad_words = array(
        "2g1c", "2 girls 1 cup", "acrotomophilia", "alabama hot pocket", "alaskan pipeline", "anal", "anilingus", "anus", "apeshit", "arsehole", "ass", "asshole", "assmunch", "auto erotic",
        "autoerotic", "babeland", "baby batter", "baby juice", "ball gag", "ball gravy", "ball kicking", "ball licking", "ball sack", "ball sucking", "bangbros", "bareback", "barely legal",
        "barenaked", "bastard", "bastardo", "bastinado", "bbw", "bdsm", "beaner", "beaners", "beaver cleaver", "beaver lips", "bestiality", "big black", "big breasts", "big knockers", "big tits",
        "bimbos", "birdlock", "bitch", "bitches", "black cock", "blonde action", "blonde on blonde action", "blowjob", "blow job", "blow your load", "blue waffle", "blumpkin", "bollocks", "bondage",
        "boner", "booty call", "brown showers", "brunette action", "bukkake", "bulldyke", "bullet vibe", "bullshit", "bung hole", "bunghole", "busty", "butt", "buttcheeks", "butthole", "camel toe",
        "camgirl", "camslut", "camwhore", "carpet muncher", "carpetmuncher", "chocolate rosebuds", "circlejerk", "cleveland steamer", "clit", "clitoris", "clover clamps", "clusterfuck", "cock", "cocks",
        "coprolagnia", "coprophilia", "cornhole", "coon", "coons", "creampie", "cum", "cumming", "cunnilingus", "cunt", "darkie", "date rape", "daterape", "deep throat", "deepthroat", "dendrophilia",
        "dick", "dildo", "dingleberry", "dingleberries", "dirty pillows", "dirty sanchez", "doggie style", "doggiestyle", "doggy style", "doggystyle", "dog style", "dolcett", "domination", "dominatrix",
        "dommes", "donkey punch", "double dong", "double penetration", "dp action", "dry hump", "dvda", "eat my ass", "ecchi", "ejaculation", "erotic", "erotism", "escort", "eunuch", "faggot", "fecal",
        "felch", "fellatio", "feltch", "female squirting", "femdom", "figging", "fingerbang", "fingering", "fisting", "foot fetish", "footjob", "frotting", "fuck", "fuck buttons", "fuckin", "fucking",
        "fucktards", "fudge packer", "fudgepacker", "futanari", "gang bang", "gay sex", "genitals", "giant cock", "girl on", "girl on top", "girls gone wild", "goatcx", "goatse", "god damn", "gokkun",
        "golden shower", "goodpoop", "goo girl", "goregasm", "grope", "group sex", "g-spot", "guro", "hand job", "handjob", "hard core", "hardcore", "hentai", "homoerotic", "honkey", "hooker", "hot carl",
        "hot chick", "how to kill", "how to murder", "huge fat", "humping", "incest", "intercourse", "jack off", "jail bait", "jailbait", "jelly donut", "jerk off", "jigaboo", "jiggaboo", "jiggerboo",
        "jizz", "juggs", "kike", "kinbaku", "kinkster", "kinky", "knobbing", "leather restraint", "leather straight jacket", "lemon party", "lolita", "lovemaking", "make me come", "male squirting",
        "masturbate", "menage a trois", "milf", "missionary position", "motherfucker", "mound of venus", "mr hands", "muff diver", "muffdiving", "nambla", "nawashi", "negro", "neonazi", "nigga", "nigger",
        "nig nog", "nimphomania", "nipple", "nipples", "nsfw images", "nude", "nudity", "nympho", "nymphomania", "octopussy", "omorashi", "one cup two girls", "one guy one jar", "orgasm", "orgy",
        "paedophile", "paki", "panties", "panty", "pedobear", "pedophile", "pegging", "penis", "phone sex", "piece of shit", "pissing", "piss pig", "pisspig", "playboy", "pleasure chest", "pole smoker",
        "ponyplay", "poof", "poon", "poontang", "punany", "poop chute", "poopchute", "porn", "porno", "pornography", "prince albert piercing", "pthc", "pubes", "pussy", "queaf", "queef", "quim", "raghead",
        "raging boner", "rape", "raping", "rapist", "rectum", "reverse cowgirl", "rimjob", "rimming", "rosy palm", "rosy palm and her 5 sisters", "rusty trombone", "sadism", "santorum", "scat", "schlong",
        "scissoring", "semen", "sex", "sexo", "sexy", "shaved beaver", "shaved pussy", "shemale", "shibari", "shit", "shitblimp", "shitty", "shota", "shrimping", "skeet", "slanteye", "slut", "s&m", "smut",
        "snatch", "snowballing", "sodomize", "sodomy", "spic", "splooge", "splooge moose", "spooge", "spread legs", "spunk", "strap on", "strapon", "strappado", "strip club", "style doggy", "suck", "sucks",
        "suicide girls", "sultry women", "swastika", "swinger", "tainted love", "taste my", "tea bagging", "threesome", "throating", "tied up", "tight white", "tit", "tits", "titties", "titty", "tongue in a",
        "topless", "tosser", "towelhead", "tranny", "tribadism", "tub girl", "tubgirl", "tushy", "twat", "twink", "twinkie", "two girls one cup", "undressing", "upskirt", "urethra play", "urophilia", "vagina",
        "venus mound", "vibrator", "violet wand", "vorarephilia", "voyeur", "vulva", "wank", "wetback", "wet dream", "white power", "wrapping men", "wrinkled starfish", "xx", "xxx", "yaoi", "yellow showers",
        "yiffy", "zoophilia"
    );

    foreach($words as $word){
        if(in_array($word, $bad_words)){
            $profanity = true;
        }
    }

    if($profanity){
        return true;
    }
}


/*------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------- OUTSIDERS ----------------*/
function validarNIF($nif) {
    if ((!is_null($nif)) && (is_numeric($nif)) && (strlen($nif) == 9) && ($nif[0] == 1 || $nif[0] == 2 || $nif[0] == 5 || $nif[0] == 6 || $nif[0] == 8 || $nif[0] == 9)) {
        $dC = $nif[0] * 9;
        for ($i = 2; $i <= 8; $i++)
            $dC += ($nif[$i - 1]) * (10 - $i);
        $dC = 11 - ($dC % 11);
        $dC = ($dC >= 10) ? 0 : $dC;
        if ($dC == $nif[8])
            return TRUE;
    }
}

function verifica_tipo_ext_documento($tipo, $nome_ficheiro) {
    $ext = pathinfo($nome_ficheiro, PATHINFO_EXTENSION);
    //$ext = str_replace("jpeg", "jpg", strtolower($ext));

    $tipos_permitidos = array(
        "image/gif" => "gif",
        "image/pjpeg" => "jpg",
        "image/png" => "png",
        "image/bmp" => "bmp",
        "image/jpeg" => "jpg",
        "application/pdf" => "pdf",
        "application/save" => "pdf",
        "application/zip" => "zip",
        "application/x-zip-compressed" => "zip",
        "application/vnd.ms-excel" => "xls",
        "application/vnd.ms-excel" => "csv",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "xls",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation"=>"pptx",
        "application/msword" => "doc",
        "application/vnd.ms-powerpoint" => "ppt",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"=>"docx",
        "text/xml"=>"xml",
        "audio/mp3"=>"mp3",
        "audio/mpeg"=>"mp3",
        "audio/x-mpeg"=>"mp3",
        "audio/x-mp3"=>"mp3",
        "audio/mpeg3"=>"mp3",
        "audio/x-mpeg3"=>"mp3",
        "audio/mpg"=>"mp3",
        "audio/x-mpg"=>"mp3",
        "audio/x-mpegaudio"=>"mp3",
    );

    if (array_key_exists($tipo, $tipos_permitidos)) {
        $ext_lista = $tipos_permitidos[$tipo];
        if ($ext_lista == $ext) {
            return true;
        }
    }

    return false;
}

function verifica_tipo_ext_imagem($tipo, $nome_ficheiro) {

    $ext = pathinfo($nome_ficheiro, PATHINFO_EXTENSION);

    $ext = str_replace("jpeg", "jpg", strtolower($ext));
    $ext = strtolower($ext);
    $tipos_permitidos = array("image/gif" => "gif",
        "image/pjpeg" => "jpg",
        "image/png" => "png",
        "image/bmp" => "bmp",
        "image/jpeg" => "jpg");

    if (array_key_exists($tipo, $tipos_permitidos)) {

        $ext_lista = $tipos_permitidos[$tipo];
        if ($ext_lista == $ext) {
            return true;
        }
    }

    return false;
}

function random_code() {
    $s = strtoupper(md5(uniqid(rand(), true)));
    $guidText =
        substr($s, 0, 8) . '-' .
        substr($s, 8, 4);
    return $guidText;
}

function verifica_imagem($nome_ficheiro) {
    $ext = pathinfo($nome_ficheiro, PATHINFO_EXTENSION);
    if (empty($ext)) {
        return false;
    }
    $ext = str_replace("jpeg", "jpg", strtolower($ext));
    $ext = strtolower($ext);
    $tipos_permitidos = array("image/gif" => "gif",
        "image/pjpeg" => "jpg",
        "image/png" => "png",
        "image/bmp" => "bmp",
        "image/jpeg" => "jpg");

    if (in_array($ext, $tipos_permitidos)) {
        return true;
    }

    return false;
}

function parseXMLtoObject($xml) {
    $obj = new stdClass();

    $xml = explode("\n", $xml);

    $main_n = '';

    foreach ($xml as $x) {
        $first_n = false;
        $close_n = false;
        if ($x != '') {
            $start_val = (strpos($x, ">") + 1);
            $end_val = strrpos($x, "<") - $start_val;
            $start_n = (strpos($x, "<") + 1);
            $end_n = strpos($x, ">") - $start_n;
            $n = strtolower(substr($x, $start_n, $end_n));
            if (substr_count($x, "<") == 1) {
                if (!empty($main_n) && !stristr($n, "/")) {
                    $submain_n = $n;
                    $first_n = true;
                } else {
                    $main_n = $n;
                    $submain_n = '';
                    $first_n = true;
                }
            }
            if (!empty($submain_n) && stristr($submain_n, "/")) {
                $submain_n = '';
                $first_n = false;
                $close_n = true;
            }
            if (!empty($main_n) && stristr($main_n, "/")) {
                $main_n = '';
                $submain_n = '';
                $first_n = false;
                $close_n = true;
            }
            $value = substr($x, $start_val, $end_val);
            if (!$close_n) {
                if (empty($main_n)) {
                    $obj->$n = $value;
                } else {
                    if ($first_n) {
                        if (empty($submain_n)) {
                            $obj->$main_n = new stdClass();
                        } else {
                            $obj->$main_n->$submain_n = new stdClass();
                        }
                    } else {
                        if (!empty($value)) {
                            if (empty($submain_n)) {
                                $obj->$main_n->$n = $value;
                            } else {
                                $obj->$main_n->$submain_n->$n = $value;
                            }
                        }
                    }
                }
            }
        }
    }

    return $obj;
}

function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function create_zip($files = array(), $destination = '', $overwrite = false) {
    //if the zip file already exists and overwrite is false, return false
    if (file_exists($destination) && !$overwrite) {
        return false;
    }
    //vars
    $valid_files = array();
    //if files were passed in...
    if (is_array($files)) {
        //cycle through each file
        foreach ($files as $file) {
            //make sure the file exists
            if (file_exists($file)) {
                $valid_files[] = $file;
            }
        }
    }
    //if we have good files...
    if (count($valid_files)) {
        //create the archive
        $zip = new ZipArchive();
        if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }
        //add the files
        foreach ($valid_files as $file) {
            $zip->addFile($file, $file);
        }
        //debug
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
        //close the zip -- done!
        $zip->close();

        //check to make sure the file exists
        return file_exists($destination);
    } else {
        return false;
    }
}

function geo_local(){

    $ip_addr = getIp();

    $geoplugin = unserialize( file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip_addr) );

    if ( is_numeric($geoplugin['geoplugin_latitude']) && is_numeric($geoplugin['geoplugin_longitude']) ) {

        $lat = $geoplugin['geoplugin_latitude'];
        $long = $geoplugin['geoplugin_longitude'];
        $region_name = $geoplugin["geoplugin_regionName"];
        $region = $geoplugin["geoplugin_region"];
        $city = $geoplugin["geoplugin_city"];
        $country = $geoplugin["geoplugin_countryName"];

        $geo = array(
            'latitude' => $lat,
            'longitude' => $long,
            'region_name' => $region_name,
            'region' => $region,
            'city' => $city,
            'country' => $country,
        );
    }

    if ($geoplugin) {
        return $geo;
    } else {
        return $ip_addr;
    }

}


/*------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------- CLASSES ----------------*/


Class Main{

    function modulo_status($module) {

        global $conn;

        $sql = "SELECT status FROM modules WHERE name = '$module'; ";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function get($module, $where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . $module;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_image($module, $id, $order = false) {

        global $conn;
        $single = substr($module, 0, -1);

        $sql = "SELECT * FROM  " . $module . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_document($module, $id, $order = false) {

        global $conn;
        $single = substr($module, 0, -1);

        $sql = "SELECT * FROM  " . $module . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function add($module, $fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . $module . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function update($module, $fields, $id) {

        global $conn;

        $sql = "UPDATE " . $module . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_where($module, $fields, $where) {

        global $conn;

        $sql = "UPDATE " . $module . " SET " . getPlaceholders($fields, "update") . " WHERE $where";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function image_path($module, $image) {

        if($module == "banners" || $module == "users" || $module == "statistics"){
            $full_path = "backoffice/images/backend/" . $module . "/" . $image;
        }else{
            $full_path = "backoffice/images/backend/" . $module . "/images/" . $image;
        }

        return $full_path;
    }

    function delete($module, $where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . $module . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($module, $id_img){

        global $conn;

        $sql = "DELETE FROM " . $module . "_images WHERE id = $id_img";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($module, $id_vid){

        global $conn;

        $sql = "DELETE FROM " . $module . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function document_path($module, $document) {

        if($module == "banners" || $module == "users" || $module == "statistics"){
            $full_path = "backoffice/images/backend/" . $module . "/" . $document;
        }else{
            $full_path = "backoffice/images/backend/" . $module . "/files/" . $document;
        }

        return $full_path;
    }

    function delete_document($module, $id_doc){

        global $conn;

        $sql = "DELETE FROM " . $module . "_files WHERE id = $id_doc";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function getIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function get_url() {

        $get_url = $_SERVER["REDIRECT_URL"];
        $get_url = str_replace("/backoffice/", "", $get_url);
        $get_url = str_replace("/", "", $get_url);

        $url = reset(explode("_", $get_url));

        /*$url = str_replace("bo_", "", $url);
        $url = str_replace("_show", "", $url);
        $url = str_replace("_edit", "", $url);*/

        return $url;
    }

    function go_to($url){
        echo "<script>window.location.replace('" . $url . "');</script>";
    }

    function sql($custom) {

        global $conn;

        $sql = $custom;

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query -> execute()) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    function custom($sql) {

        global $conn;

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    /*function utf_text($texto) {
        $te_c = html_entity_decode(stripslashes(str_replace("\\", "", $texto)));
        $encoding = mb_detect_encoding($te_c);
        if ($encoding != "UTF-8") {
            $t =  html_entity_decode(stripslashes(str_replace("\\", "", utf8_encode($texto))), ENT_QUOTES, 'UTF-8');
        } else {
            if (mb_check_encoding($te_c, "UTF-8") OR mb_check_encoding($te_c, "ASCII")) {
                $t =  html_entity_decode(stripslashes(str_replace("\\", "", $texto)));
            } else {
                $t =  html_entity_decode(stripslashes(str_replace("\\", "", utf8_encode($texto))), ENT_QUOTES, 'UTF-8');
            }
        }
        return $t;
    }*/

}

Class Modules{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . modules;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . modules . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . modules . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . modules;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

}

Class Banners{

    function get($where = false, $order = false, $limit = false) {

        global $conn;

        $sql = "SELECT * FROM " . banners;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        if (!empty($limit)) {

            $limit_query = " LIMIT ";

            if (is_array($limit)) {
                if (count($limit) == 2) {
                    $limit_query .= $limit[0] . ",";
                    $limit_query .= $limit[1] . " ";
                }
            }

            if (is_string($limit)) {
                $limit_query .= $limit;
            }

        } else {
            $limit_query = '';
        }

        $sql .=  $limit_query;

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . banners . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . banners . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . banners . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . banners . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(banners, 0, -1);

        $sql = "UPDATE " . banners . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . banners . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }



    function get_image($id, $order = false) {

        global $conn;
        $single = substr(banners, 0, -1);

        $sql = "SELECT * FROM  " . banners . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        $query = $conn->prepare($sql);
        $query->execute();

        //$query = $query->fetchAll();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . banners . "/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(banners, 0, -1);

        $sql = "SELECT * FROM  " . banners . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . banners . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . banners . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . banners . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . banners . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . banners . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . banners . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . banners . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . banners . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . banners;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }


}

Class Pages{

    function get($where = false, $order = false, $limit = false) {

        global $conn;

        $sql = "SELECT * FROM " . pages;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        if (!empty($limit)) {

            $limit_query = " LIMIT ";

            if (is_array($limit)) {
                if (count($limit) == 2) {
                    $limit_query .= $limit[0] . ",";
                    $limit_query .= $limit[1] . " ";
                }
            }

            if (is_string($limit)) {
                $limit_query .= $limit;
            }

        } else {
            $limit_query = '';
        }

        $sql .=  $limit_query;

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false, $limit = false) {

        global $conn;

        $sql = "SELECT * FROM " . pages . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        if (!empty($limit)) {

            $limit_query = " LIMIT ";

            if (is_array($limit)) {
                if (count($limit) == 2) {
                    $limit_query .= $limit[0] . ",";
                    $limit_query .= $limit[1] . " ";
                }
            }

            if (is_string($limit)) {
                $limit_query .= $limit;
            }

        } else {
            $limit_query = '';
        }

        $sql .=  $limit_query;

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . pages . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . pages . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . pages . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(pages, 0, -1);

        $sql = "UPDATE " . pages . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . pages . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        $single = substr(pages, 0, -1);

        $sql = "SELECT * FROM  " . pages . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . pages . "/images/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(pages, 0, -1);

        $sql = "SELECT * FROM  " . pages . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        $single = substr(pages, 0, -1);

        $sql = "SELECT * FROM  " . pages . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . pages . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . pages . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . pages . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . pages . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . pages . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . pages . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . pages . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . pages . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . pages . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . pages . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_document($id_doc){

        global $conn;

        $sql = "DELETE FROM " . pages . "_files WHERE id = $id_doc";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . pages;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

}

Class News{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . news;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . news . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . news . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . news . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . news . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        //$single = substr(news, 0, -1);

        $sql = "UPDATE " . news . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . news . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . news . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        //$single = substr(news, 0, -1);

        $sql = "SELECT * FROM  " . news . "_images WHERE id_" . news . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . news . "/images/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        /*$single = substr(news, 0, -1);*/

        $sql = "SELECT * FROM  " . news . "_videos WHERE id_" . news . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        //$single = substr(news, 0, -1);

        $sql = "SELECT * FROM  " . news . "_files WHERE id_" . news . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . news . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . news . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . news . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . news . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . news . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . news . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . news . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . news . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . news . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . news . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_document($id_doc){

        global $conn;

        $sql = "DELETE FROM " . news . "_files WHERE id = $id_doc";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . news;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }




}

Class Users{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . users;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_user($user) {

        global $conn;

        $sql = "SELECT * FROM " . users . " WHERE user = '$user'";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . users . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . users . "/" . $image;

        return $full_path;
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . users . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . users . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . users . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function check_user_permission($id_permission) {

        global $conn;

        $sql = "SELECT * FROM permissions WHERE id = $id_permission";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . users;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    function actualizar_login($resgisto_log, $where) {

        global $conn;

        $sql = "UPDATE " . users . " SET last_log = '$resgisto_log' $where";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if($query -> execute()) {
            return true;
        }else {
            return false;
        }
    }

    function update_attempts($fields, $id) {

        global $conn;

        $sql = "UPDATE " . users . "_login_attempts SET " . getPlaceholders($fields, "update") . " WHERE user_id = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function get_user_attempts($id) {

        global $conn;

        $sql = "SELECT * FROM " . users . "_login_attempts WHERE user_id = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function check_login(){

        if(isset($_COOKIE["log"])){

            $logger = $_COOKIE["log"];
            $get_logger = Users::get("key_log = '$logger' AND status = 1");

            $user = $get_logger[0];
            $user_name = $user["user"];
            $user = Users::get_user_attempts($user_name);

            if($user["times"] < 5 && $user["locked_until"] == ""){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

}

Class Menus{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . menus;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . menus . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_sub_menus($id_prev_menu, $order = false) {

        global $conn;

        $sql = "SELECT * FROM ". menus . " WHERE prev_menu = " . $id_prev_menu;

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . menus . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . menus . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . menus . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . menus;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

}

Class Faqs{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . faqs;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . faqs . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . faqs . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . faqs . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . faqs . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(faqs, 0, -1);

        $sql = "UPDATE " . faqs . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . faqs . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        $single = substr(faqs, 0, -1);

        $sql = "SELECT * FROM  " . faqs . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . faqs . "/images/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(faqs, 0, -1);

        $sql = "SELECT * FROM  " . faqs . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        $single = substr(faqs, 0, -1);

        $sql = "SELECT * FROM  " . faqs . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . faqs . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . faqs . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . faqs . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . faqs . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . faqs . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . faqs . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . faqs . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . faqs . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . faqs . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . faqs . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_document($id_doc){

        global $conn;

        $sql = "DELETE FROM " . faqs . "_files WHERE id = $id_doc";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . faqs;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }





}

Class Products{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . products;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . products . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . products . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . products . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . products . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(products, 0, -1);

        $sql = "UPDATE " . products . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . products . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        $single = substr(products, 0, -1);

        $sql = "SELECT * FROM  " . products . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . products . "/images/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(products, 0, -1);

        $sql = "SELECT * FROM  " . products . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        $single = substr(products, 0, -1);

        $sql = "SELECT * FROM  " . products . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . products . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . products . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . products . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . products . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . products . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . products . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . products . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . products . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . products . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . products . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_document($id_doc){

        global $conn;

        $sql = "DELETE FROM " . products . "_files WHERE id = $id_doc";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . products;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }




}

Class Projects{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . projects;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . projects . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . projects . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . projects . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . projects . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(projects, 0, -1);

        $sql = "UPDATE " . projects . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . projects . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        $single = substr(projects, 0, -1);

        $sql = "SELECT * FROM  " . projects . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . projects . "/images/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(projects, 0, -1);

        $sql = "SELECT * FROM  " . projects . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        $single = substr(projects, 0, -1);

        $sql = "SELECT * FROM  " . projects . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . projects . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . projects . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . projects . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . projects . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . projects . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . projects . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . projects . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . projects . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . projects . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . projects . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_document($id_doc){

        global $conn;

        $sql = "DELETE FROM " . projects . "_files WHERE id = $id_doc";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . projects;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }





}

Class Services{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . services;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . services . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . services . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . services . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . services . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(services, 0, -1);

        $sql = "UPDATE " . services . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . services . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        $single = substr(services, 0, -1);

        $sql = "SELECT * FROM  " . services . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . services . "/images/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(services, 0, -1);

        $sql = "SELECT * FROM  " . services . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        $single = substr(services, 0, -1);

        $sql = "SELECT * FROM  " . services . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . services . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . services . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . services . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . services . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . services . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . services . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . services . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . services . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . services . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . services . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_document($id_doc){

        global $conn;

        $sql = "DELETE FROM " . services . "_files WHERE id = $id_doc";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . services;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }




}

Class Forms{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . forms;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . forms . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }


    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . forms . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . forms . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . forms . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . forms;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }


    function register_contact($fields_registo, $variables, $notif_admin = 1, $notif_user = 1, $email_admin = false, $return_last_inserted = true) {

        global $conn;

        $form = Forms::get_by_id($fields_registo["id_form"]);

        if($form){

            $general_email = $fields_registo["email"];
            $general_subject = $fields_registo["subject"];

            //$general_name = $fields["name"];
            //$general_message = $fields["message"];
            //$form_vars = explode(",", $form["variaveis"]);

            //data admin
            $subject_admin = $form["subject_admin"];
            $msg_admin = $form["msg_admin"];

            //data user
            $subject_user = $form["subject_user"];
            $msg_user = $form["msg_user"];

            foreach ($variables as $index => $variable){
                $msg_admin = str_replace($index, $variable, $msg_admin);
                $msg_user = str_replace($index, $variable, $msg_user);
            }

            $msg_user = str_replace("<div>", "", $msg_user);
            $msg_user = str_replace("</div>", "<br>", $msg_user);
            $msg_user = str_replace("<br><br><br><br>", "<br><br>", $msg_user);
            $msg_user = str_replace("<br><br><br>", "<br><br>", $msg_user);

            $msg_admin = str_replace("<div>", "", $msg_admin);
            $msg_admin = str_replace("</div>", "<br>", $msg_admin);
            $msg_admin = str_replace("<br><br><br><br>", "<br><br>", $msg_admin);
            $msg_admin = str_replace("<br><br><br>", "<br><br>", $msg_admin);


            // enviar email ao administrador
            $name_email_origin = $form["name_from"] . "<" . $form["email_from"] . ">";
            //$name_email_origin = $form["email_from"];

            if($notif_admin && $email_admin == false){
                sendmail($form["email_to"], $name_email_origin, $subject_admin . " - " . $general_subject, nl2br($msg_admin));
            }
            elseif ($notif_admin && !empty($email_admin)){
                sendmail($email_admin, $form["email_from"], $subject_admin . " - " . $general_subject, nl2br($msg_admin));
            }

            // enviar email ao utilizador
            if($notif_user){
                sendmail($general_email, $name_email_origin, $subject_user . " - " . $general_subject, nl2br($msg_user));
            }

            if($variables["]subject["] == ""){
                $fields_registo["subject"] = $subject_admin . " - " . $general_subject;
            }else{
                $fields_registo["subject"] = $subject_admin . " - " . $variables["]subject["];
            }

            $fields_registo["msg"] = $msg_admin;


            $sql = "INSERT INTO " . forms . "_emails_received (" . getPlaceholders($fields_registo, "reference") . ") VALUES(" . getPlaceholders($fields_registo) . ")";
            $query = $conn->prepare($sql);

            if ($query->execute(getPlaceholders($fields_registo, "values"))) {

                if ($return_last_inserted) {
                    return $conn->lastInsertId();
                } else {
                    return true;
                }
            } else {
                //debug($query->errorInfo());
                return false;
            }

        }
    }

    function get_emails_received($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . forms . "_emails_received";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_emails_received_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . forms . "_emails_received WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }
}

Class Blogs{

    function get($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . blogs;

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_categories($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_categories";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_users($where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_users";

        if($where != false){
            $sql .= " WHERE $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . " WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_user($user) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_users WHERE user = '$user'";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_user_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_users WHERE id = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_category_by_id($id) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_categories WHERE id = $id";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_comments($post_id, $where = false, $order = false) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_comments WHERE id_blog = $post_id";

        if($where != false){
            $sql .= " AND $where";
        }

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function get_comments_by_post($post_id) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_comments WHERE id_blog = $post_id AND prev_comment IS NULL";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function get_sub_comments($post_id, $parent_comment) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_comments WHERE id_blog = $post_id AND prev_comment = $parent_comment";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    function get_comments_likes($comment_id, $where = false) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_comments_likes WHERE id_comment = $comment_id";

        if($where != false){
            $sql .= " AND $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    function get_total_comments_likes($comment_id) {

        global $conn;

        $sql = "SELECT COUNT(id) FROM " . blogs . "_comments_likes WHERE id_comment = $comment_id GROUP BY id_comment";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function update($fields, $id) {

        global $conn;

        $sql = "UPDATE " . blogs . " SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_image($fields, $id) {

        global $conn;
        $single = substr(blogs, 0, -1);

        $sql = "UPDATE " . blogs . "_images SET " . getPlaceholders($fields, "update") . " WHERE id_" . $single . " = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_category($fields, $id) {

        global $conn;

        $sql = "UPDATE " . blogs . "_categories SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

    function update_user($fields, $id) {

        global $conn;

        $sql = "UPDATE " . blogs . "_users SET " . getPlaceholders($fields, "update") . " WHERE id='$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }


    function get_image($id, $order = false) {

        global $conn;
        $single = substr(blogs, 0, -1);

        $sql = "SELECT * FROM  " . blogs . "_images WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function image_path($image) {

        $full_path = "backoffice/images/backend/" . blogs . "/images/" . $image;

        return $full_path;
    }

    function user_image_path($image) {

        $full_path = "backoffice/images/backend/" . blogs . "/users/" . $image;

        return $full_path;
    }


    function get_video($id, $order = false) {

        global $conn;
        $single = substr(blogs, 0, -1);

        $sql = "SELECT * FROM  " . blogs . "_videos WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }else{
            $sql .= " ORDER BY id ASC";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }


    function get_file($id, $order = false) {

        global $conn;
        $single = substr(blogs, 0, -1);

        $sql = "SELECT * FROM  " . blogs . "_files WHERE id_" . $single . " = $id";

        if($order != false){
            $sql .= " ORDER BY $order";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }

    }

    function file_path($file) {

        $full_path = "backoffice/images/backend/" . blogs . "/files/" . $file;

        return $full_path;
    }


    function add_category($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . blogs . "_categories (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . blogs . " (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_image($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . blogs . "_images (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_video($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . blogs . "_videos (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_document($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . blogs . "_files (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }

    function add_user($fields, $return_last_inserted = false) {

        global $conn;

        $sql = "INSERT INTO " . blogs . "_users (" . getPlaceholders($fields, "reference") . ") VALUES(" . getPlaceholders($fields) . ")";

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {

            if ($return_last_inserted) {
                return $conn->lastInsertId();
            } else {
                return true;
            }
        } else {
            //debug($query->errorInfo());
            return false;
        }
    }


    function delete($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . blogs . " $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_category($where){

        global $conn;

        $where = "WHERE " . $where;

        $sql = "DELETE FROM " . blogs . "_categories $where";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_image($id_img){

        global $conn;

        $sql = "DELETE FROM " . blogs . "_images WHERE id = $id_img";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }

    function delete_video($id_vid){

        global $conn;

        $sql = "DELETE FROM " . blogs . "_videos WHERE id = $id_vid";

        $query = $conn->prepare($sql);

        if ($query -> execute()) {
            return true;
        } else {
            return false;
        }

    }


    function count_rows($where = false) {

        global $conn;

        $sql = "SELECT COUNT(id) AS count_rows FROM " . blogs;

        if($where != false){
            $sql .= " WHERE $where";
        }

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetchAll();
        } else {
            return false;
        }
    }

    function get_user_attempts($id) {

        global $conn;

        $sql = "SELECT * FROM " . blogs . "_login_attempts WHERE user_id = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if ($query->rowCount() > 0) {
            return $query->fetch();
        } else {
            return false;
        }
    }

    function check_login(){

        if(isset($_COOKIE["anime_log"])){

            $logger = $_COOKIE["anime_log"];
            $get_logger = Blogs::get_users("key_log LIKE '%$logger%' AND status = 1");

            if($get_logger){
                $user = $get_logger[0];
                $user_name = $user["user"];
                $user = Blogs::get_user_attempts($user_name);

                if($user["times"] < 5 && $user["locked_until"] == ""){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }



        }else{
            return false;
        }
    }

    function actualizar_login($resgisto_log, $where) {

        global $conn;

        $sql = "UPDATE " . blogs . "_users SET last_log = '$resgisto_log' $where";

        //debug($sql);

        $query = $conn->prepare($sql);
        $query->execute();

        if($query -> execute()) {
            return true;
        }else {
            return false;
        }
    }

    function update_attempts($fields, $id) {

        global $conn;

        $sql = "UPDATE " . blogs . "_login_attempts SET " . getPlaceholders($fields, "update") . " WHERE user_id = '$id'";
        //debug($sql);

        $query = $conn->prepare($sql);

        if ($query -> execute(getPlaceholders($fields, "values"))) {
            return true;
        } else {
            return false;
        }
    }

}

