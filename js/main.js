/*Código JS apenas para funções de Front-end*/
$(document).ready(function(){


    var pathname = window.location.pathname; // Returns path only (/path/example.html)
    var url = window.location.href;     // Returns full URL (https://example.com/path/example.html)
    var origin = window.location.origin;   // Returns base URL (https://example.com)
    var params = $(location).attr('search');   // Returns base URL params (?id=123)

    if(pathname == "/my_lists"){
        if($(window).width() <= 992){
            var arr = [1, 2, 3, 4, 5, 6, 7 , 8, 9, 10];
            $("select.personal_score option[disabled]").html("1-10");
            $.each( arr, function( i, val ) {
                var str = $("select.personal_score option[value='" + val + "']").html();
                var options = str.split(' ')[0];
                $("select.personal_score option[value='" + val + "']").html(options);

            });
        }
    }



    $(".table_id_datatables").DataTable({
        paging: false
    });

    $(".table_id_datatables_submissions").DataTable();

    $("[data-toggle='tooltip']").tooltip();

    $(".pop[data-toggle='popover']").popover();

    $(".classy-editor").ClassyEdit();







/*MENU MOBILE TOGGLER*/

    $(".menu-toggler").click(function() {
        var menu = $(".menu");

        menu.toggleClass("aberto");
        $('#nav-icon1').toggleClass('open');

        /*var menuA = $(".menu_holder");
        var menuB = $(".menu");
        if(menuB.hasClass("aberto")){
            menuA.css("background-color", "rgba(255, 255, 255, 1)");
        }else{
            menuA.css("background-color", "rgba(255, 255, 255, 0.7)");
        }*/

    });


/*MENU SCROLL -> css changer*/

    /*$(window).scroll(function() {
        //when you scroll the page, hide the submenu box
        if ($(window).scrollTop() > 50) {
            $(".menu_holder").addClass("scroll");
        }else{
            $(".menu_holder").removeClass("scroll");
        }
    });*/


    var wer = $(".footer").outerHeight();
    $(".whole_page").css("padding-bottom", wer);




/*FANCYBOX*/

    $(".fancybox").fancybox({
        type: "image",
        openEffect	: 'none',
        closeEffect	: 'none',
        beforeShow: function () {
            this.title = $(this.element).attr('title');
        }
    });

    $(".fancybox_pdf").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        autoSize: true,
        type: 'iframe',
        iframe: {
            preload: false // fixes issue with iframe and IE
        }
    });

    $(".fancybox_youtube").fancybox({
        maxWidth    : 800,
        maxHeight   : 600,
        fitToView   : false,
        width       : '70%',
        height      : '70%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });

    /*window.setInterval(function () {

        $(".teste").animate({bottom: "5px"}, 500);
        $(".teste").animate({bottom: "0"}, 500);

    }, 1500);*/




/*SLIDER HOME*/
    var timer_fast = 10000;
    var timer_slow = 450099999;


    $("#slider_home").owlCarousel({
        loop:true,
        autoplay:true,
        //navText: ['<i class="icon-arrow-left" aria-hidden="true"></i>', '<i class="icon-arrow-right" aria-hidden="true"></i>'],
        navText: ['<img src="images/prev.png">', '<img src="images/next.png">'],
        nav:false,
        dots:false,
        items:1,
        autoplayTimeout: timer_fast,
        //autoplayHoverPause: true,
    });


/*FORM CART ORDER*/

    $("#form_cart_order").submit(function(e) {
        e.preventDefault();

        var $serialize = $(this).serialize();

        $("#order_response").show().html('Validating <i style="font-size: 20px;" class="fa fa-spinner fa-spin"></i>');

        $('html, body').animate({
            scrollTop: ( $(".anchor_scroll").offset().top - $(".menu_holder").height() ),
        }, 1000);

        delay(function(){
            $.post("processes/ajax_cart_order.php", $serialize, function(data) {
                $("#order_response").html(data);
            });
        },1000);
    });


/*FAQS*/

    $(".tab_titulo").click(function () {

        if($(".conteudo_titulo[anchor='" + $(this).attr("anchor") + "']").hasClass("aberto")){
            $(".conteudo_titulo").slideUp();
            $(".conteudo_titulo").removeClass("aberto");

            $(".faqs i").removeClass("fa-minus");
            $(".faqs i").addClass("fa-plus");

        }else{
            $(".conteudo_titulo").slideUp();
            $(".conteudo_titulo[anchor='" + $(this).attr("anchor") + "']").slideDown();
            $(".conteudo_titulo[anchor='" + $(this).attr("anchor") + "']").addClass("aberto");

            $("i", this).removeClass("fa-plus");
            $("i", this).addClass("fa-minus");
        }

    });


    /*$("#pesq").keyup(function(){
        var $pesq = $(this).val();

        $("#target").html("<div class='row domain_each_line'><div class='col-sm-12 domain_field_line'><i style='color: #000; font-size: 20px;' class='fa fa-spinner fa-spin'></i></div></div>");

        delay(function(){
            $.post("/processes/ajax_search.php", {pesq:$pesq}, function(data) {
                $("#target").html(data);
            });
        },1000);
    });*/

    $(document).on("click", ".remove_like", function (event) {
        var $id_user = $(this).attr("iu");
        var $id_blog = $(this).attr("ib");

        $.post("/processes/ajax_remove_like.php", {id_user:$id_user, id_blog:$id_blog}, function(data) {
            $("#target").html(data);
        });
    });

    $(document).on("click", ".add_like", function (event) {
        var $id_user = $(this).attr("iu");
        var $id_blog = $(this).attr("ib");

        $.post("/processes/ajax_add_like.php", {id_user:$id_user, id_blog:$id_blog}, function(data) {
            $("#target").html(data);
        });
    });





    $(document).on("click", ".table_id_datatables_submissions .add_studio", function (event) {
        var $all_studios = $("#edit_submission input[name='studios_of_anime_all']").val();
        var $studio = $(this).attr("studio");

        if($all_studios == $studio || $all_studios.includes($studio)){
            alert("That studio is already associated.");
        } else {
            $.post("/processes/ajax_subm_add_studio.php", {studio:$studio}, function(data) {
                $("#studios").html(data);
            });
        }
    });

    $(document).on("click", ".table_id_datatables_submissions .add_genre", function (event) {
        var $all_genres = $("#edit_submission input[name='genres_of_anime_all']").val();
        var $genre = $(this).attr("genre");

        if($all_genres == $genre || $all_genres.includes($genre)){
            alert("That genre is already associated.");
        } else {
            $.post("/processes/ajax_subm_add_genre.php", {genre:$genre}, function(data) {
                $("#genres").html(data);
            });
        }
    });


    $(".btn_trailer").click(function(){

        if($(this).hasClass("closed")){

            if ($(window).width() < 992) {
                $('html, body').animate({
                    //scrollTop: ( $(".anchor_scroll").offset().top - $(".menu_holder").height() ),
                    scrollTop: ( $(".youtube_video_scroll_target").offset().top - 50 ),
                }, 1000);
            }else{
                $('html, body').animate({
                    //scrollTop: ( $(".anime_info").offset().top ),
                    scrollTop: ( $(".youtube_video_scroll_target").offset().top - 100 ),
                }, 1000);
            }

            $(".youtube_video").slideDown();
            $(this).removeClass("closed");
        }else{
            $(".youtube_video").slideUp();
            $(this).addClass("closed");
        }

    });

    $(".btn_trailer2").click(function(){
        $(".btn_trailer").trigger("click");
    });

    $("#btn_open_sorting").click(function(){
        var sort_bar = $("#sorting_bar");

        if(sort_bar.hasClass("open")){
            sort_bar.removeClass("open");
        }else{
            sort_bar.addClass("open");
        }
    });

    $(document).on("click", ".synopsis_btn", function (event) {
        var target = $(this).attr("target");

        $(this).addClass("disp_none");
        $(".overlay[target='" + target + "']").addClass("disp_none");
        $(".synopsis[target='" + target + "']").removeClass("disp_none");
    });

    $(document).on("click", ".synopsis i", function (event) {
        var target = $(this).parent().attr("target");

        $(".synopsis[target='" + target + "']").addClass("disp_none");
        $(".synopsis_btn[target='" + target + "']").removeClass("disp_none");
        $(".overlay[target='" + target + "']").removeClass("disp_none");
    });

    $(document).on("click", "#add_to_list .btn_add_to_list", function (event) {
        var self = $(this);
        var target = $("#add_to_list .lists");

        if(self.hasClass("closed")){
            self.removeClass("closed");
            target.slideDown();
            target.removeClass("disp_none");
        }else{
            self.addClass("closed");
            target.slideUp();
            target.addClass("disp_none");
        }


    });

    $(".search_by .magnifier").click(function(){
        var $search = $(this).prev().val();
        var $sort = $(this).prev().attr("sort");

        if($search.length >= 3){

            window.history.pushState({"html":"","pageTitle":""},"", "?q=" + $search);
            $("#search_target").html("<div class='col-md-12 loading_spinner'><i class='fa fa-spinner fa-spin'></i></div>");

            $.post("/processes/ajax_search.php", {search:$search, sort:$sort}, function(data) {
                $("#search_target").html(data);
                $(".search_by input").val("");
                $("#btn_open_sorting").trigger("click");
            });

        }else{
            alert("Need at least 3 letters to search");
        }

    });

    $(".search_by input").keyup(function(e){
        if(e.keyCode == 13) { // presss key 'Enter'
            $(".search_by .magnifier").trigger("click");
        }
    });



/*MENU SEARCH*/
    $("#m_search .open_search_bar").click(function(){
        $("#m_search i").removeClass("open_search_bar");
        $("#m_search").addClass("open");
        $("#menu_search").focus();
    });

    $("#menu_search_form").submit(function(e) {
        e.preventDefault();

        var $search = $("#menu_search").val();

        if($search.length >= 3){
            var url = $search.replace(" ", "%20");
            window.location.replace("animes?q=" + url);
        }else{
            alert("Need at least 3 letters to search");
        }
    });
/*MENU SEARCH*/




    $(document).keyup(function(e) {
        if (e.keyCode === 27){ // presss key 'ESC'
            $("#sorting_bar").removeClass("open");
        }
    });

    $(".select_studios").change(function(){
        var $studio = $(this).val();

        $("#search_target").html("<div class='col-md-12 loading_spinner'><i class='fa fa-spinner fa-spin'></i></div>");

        $.post("/processes/ajax_search_by_studio.php", {studio:$studio}, function(data) {
            $("#search_target").html(data);
            $("#btn_open_sorting").trigger("click");
        });

    });

    $(".select_genre").change(function(){
        var $genre = $(this).val();

        $("#search_target").html("<div class='col-md-12 loading_spinner'><i class='fa fa-spinner fa-spin'></i></div>");

        $.post("/processes/ajax_search_by_genre.php", {genre:$genre}, function(data) {
            $("#search_target").html(data);
            $("#btn_open_sorting").trigger("click");
        });

    });

    $(".select_tpp").change(function(){
        var $tpp = $(this).val();
        var pathname = $(location).attr('pathname');
        window.location.replace(pathname + "?tpp=" + $tpp);
    });


    $(".tab_opener").click(function(){
        var target = $(this).attr("show");

        $(".tab_opener").removeClass("active");
        $(this).addClass("active");

        $(".tab").addClass("disp_none");
        $(".tab[show='" + target + "']").removeClass("disp_none");
    });

    $(".each_theme_type").click(function(){
        var theme = $(this).attr("theme_type");

        $(".theme_type_img").removeClass("active");
        $(".theme_type_img", this).addClass("active");

        $("input[name='theme_type']").val(theme);
    });

    $(".each_anime_page_type").click(function(){
        var page_type = $(this).attr("page_type");

        $(".anime_page_type_img").removeClass("active");
        $(".anime_page_type_img", this).addClass("active");

        $("input[name='anime_page_type']").val(page_type);
    });

    $(document).on("click", ".select_list", function (event) {
        var $id_anime = $(this).attr("anime");
        var $id_list = $(this).attr("id_list");
        var icon = $("i", this).attr("icon");


        $.post("/processes/ajax_add_to_list.php", {id_anime:$id_anime, id_list:$id_list}, function(data) {
            $("#target").html(data);
            $(".type_list_label").addClass("disp_none");
            $(".type_list_label[id_list='" + $id_list + "']").removeClass("disp_none");
            $(".btn_add_to_list").trigger("click");
            $(".btn_add_to_list i").removeClass();
            $(".btn_add_to_list i").addClass("fa " + icon);
        });
    });

    $(".personal_score").change(function(){
        var $score = $(this).val();
        var $id_anime = $(this).attr("id_anime");

        $(".select_overlay[id_anime='" + $id_anime + "']").css("display", "block");

        delay(function(){
            $.post("/processes/ajax_update_score.php", {score:$score, id_anime:$id_anime}, function(data) {
                $(".select_overlay[id_anime='" + $id_anime + "']").css("display", "none");
                $(".personal_score[id_anime='" + $id_anime + "']").addClass("changed");
                $("#target").html(data);
            });
        },1000);
    });


//SEARCH ANIMES FOR RELATIONS
    $("#get_animes_form").submit(function(e) {
        e.preventDefault();

        var search = $("#get_animes").val();
        var id_anime = $("#get_animes").attr("iu");

        if(search.length >= 3){
            var url = search.replace(" ", "%20");
            window.location.replace("relations?id=" + id_anime + "&s=" + url);
        }else{
            alert("Need at least 3 letters to search");
        }
    });



//EDIT RELATIONS
    $(document).on("click", ".btn_relations", function (event) {
        var $id_anime = $(this).attr("ia");
        var $add_type = $(this).attr("add_type");

        var $my_ids = "";

        $(".each_holder.selected").each(function() {
            $my_ids += "[" + $(this).attr("ia") + "];";
        });

        $.post("/processes/ajax_add_relations.php", {id_anime:$id_anime, my_ids:$my_ids, add_type:$add_type}, function(data) {
            $("#target").html(data);
        });

        $("#get_animes_target .each_holder").removeClass("selected");
    });

    $(document).on("click", "#get_animes_target .each_holder", function (event) {
        if(!$(this).hasClass("is_prequel") && !$(this).hasClass("is_sequel") && !$(this).hasClass("is_other")){
            $(this).toggleClass("selected");
        }
    });








/*PROFILE - CHANGE PICTURE*/

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $("#profile_pic").attr("style", "background-image: url('" + e.target.result +"')");
                $("#profile_pic").parent().attr("href", "" + e.target.result +"");
            };

            reader.readAsDataURL(input.files[0]);

            //$("#change_profile_pic").trigger("submit");
        }
    }

    $("input#file_image").change(function () {
        readURL(this);

        var file_name = $('input[type=file]').val().split('\\').pop();

        $("#file_image_clone").css("background-color", "#567700");
        $("#file_image_clone").html("<i class=\"fa fa-check\" aria-hidden=\"true\"></i> " + file_name);

    });


    $(".each_list").click(function(){
        var target = $(this).attr("id_list");

        $(".each_list").removeClass("active");
        $(this).addClass("active");

        $(".list_table").addClass("disp_none");
        $(".list_table[id_list='" + target + "']").removeClass("disp_none");

        window.history.pushState({"html":"","pageTitle":""},"", "?list=" + target);
    });


    $(".nakama .each_list").click(function(){
        var target = $(this).attr("id_list");
        var nakama = $("#nakama").val();

        $(".nakama .each_list").removeClass("active");
        $(this).addClass("active");

        $(".list_table").addClass("disp_none");
        $(".list_table[id_list='" + target + "']").removeClass("disp_none");

        window.history.pushState({"html":"","pageTitle":""},"", "?id=" + nakama + "&list=" + target);
    });


    $(document).on("click", ".actions .delete_anime", function (event) {
        //var row = $(this).parent().parent();
        var row = $(".row_anime[anime='" + $id_anime + "']");
        var $id_anime = $(this).attr("anime");

        $.post("/processes/ajax_remove_from_lists.php", {id_anime:$id_anime}, function(data) {
            $("#target").html(data);
            row.addClass("deleted");
        });
    });

    $(document).on("click", ".actions .add_ep", function (event) {
        var $id_anime = $(this).attr("anime");
        var add = $(".ep_target[anime='" + $id_anime + "']").val();
        //var row = $(this).parent().parent();
        var row = $(".row_anime[anime='" + $id_anime + "']");

        if(add >= 0){
            var $new_ep = parseInt(add) + 1;
            $(".ep_target[anime='" + $id_anime + "']").val($new_ep);
        }

        delay(function(){
            $.post("/processes/ajax_update_ep.php", {id_anime:$id_anime, new_ep:$new_ep}, function(data) {
                $("#target").html(data);
                row.addClass("changed");
            });
        },1000);
    });

    $(document).on("click", ".actions .sub_ep", function (event) {
        var $id_anime = $(this).attr("anime");
        var add = $(".ep_target[anime='" + $id_anime + "']").val();
        //var row = $(this).parent().parent();
        var row = $(".row_anime[anime='" + $id_anime + "']");

        if(add > 0){
            var $new_ep = parseInt(add) - 1;
            $(".ep_target[anime='" + $id_anime + "']").val($new_ep);
        }

        delay(function(){
            $.post("/processes/ajax_update_ep.php", {id_anime:$id_anime, new_ep:$new_ep}, function(data) {
                $("#target").html(data);
                row.addClass("changed");
            });
        },1000);
    });

    $(document).on("keyup", "input.ep_target", function (event) {
        if(event.keyCode == 13) { // presss key 'Enter'
            var $new_ep = $(this).val();
            var $id_anime = $(this).attr("anime");
            //var row = $(this).parent().parent().parent();
            var row = $(".row_anime[anime='" + $id_anime + "']");

            $.post("/processes/ajax_update_ep.php", {id_anime:$id_anime, new_ep:$new_ep}, function(data) {
                $("#target").html(data);
                row.addClass("changed");
            });
        }
    });

    $(".switch_lists").click(function() {
        var anime = $(this).attr("anime");

        //$(".switch_lists").trigger("click");

        $(this).popover({
            html : true,
            content: function() {
                return $('.popover_content_wrapper[anime="' + anime + '"]').html();
            }
        });

    });

    $(document).on("click", ".popover_select", function (event) {
        var $id_anime = $(this).attr("anime");
        var $id_list = $(this).attr("id_list");
        var row = $(".row_anime[anime='" + $id_anime + "']");

        $.post("/processes/ajax_change_lists.php", {id_anime:$id_anime, id_list:$id_list}, function(data) {
            $("#target").html(data);
            $(".switch_lists[anime='" + $id_anime + "']").trigger("click");
            row.addClass("changed");
        });
    });


/*PAGINATION*/
    $(document).on("click", ".all_animes .pagination li span.link_pag", function (event) {
        var $pageno = $(this).attr("pageno");
        var $sort = $(this).attr("sort");
        var $tpp = $(this).attr("tpp");
        var $q = $(this).attr("q");

        $.post("/processes/ajax_pagination.php", {pageno:$pageno, sort:$sort, tpp:$tpp, q:$q}, function(data) {
            $("#search_target").html(data);
            window.history.pushState({"html":"","pageTitle":""},"", "?pageno=" + $pageno);
        });

        $('html, body').animate({
            scrollTop: ( $(".fakebanner").offset().top ),
        }, 1000);
    });

    $(document).on("click", ".all_studios .pagination li span.link_pag", function (event) {
        var $pageno = $(this).attr("pageno");
        var $tpp = $(this).attr("tpp");
        var $id_studio = $(this).attr("id_studio");

        $.post("/processes/ajax_pagination_studios.php", {pageno:$pageno, tpp:$tpp, id_studio:$id_studio}, function(data) {
            $("#studios").html(data);

            window.history.pushState({"html":"","pageTitle":""},"", params + "&pageno=" + $pageno);
        });

        $('html, body').animate({
            scrollTop: ( $(".fakebanner").offset().top ),
        }, 1000);
    });

    $(document).on("click", ".all_genres .pagination li span.link_pag", function (event) {
        var $pageno = $(this).attr("pageno");
        var $tpp = $(this).attr("tpp");
        var $id_genre = $(this).attr("id_genre");

        $.post("/processes/ajax_pagination_genres.php", {pageno:$pageno, tpp:$tpp, id_genre:$id_genre}, function(data) {
            $("#genres").html(data);
            window.history.pushState({"html":"","pageTitle":""},"", params + "&pageno=" + $pageno);
        });

        $('html, body').animate({
            scrollTop: ( $(".fakebanner").offset().top ),
        }, 1000);
    });


/*TABS SEASON*/

    $(document).on("click", ".each_type", function (event) {
        var target = $(this).attr("type");

        $(".each_type").removeClass("active");
        $(this).addClass("active");

        $(".anime_type_holder").addClass("disp_none");

        $(".anime_type_holder[type='" + target + "']").removeClass("disp_none");
    });

    $(".select_years").change(function(){
        var $year = $(this).val();
        var $season = $(".select_stations").val();

        if($season == ""){
            var $season = "Summer";
        }

        var $season_total = $year + $season;
        alert($season_total);

        $("#search_target").html("<div class='col-md-12 loading_spinner'><i class='fa fa-spinner fa-spin'></i></div>");

        $.post("/processes/ajax_search_by_season.php", {season_total:$season_total}, function(data) {
            $("#search_target").html(data);
            $("#btn_open_sorting").trigger("click");


            var season = $season.replace('-[A]', '').replace('-[B]', '').replace('-[C]', '').replace('-[D]', '');

            var link = $year + "_" + season;


            window.history.pushState({"html":"","pageTitle":""},"", "?season=" + link);
        });

    });

    $(document).on("click", ".comment .details i.add_like", function (event) {
        var $id_comment = $(this).attr("id_comment");

        $(this).animate({top: "10px"}, {speed: 300});
        $(this).animate({top: "17px"}, {speed: 300});

        $.post("/processes/ajax_comment_add_like.php", {id_comment:$id_comment}, function(data) {
            $("#target").html(data);
        });
    });

    $(document).on("click", ".comment .details i.remove_like", function (event) {
        var $id_comment = $(this).attr("id_comment");

        $(this).animate({top: "25px"}, {speed: 300});
        $(this).animate({top: "17px"}, {speed: 300});

        $.post("/processes/ajax_comment_remove_like.php", {id_comment:$id_comment}, function(data) {
            $("#target").html(data);
        });
    });

    $(document).on("click", ".comment .parent_comment .delete_comment", function (event) {
        var $id_comment = $(this).attr("id_comment");

        var r = confirm("Delete this feedback?");
        if(r == true){
            $.post("/processes/ajax_delete_comment.php", {id_comment:$id_comment}, function(data) {
                $("#target").html(data);
            });
        }
    });

    $("#form_comment").submit(function(e){
        e.preventDefault();

        var $id_anime = $("input[name='id_anime']").val();
        var $comment = $("textarea[name='comment']").val();

        $.post("/processes/ajax_add_comment.php", {id_anime:$id_anime, comment:$comment}, function(data) {
            $("#comment_response").html(data);
        });
    });

    $(".btn_submit_comment").click(function(){
        $("#form_comment").trigger("submit");
    });

    $(".collap h3").click(function(){
        var target = $(this).attr("target");

        if($(this).hasClass("closed")){
            $("ul[target='" + target + "']").slideDown();
            $(this).removeClass("closed");
        }else{
            $("ul[target='" + target + "']").slideUp();
            $(this).addClass("closed");
        }


    });


//------------------------------------//
//Wow Animation//
//------------------------------------//
    wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true        // trigger animations on mobile devices (true is default)
        }
    );
    wow.init();

});


var delay = (function(){
    var timer = 0;

    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();
