<?
include 'inicio.php';
?>

<div class="bloco">
    <div class="container">

        <?

        /*---------------*/
        /*---- Image ----*/
        /*---------------*/

        $no_img = "https://mochi.jr-dev.pt/images/no_image.jpg";
        $warnings = array();
        $all = array();

        $sql = "SELECT pages.id, pages.name, pages_images.id_page, pages_images.image";
        $sql .= " FROM pages";
        $sql .= " INNER JOIN pages_images ON pages.id = pages_images.id_page";
        $sql .= " WHERE pages_images.image LIKE '%.webp%'";
        $sql .= " ORDER BY pages.id ASC";
        $sql .= " LIMIT 0, 100";

        $all_animes = Main::sql($sql);
        foreach($all_animes as $anime){

            $id = $anime["id"];
            $name = $anime["name"];
            $id_page = $anime["id_page"];
            $img = $anime["image"];
            $data = date("Y-m-d H:i:s");

            $img = $anime["image"];
            $img = str_replace("r/167x242/", "", $img);
            $img = explode(".webp", $img);
            $img = reset($img);
            $file_type = ".jpg";
            $img = $img . $file_type;

            $f_target_path = "/backoffice/images/backend/pages/images/";
            $data_2 = reset(explode(" ", $data));
            $codigo_imagem = random_code();
            $nome_imagem = $id . "-" . $codigo_imagem . "_" . $data_2;

            $target_path = $f_target_path . $nome_imagem . $file_type;
            $server_img = $_SERVER["DOCUMENT_ROOT"] . $target_path;

            $fields_image = array(
                "id" => $id,
                "name" => $name,
                "id_page" => $id_page,
                "image" => $anime["image"],
                "new_img_url" => $img,
                "image_name" => $nome_imagem,
                "path" => $target_path,
                "full_path" => $server_img,
                "date" => $data,
            );
            array_push($all, $fields_image);
        }

        debug(date("Y-m-d H:i:s"));
        debug($all[0]["id"]);

        foreach($all as $each){
            if(file_put_contents($each["full_path"], file_get_contents($each["new_img_url"]))) {
                $new_image_path = $each["image_name"] . ".jpg";

                $fields_image = array(
                    "image" => $new_image_path,
                    "created_at" => $each["date"],
                );
                //$update_image = Main::update_where("pages_images", $fields_image, "id_page = " . $each["id_page"]);

                if(!$update_image){
                    $warnings[] = "[" . $each["id"] . "] == Update Error";
                }
            }else{
                $warnings[] = "[" . $each["id"] . "] == Upload Error";
            }
        }
        debug($each["id"]);

        /*$all_animes = Main::get("pages_images", "image LIKE '%.webp%' ORDER BY id_page ASC LIMIT 0, 50");
        foreach($all_animes as $anime){

            $id_image = $anime["id"];
            $id_page = $anime["id_page"];

            $data = date("Y-m-d H:i:s");

            $img_web_url = $anime["image"];
            $img_web_url = str_replace("r/167x242/", "", $img_web_url);
            $img_web_url = explode(".webp", $img_web_url);
            $img_web_url = reset($img_web_url);
            $img_web_url = $img_web_url . ".jpg";

            $file_type = end(explode(".", $img_web_url));

            $f_target_path = "/backoffice/images/backend/pages/images/";
            //$f_target_path = "/images/test/";
            $data_2 = reset(explode(" ", $data));
            $codigo_imagem = random_code();
            $nome_imagem = $id_image . "_" . $codigo_imagem . "_" . $data_2;
            $target_path = $f_target_path . $nome_imagem . "." . $file_type;

            $img = $_SERVER["DOCUMENT_ROOT"] . $target_path;
            //file_put_contents($img, file_get_contents($img_web_url));
            //debug($img);

            if (file_put_contents($img, file_get_contents($img_web_url))) {
                $new_image_path = $nome_imagem . "." . $file_type;
                $fields_image = array(
                    "image" => $new_image_path,
                    "created_at" => $data,
                );
                $update_image = Pages::update_image($fields_image, $id_image);

                $fields_teste = array(
                    "id" => $id_image,
                    "id_page" => $id_page,
                    "image" => $new_image_path,
                    "cpanel_path" => $img,
                    "created_at" => $data,
                );
                debug($fields_teste);
                echo "<br>";

                if(!$update_image){
                    $warnings [] = "[" . $id_image . "] == Update Error";
                }
            } else{
                $warnings [] = "[" . $id_image . "] == Upload Error";
            }
        }
        */


        if(empty($warnings)){
            debug("Image updating complete");
        }else{
            debug($warnings);
        }


        //543594bfebc3563ce1849f9f2170fb7a

        /*
         * SELECT pages.id, pages.name, pages_images.id_page, pages_images.image FROM pages INNER JOIN pages_images ON pages.id = pages_images.id_page WHERE pages_images.image LIKE '%.webp%' ORDER BY `pages`.`id` ASC
         * SELECT * FROM `pages_images` WHERE `image` NOT LIKE '%https://cdn.myanimelist%' AND `image` != "" ORDER BY `pages_images`.`id_page` DESC
         * cdn.myanimelist
         * */
        ?>

    </div>
</div>


<?
include 'fim.php';
?>



