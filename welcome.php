<?
include 'inicio.php';

if (Blogs::check_login() == false){

    if (isset($_POST["access"])){

    }elseif(isset($_POST["register"])){

    }

    ?>
    <!-- https://charat.me/#index-contents -->
    <div class="bloco">
        <div class="container">
            <div class="col-md-3"></div>

            <div class="col-md-6 center modal_full">
                <div class="modal_block">

                    <div class="top_block">
                        <div class="tab_opener <?= ($post["register"] == "true") ? "" : "active"?>" show="login">Login</div>
                        <div class="tab_opener <?= ($post["register"] == "true") ? "active" : ""?>" show="register">Sign Up</div>
                    </div>

                    <?
                    if($warning){
                        ?>
                        <div class="msg_warning"><?= $warning?></div>
                        <?
                    }
                    ?>

                    <div class="tab <?= ($post["register"] == "true") ? "disp_none" : ""?>" show="login">

                        <form id="form_access" method="post" action="">
                            <input type="hidden" name="access" value="true">

                            <div class="center">
                                <div class="">
                                    <div class="block_field disp_inline_flex">
                                        <div class="label_field">Username</div>
                                        <input class="input_field" type="text" name="name" placeholder="Enter username" />
                                    </div>
                                </div>

                                <div class="">
                                    <div class="block_field disp_inline_flex">
                                        <div class="label_field">Password</div>
                                        <input class="input_field" type="password" name="password" placeholder="Enter password" />
                                    </div>
                                </div>
                            </div>

                            <div class="center">
                                <button type="submit">Login</button>
                            </div>
                        </form>

                        <div class="forgot_password" onclick="location.href='reset_password';">&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;Forgot Password?</div>

                    </div>

                    <div class="tab <?= ($post["register"] == "true") ? "" : "disp_none"?>" show="register">

                        <form id="form_register" method="post" action="">
                            <input type="hidden" name="register" value="true">

                            <div class="center">
                                <div class="">
                                    <div class="block_field disp_inline_flex">
                                        <div class="label_field">Username</div>
                                        <input class="input_field" type="text" name="reg_name" placeholder="Enter username" />
                                    </div>
                                </div>

                                <div class="">
                                    <div class="block_field disp_inline_flex">
                                        <div class="label_field">Email</div>
                                        <input class="input_field" type="email" name="reg_email" placeholder="Email account" />
                                    </div>
                                </div>

                                <div class="">
                                    <div class="block_field disp_inline_flex">
                                        <div class="label_field">Password</div>
                                        <input class="input_field" type="password" name="reg_password" placeholder="Enter password" />
                                    </div>
                                </div>

                                <div class="">
                                    <div class="block_field disp_inline_flex">
                                        <div class="label_field">Repeat Password</div>
                                        <input class="input_field" type="password" name="reg_password_2" placeholder="Rewrite password" />
                                    </div>
                                </div>

                            </div>

                            <div class="center">
                                <button type="submit">Register</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <?
}else{
    ?>
    <script>
        window.location.replace("animes");
    </script>
    <?
}

include 'fim.php';
?>


