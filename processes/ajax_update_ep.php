<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_anime']) || isset($_POST['new_ep'])){
    $id_anime = trim(strip_tags($_POST['id_anime']));
    $id_anime = str_replace("'", "''", $id_anime);

    $new_ep = trim(strip_tags($_POST['new_ep']));
    $new_ep = str_replace("'", "''", $new_ep);

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log LIKE '%[$cookie];%' AND status = '1'");
    $user_id = $user[0]["id"];

    $anime = Pages::get_by_id($id_anime);
    $name = $anime["name"];
    $total_eps = $anime["nb_eps"];

    $data = date("Y-m-d H:i:s");

    if($total_eps <= $new_ep){
        $fields = array(
            "eps_seen" => $total_eps,
            "id_list" => 3,
            "updated_at" => $data,
        );

        $change_lists = true;

    }else{
        $fields = array(
            "eps_seen" => $new_ep,
            "updated_at" => $data,
        );

        $change_lists = false;
    }

    $update_eps = Main::update_where("blogs_users_animes", $fields,  "id_user = $user_id AND id_anime = $id_anime");
    if(!$update_eps){
        alert("Failed to update the last episode seen on '" . $name . "'");
    }else{
        if($change_lists == true){
            ?>
            <script>
                $(".type_list_label").addClass("disp_none");
                $(".type_list_label[id_list='3']").removeClass("disp_none");
                $(".btn_add_to_list i").removeClass();
                $(".btn_add_to_list i").addClass("fa fa-check-circle");

                $(".personal_eps").addClass("disp_none");
            </script>
            <?
        }
    }


}
?>
