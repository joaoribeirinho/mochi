<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions.php");

$post = $_POST;

//debug($post);

$warnings = array();

$form = Forms::get_by_id(1);

// Nome
if(isset($post["name"]) && !empty($post["name"])){
    $nome = trim(strip_tags($post["name"]));
}else{
    $warnings [] = "You have to insert your name";
    $nome = null;
}

// Email
if(isset($post["email"]) && !empty($post["email"]) && filter_var($post["email"], FILTER_VALIDATE_EMAIL)){
    $email = trim(strip_tags($post["email"]));
}else{
    $warnings [] = "You have to insert a valid email";
    $email = null;
}

// Telefone
if(isset($post["phone"]) && !empty($post["phone"])){
    $telefone = trim(strip_tags($post["phone"]));
}else{
    $warnings [] = "You have to insert your phone number";
    $telefone = null;
}

// Telefone
if(isset($post["subject"]) && !empty($post["subject"])){
    $assunto = trim(strip_tags($post["subject"]));
}else{
    $warnings [] = "You have to insert the subject";
    $assunto = null;
}

// Mensagem
if(isset($post["message"]) && !empty($post["message"])){
    $mensagem = $post["message"];
}else{
    $warnings [] = "You have to insert your message";
    $mensagem = null;
}

if(count($warnings) <= 0){

    $ip = getIp();

    $geo = geo_local();
    $local = $geo["city"] . ", " . $geo["country"];

    $data = date("Y-m-d H:i:s");

    if($_SERVER["HTTPS"] == "on"){
        $link_logo = "https://";
    }else{
        $link_logo = "http://";
    }
    $link_logo .= $_SERVER["HTTP_HOST"] . "/images/logo.png";

    $fields_registo = array(
        "id_form" => $form["id"],
        "name" => $nome,
        "email" => $email,
        "subject" => $assunto,
        "msg" => $mensagem,
        "ip" => $ip,
        "city" => $local,
        "created_at" => $data,
    );

    $variables = array(
        "]website[" => $_SERVER["HTTP_HOST"],
        "]name[" => $nome,
        "]email[" => $email,
        "]phone[" => $telefone,
        "]subject[" => $assunto,
        "]message[" => $mensagem,
        "]date[" => date("Y-m-d"),
        "]time[" => date("H:i:s"),
        "]logo-website[" => "<img style='width: 100px;' src='" . $link_logo ."'  />"
    );

    $register = Forms::register_contact($fields_registo, $variables);

    if ($register) {
        //debug($variables);
        $result = $form["success_msg"];
    }else{
        $result = $form["error_msg"];
    }

}else{
    foreach($warnings as $warning) {
        $msg .= $warning ."<br>";
    }

    $result = $msg;
}

echo $result;
