<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_comment'])){
    $id_comment = trim(strip_tags($_POST['id_comment']));
    $id_comment = str_replace("'", "''", $id_comment);

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log LIKE '%[$cookie];%' AND status = '1'");
    $user_id = $user[0]["id"];
    $points = $user[0]["points"];

    $check = Main::get("blogs_comments_likes", "id_comment = $id_comment AND id_user = $user_id");
    if($check){

        $remove_like = Main::delete("blogs_comments_likes", "id_comment = $id_comment AND id_user = $user_id");
        if($remove_like){

            $points_per_like = Main::get("blogs_forum_points", false);
            $points_per_like = $points_per_like[0]["pp_like"];

            $points_updated = $points - $points_per_like;
            $fields = array(
                "points" => $points_updated,
            );
            $remove_points = Blogs::update_user($fields, $user_id);

            ?>
            <script>
                $(".comment .details i[id_comment='<?= $id_comment?>']").removeClass("fa-thumbs-up");
                $(".comment .details i[id_comment='<?= $id_comment?>']").addClass("fa-thumbs-o-up");

                $(".comment .details i[id_comment='<?= $id_comment?>']").removeClass("remove_like");
                $(".comment .details i[id_comment='<?= $id_comment?>']").addClass("add_like");

                var upvotes = $(".details .upvotes[id_comment='<?= $id_comment?>']").html();
                var new_upvotes = parseInt(upvotes) - 1;
                $(".details .upvotes[id_comment='<?= $id_comment?>']").html(new_upvotes);
            </script>
            <?
        }
    }


}
?>
