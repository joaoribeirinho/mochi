<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_anime']) || isset($_POST['comment'])){
    $id_anime = trim(strip_tags($_POST['id_anime']));
    $id_anime = str_replace("'", "''", $id_anime);

    $comment = trim($_POST['comment']);

    $profanity_proof = profanity($comment);
    if(!$profanity_proof){

        //details of user
        $cookie = $_COOKIE["anime_log"];
        $user = Blogs::get_users("key_log LIKE '%[$cookie];%' AND status = '1'");
        $user_id = $user[0]["id"];
        $points = $user[0]["points"];

        $anime = Pages::get_by_id($id_anime);
        $name = $anime["name"];

        $data = date("Y-m-d H:i:s");

        $fields = array(
            "id_blog" => $id_anime,
            "id_user" => $user_id,
            "content" => $comment,
            "status" => 1,
            "created_at" => $data,
        );

        $add_comment = Main::add("blogs_comments", $fields,  true);
        if($add_comment){

            $points_per_comment = Main::get("blogs_forum_points", false);
            $points_per_comment = $points_per_comment[0]["pp_comment"];

            $points_updated = $points + $points_per_comment;
            $fields = array(
                "points" => $points_updated,
            );
            $add_points = Blogs::update_user($fields, $user_id);

            ?>
            <script>window.location.reload();</script>
            <?
        }else{
            $warnings = "Something went wrong and we weren't able to save your feedback. Try again please.";
            echo $warnings;
        }

    }else{
        $warnings = "Your comment contains 'dirty words', so we consider it profanity. Please revise your feedback";
        echo $warnings;
    }

}
?>
