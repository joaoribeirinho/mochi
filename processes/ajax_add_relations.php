<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_anime']) || isset($_POST['my_ids']) || isset($_POST['add_type'])){
    $id_anime = trim(strip_tags($_POST['id_anime']));
    $id_anime = str_replace("'", "''", $id_anime);

    $add_type = trim(strip_tags($_POST['add_type']));
    $add_type = str_replace("'", "''", $add_type);

    $my_ids = trim(strip_tags($_POST['my_ids']));
    $my_ids = str_replace("'", "''", $my_ids);

    $data = date("Y-m-d H:i:s");

    if($add_type == "prequels"){
        $additional_id = 2;
    }elseif($add_type == "sequels"){
        $additional_id = 3;
    }elseif($add_type == "others"){
        $additional_id = 4;
    }

    $check = Main::get("additional_contents", "additional_id = $additional_id AND module_content_id = $id_anime");
    if($check){
        $check_id = $check[0]["id"];
        $check_content = $check[0]["content"];

        $fields = array(
            "content" => $check_content . $my_ids,
            "updated_at" => $data,
        );
        //debug($fields);
        $update = Main::update("additional_contents", $fields, $check_id);
        if($update){
            alert("Updated with success!");
        }
    }else{
        $fields = array(
            "additional_id" => $additional_id,
            "module_content_id" => $id_anime,
            "content" => $my_ids,
            "created_at" => $data,
        );
        //debug($fields);
        $add = Main::add("additional_contents", $fields, true);
        if($add){
            alert("Added with success!");
        }
    }
}
?>
