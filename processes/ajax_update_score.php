<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_anime']) || isset($_POST['score'])){
    $id_anime = trim(strip_tags($_POST['id_anime']));
    $id_anime = str_replace("'", "''", $id_anime);

    $score = trim(strip_tags($_POST['score']));
    $score = str_replace("'", "''", $score);

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log LIKE '%[$cookie];%' AND status = '1'");
    $user_id = $user[0]["id"];

    $anime = Pages::get_by_id($id_anime);
    $name = $anime["name"];

    $data = date("Y-m-d H:i:s");

    $fields = array(
        "score" => $score,
        "updated_at" => $data,
    );

    $update_score = Main::update_where("blogs_users_animes", $fields,  "id_user = $user_id AND id_anime = $id_anime");

    if(!$update_score){
        alert("Failed to update the score seen on '" . $name . "'");
    }
}
?>
