<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_comment'])){
    $id_comment = trim(strip_tags($_POST['id_comment']));
    $id_comment = str_replace("'", "''", $id_comment);

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log LIKE '%[$cookie];%' AND status = '1'");
    $user_id = $user[0]["id"];
    $points = $user[0]["points"];

    $remove_comment = Main::delete("blogs_comments", "id = $id_comment AND id_user = $user_id");
    if($remove_comment){

        $remove_likes = Main::delete("blogs_comments_likes", "id_comment = $id_comment");
        if($remove_likes){

            $points_per_comment = Main::get("blogs_forum_points", false);
            $points_per_comment = $points_per_comment[0]["pp_comment"];

            $points_updated = $points - $points_per_comment;
            $fields = array(
                "points" => $points_updated,
            );
            $remove_points = Blogs::update_user($fields, $user_id);

            ?>
            <script>
                $(".comment[id_comment='<?= $id_comment?>']").remove();
            </script>
            <?
        }else{
            alert("Problem deleting likes associated to the deleted feedback");
        }
    }else{
        alert("Problem deleting the feedback");
    }
}
?>
