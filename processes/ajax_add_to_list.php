<?
//require_once __DIR__ . '/../functions.php';
include($_SERVER['DOCUMENT_ROOT'] . "/functions_front.php");

if(isset($_POST['id_anime']) || isset($_POST['id_list'])){
    $id_anime = trim(strip_tags($_POST['id_anime']));
    $id_anime = str_replace("'", "''", $id_anime);

    $id_list = trim(strip_tags($_POST['id_list']));
    $id_list = str_replace("'", "''", $id_list);

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log LIKE '%[$cookie];%' AND status = '1'");
    $user_id = $user[0]["id"];

    $anime = Pages::get_by_id($id_anime);
    $name = $anime["name"];

    $data = date("Y-m-d H:i:s");


    //
    $check_if_exists_on_list = Main::get("blogs_users_animes", "id_anime = $id_anime AND id_user = $user_id");

    if($check_if_exists_on_list){
        $fields = array(
            "id_list" => $id_list,
            "updated_at" => $data,
        );

        $change_lists = Main::update_where("blogs_users_animes", $fields,  "id_user = $user_id AND id_anime = $id_anime");
        if(!$change_lists){
            alert("Failed to change '" . $name . "' to another list");
        }

    }else{
        $fields = array(
            "id_user" => $user_id,
            "id_anime" => $id_anime,
            "id_list" => $id_list,
            "created_at" => $data,
        );

        $change_lists = Main::add("blogs_users_animes", $fields,  true);
        if(!$change_lists){
            alert("Failed to add '" . $name . "' to the list");
        }
    }


}
?>
