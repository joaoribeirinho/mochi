<?
include 'inicio.php';
?>

<div class="fakebanner" style="background-image: url('images/kimi2.jpg')"></div>

<div class="bloco first_bloco">
    <div class="container">
        <div class="all_anime" id="search_target">
            <div class="row">
                <div class="anime_types">
                    <?
                    $get_lists = array("TV", "Special", "Movie", "ONA", "OVA");
                    foreach($get_lists as $type){
                        switch($type) {
                            case "TV":
                                $bg = "matcha";
                                $active = "active";
                                break;
                            case "ONA":
                                $bg = "durian";
                                $active = "";
                                break;
                            case "OVA":
                                $bg = "sesame";
                                $active = "";
                                break;
                            case "Special":
                                $bg = "redbeanpaste";
                                $active = "";
                                break;
                            case "Movie":
                                $bg = "iris";
                                $active = "";
                                break;
                            default:
                                $bg = "matcha";
                                $active = "";
                                break;
                        }
                        ?>
                        <span class="each_type <?= $bg?> <?= $active?>" type="<?= $type?>"><?= $type?></span>
                        <?
                    }
                    ?>
                </div>
            </div>
            <br><br>

            <?
            $season = $_GET["season"];
            if($season){
                $year_url = reset(explode("_", $season));

                $station_url = end(explode("_", $season));
                $station_url = ucwords($station_url);

                if($station_url == "Winter"){
                    $station_url = "-[A]" . $station_url;
                }elseif($station_url == "Spring"){
                    $station_url = "-[B]" . $station_url;
                }elseif($station_url == "Summer"){
                    $station_url = "-[C]" . $station_url;
                }elseif($station_url == "Fall"){
                    $station_url = "-[D]" . $station_url;
                }

                $season = $year_url . $station_url;

            }else{
                $season = "2019-[C]Summer";

                $year_url = reset(explode("-", $season));
                $station_url = "-" . end(explode("-", $season));
            }

            $get_animes = Pages::get("status = 1 AND id_category = 1 AND season = '$season'", "name ASC");

            $type_desired = "TV";
            $class = "matcha";
            ?>
            <div class="row anime_type_holder" type="<?= $type_desired?>">
                <?
                foreach($get_animes as $anime){
                    $name = $anime["name"];
                    $id = $anime["id"];
                    $type = $anime["type"];
                    $studios = explode(";", $anime["studios_id"]);
                    $genres = explode(";", $anime["genres_id"]);

                    $image = Pages::get_image($id,"description ASC");

                    if($image){
                        $image_path = Pages::image_path($image[0]["image"]);
                    }else{
                        $image_path = "images/no_image.jpg";
                    }

                    //check if there is hentai to block
                    $censured = false;
                    foreach($genres as $genre){
                        if($genre == 12){
                            $censured = true;
                        }
                    }

                    /*check data if anime on any list*/
                    if($login){
                        $user_id = $user["id"];
                        $check_if_on_any_list = Main::get("blogs_users_animes", "id_user = $user_id AND id_anime = $id");
                        $is_on_list = $check_if_on_any_list[0]["id_list"];

                        if($is_on_list){
                            $list_name = Main::get("anime_lists", "id = $is_on_list");
                            $list_name = $list_name[0]["name"];
                        }else{
                            $list_name = "";
                        }
                    }

                    if($type == $type_desired){
                        ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="row" style="overflow:unset;">
                                <div class="each_anime no_hover">
                                    <div class="col-md-6 col-xs-6">
                                        <?
                                        if($censured == true){
                                            ?>
                                            <div class="image" style="background-image: url('<?= $image_path?>')">
                                                <div class="overlay_18">
                                                    <img src="images/censured.png" />
                                                </div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="image" style="background-image: url('<?= $image_path?>')">
                                                    <?
                                                    if($list_name){
                                                        switch($list_name) {
                                                            case "Watching":
                                                                $class = "matcha";
                                                                break;
                                                            case "Interested In":
                                                                $class = "durian";
                                                                break;
                                                            case "Finished":
                                                                $class = "iris";
                                                                break;
                                                            case "On Hold":
                                                                $class = "redbeanpaste";
                                                                break;
                                                            default:
                                                                $class = "disp_none";
                                                        }

                                                        if(!$class){
                                                            $class = "disp_none";
                                                        }

                                                        ?>
                                                        <div class="on_list <?= $class?>"><?= $list_name?></div>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6 col-xs-6">
                                        <div class="body">

                                            <div class="type <?= $class?>"><?= $type?></div>

                                            <?
                                            foreach($studios as $studio){
                                                if($studio != ""){
                                                    $get_studio_name = Main::get("anime_studios", "id = $studio");
                                                    if($get_studio_name){
                                                        $each_studio_name = $get_studio_name[0]["name"];
                                                        ?>
                                                        <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                                            <div class="studio"><?= $each_studio_name?></div>
                                                        </a>
                                                        <?
                                                    }
                                                }
                                            }

                                            if($studios[0] != ""){
                                                ?>
                                                <br>
                                                <?
                                            }

                                            foreach($genres as $genre){
                                                if($genre != ""){
                                                    $get_genre_name = Main::get("anime_genres", "id = $genre");
                                                    if($get_genre_name){
                                                        $each_genre_name = $get_genre_name[0]["name"];

                                                        if($censured == true && $get_genre_name[0]["id"] == 12){
                                                            ?>
                                                            <div class="genre"><?= $each_genre_name?></div>
                                                            <?
                                                        }else{
                                                            ?>
                                                            <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                                                <div class="genre"><?= $each_genre_name?></div>
                                                            </a>
                                                            <?
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?
                                    if($censured != true){
                                        $synopsis = Main::get("anime_other_details", "id_anime = $id");
                                        if($synopsis){
                                            ?>
                                            <div class="overlay has_sinopsis" target="<?= $id?>">
                                                <a href="anime?id=<?= $id?>" title="<?= $name?>"><?= $name?></a>
                                            </div>

                                            <div class="synopsis_btn" target="<?= $id?>">
                                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            </div>

                                            <div class="synopsis disp_none" target="<?= $id?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                <div class="synopsis_inner_text"><?= $synopsis[0]["synopsis_mal"]?></div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <div class="overlay">
                                                <a href="anime?id=<?= $id?>" title="<?= $name?>"><?= $name?></a>
                                            </div>
                                            <?
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                        <?
                    }

                }
                ?>
            </div>
            <?

            $type_desired = "Special";
            $class = "redbeanpaste";
            ?>
            <div class="row anime_type_holder disp_none" type="<?= $type_desired?>">
                <?
                foreach($get_animes as $anime){
                    $name = $anime["name"];
                    $id = $anime["id"];
                    $type = $anime["type"];
                    $studios = explode(";", $anime["studios_id"]);
                    $genres = explode(";", $anime["genres_id"]);

                    $image = Pages::get_image($id,"description ASC");
                    $image_path = $image[0]["image"];

                    //check if there is hentai to block
                    $censured = false;
                    foreach($genres as $genre){
                        if($genre == 12){
                            $censured = true;
                        }
                    }

                    if($type == $type_desired){
                        ?>
                        <div class="col-md-4 col-sm-6 scaler">
                            <div class="row">
                                <div class="each_anime no_hover">
                                    <div class="col-md-6 col-sm-6">
                                        <?
                                        if($censured == true){
                                            ?>
                                            <div class="image" style="background-image: url('<?= $image_path?>')">
                                                <div class="overlay_18">
                                                    <img src="images/censured.png" />
                                                </div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="image" style="background-image: url('<?= $image_path?>')"></div>
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="body">

                                            <div class="type <?= $class?>"><?= $type?></div>

                                            <?
                                            foreach($studios as $studio){
                                                if($studio != ""){
                                                    $get_studio_name = Main::get("anime_studios", "id = $studio");
                                                    if($get_studio_name){
                                                        $each_studio_name = $get_studio_name[0]["name"];
                                                        ?>
                                                        <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                                            <div class="studio"><?= $each_studio_name?></div>
                                                        </a>
                                                        <?
                                                    }
                                                }
                                            }

                                            if($studios[0] != ""){
                                                ?>
                                                <br>
                                                <?
                                            }

                                            foreach($genres as $genre){
                                                if($genre != ""){
                                                    $get_genre_name = Main::get("anime_genres", "id = $genre");
                                                    if($get_genre_name){
                                                        $each_genre_name = $get_genre_name[0]["name"];

                                                        if($censured == true && $get_genre_name[0]["id"] == 12){
                                                            ?>
                                                            <div class="genre"><?= $each_genre_name?></div>
                                                            <?
                                                        }else{
                                                            ?>
                                                            <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                                                <div class="genre"><?= $each_genre_name?></div>
                                                            </a>
                                                            <?
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?
                                    if($censured != true){
                                        $synopsis = Main::get("anime_other_details", "id_anime = $id");
                                        if($synopsis){
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?> has_sinopsis" target="<?= $id?>"><?= $name?></div>
                                            </a>

                                            <div class="synopsis_btn <?= $class?>" target="<?= $id?>">
                                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            </div>

                                            <div class="synopsis <?= $class?> disp_none" target="<?= $id?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                <div class="synopsis_inner_text"><?= $synopsis[0]["synopsis_mal"]?></div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?>"><?= $name?></div>
                                            </a>
                                            <?
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                        <?
                    }

                }
                ?>
            </div>
            <?

            $type_desired = "Movie";
            $class = "iris";
            ?>
            <div class="row anime_type_holder disp_none" type="<?= $type_desired?>">
                <?
                foreach($get_animes as $anime){
                    $name = $anime["name"];
                    $id = $anime["id"];
                    $type = $anime["type"];
                    $studios = explode(";", $anime["studios_id"]);
                    $genres = explode(";", $anime["genres_id"]);

                    $image = Pages::get_image($id,"description ASC");
                    $image_path = $image[0]["image"];

                    //check if there is hentai to block
                    $censured = false;
                    foreach($genres as $genre){
                        if($genre == 12){
                            $censured = true;
                        }
                    }

                    if($type == $type_desired){
                        ?>
                        <div class="col-md-4 col-sm-6 scaler">
                            <div class="row">
                                <div class="each_anime no_hover">
                                    <div class="col-md-6 col-sm-6">
                                        <?
                                        if($censured == true){
                                            ?>
                                            <div class="image" style="background-image: url('<?= $image_path?>')">
                                                <div class="overlay_18">
                                                    <img src="images/censured.png" />
                                                </div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="image" style="background-image: url('<?= $image_path?>')"></div>
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="body">

                                            <div class="type <?= $class?>"><?= $type?></div>

                                            <?
                                            foreach($studios as $studio){
                                                if($studio != ""){
                                                    $get_studio_name = Main::get("anime_studios", "id = $studio");
                                                    if($get_studio_name){
                                                        $each_studio_name = $get_studio_name[0]["name"];
                                                        ?>
                                                        <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                                            <div class="studio"><?= $each_studio_name?></div>
                                                        </a>
                                                        <?
                                                    }
                                                }
                                            }

                                            if($studios[0] != ""){
                                                ?>
                                                <br>
                                                <?
                                            }

                                            foreach($genres as $genre){
                                                if($genre != ""){
                                                    $get_genre_name = Main::get("anime_genres", "id = $genre");
                                                    if($get_genre_name){
                                                        $each_genre_name = $get_genre_name[0]["name"];

                                                        if($censured == true && $get_genre_name[0]["id"] == 12){
                                                            ?>
                                                            <div class="genre"><?= $each_genre_name?></div>
                                                            <?
                                                        }else{
                                                            ?>
                                                            <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                                                <div class="genre"><?= $each_genre_name?></div>
                                                            </a>
                                                            <?
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?
                                    if($censured != true){
                                        $synopsis = Main::get("anime_other_details", "id_anime = $id");
                                        if($synopsis){
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?> has_sinopsis" target="<?= $id?>"><?= $name?></div>
                                            </a>

                                            <div class="synopsis_btn <?= $class?>" target="<?= $id?>">
                                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            </div>

                                            <div class="synopsis <?= $class?> disp_none" target="<?= $id?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                <div class="synopsis_inner_text"><?= $synopsis[0]["synopsis_mal"]?></div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?>"><?= $name?></div>
                                            </a>
                                            <?
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                        <?
                    }

                }
                ?>
            </div>
            <?

            $type_desired = "ONA";
            $class = "durian";
            ?>
            <div class="row anime_type_holder disp_none" type="<?= $type_desired?>">
                <?
                foreach($get_animes as $anime){
                    $name = $anime["name"];
                    $id = $anime["id"];
                    $type = $anime["type"];
                    $studios = explode(";", $anime["studios_id"]);
                    $genres = explode(";", $anime["genres_id"]);

                    $image = Pages::get_image($id,"description ASC");
                    $image_path = $image[0]["image"];

                    //check if there is hentai to block
                    $censured = false;
                    foreach($genres as $genre){
                        if($genre == 12){
                            $censured = true;
                        }
                    }

                    if($type == $type_desired){
                        ?>
                        <div class="col-md-4 col-sm-6 scaler">
                            <div class="row">
                                <div class="each_anime no_hover">
                                    <div class="col-md-6 col-sm-6">
                                        <?
                                        if($censured == true){
                                            ?>
                                            <div class="image" style="background-image: url('<?= $image_path?>')">
                                                <div class="overlay_18">
                                                    <img src="images/censured.png" />
                                                </div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="image" style="background-image: url('<?= $image_path?>')"></div>
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="body">

                                            <div class="type <?= $class?>"><?= $type?></div>

                                            <?
                                            foreach($studios as $studio){
                                                if($studio != ""){
                                                    $get_studio_name = Main::get("anime_studios", "id = $studio");
                                                    if($get_studio_name){
                                                        $each_studio_name = $get_studio_name[0]["name"];
                                                        ?>
                                                        <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                                            <div class="studio"><?= $each_studio_name?></div>
                                                        </a>
                                                        <?
                                                    }
                                                }
                                            }

                                            if($studios[0] != ""){
                                                ?>
                                                <br>
                                                <?
                                            }

                                            foreach($genres as $genre){
                                                if($genre != ""){
                                                    $get_genre_name = Main::get("anime_genres", "id = $genre");
                                                    if($get_genre_name){
                                                        $each_genre_name = $get_genre_name[0]["name"];

                                                        if($censured == true && $get_genre_name[0]["id"] == 12){
                                                            ?>
                                                            <div class="genre"><?= $each_genre_name?></div>
                                                            <?
                                                        }else{
                                                            ?>
                                                            <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                                                <div class="genre"><?= $each_genre_name?></div>
                                                            </a>
                                                            <?
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?
                                    if($censured != true){
                                        $synopsis = Main::get("anime_other_details", "id_anime = $id");
                                        if($synopsis){
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?> has_sinopsis" target="<?= $id?>"><?= $name?></div>
                                            </a>

                                            <div class="synopsis_btn <?= $class?>" target="<?= $id?>">
                                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            </div>

                                            <div class="synopsis <?= $class?> disp_none" target="<?= $id?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                <div class="synopsis_inner_text"><?= $synopsis[0]["synopsis_mal"]?></div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?>"><?= $name?></div>
                                            </a>
                                            <?
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                        <?
                    }

                }
                ?>
            </div>
            <?

            $type_desired = "OVA";
            $class = "sesame";
            ?>
            <div class="row anime_type_holder disp_none" type="<?= $type_desired?>">
                <?
                foreach($get_animes as $anime){
                    $name = $anime["name"];
                    $id = $anime["id"];
                    $type = $anime["type"];
                    $studios = explode(";", $anime["studios_id"]);
                    $genres = explode(";", $anime["genres_id"]);

                    $image = Pages::get_image($id,"description ASC");
                    $image_path = $image[0]["image"];

                    //check if there is hentai to block
                    $censured = false;
                    foreach($genres as $genre){
                        if($genre == 12){
                            $censured = true;
                        }
                    }

                    if($type == $type_desired){
                        ?>
                        <div class="col-md-4 col-sm-6 scaler">
                            <div class="row">
                                <div class="each_anime no_hover">
                                    <div class="col-md-6 col-sm-6">
                                        <?
                                        if($censured == true){
                                            ?>
                                            <div class="image" style="background-image: url('<?= $image_path?>')">
                                                <div class="overlay_18">
                                                    <img src="images/censured.png" />
                                                </div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="image" style="background-image: url('<?= $image_path?>')"></div>
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="body">

                                            <div class="type <?= $class?>"><?= $type?></div>

                                            <?
                                            foreach($studios as $studio){
                                                if($studio != ""){
                                                    $get_studio_name = Main::get("anime_studios", "id = $studio");
                                                    if($get_studio_name){
                                                        $each_studio_name = $get_studio_name[0]["name"];
                                                        ?>
                                                        <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                                            <div class="studio"><?= $each_studio_name?></div>
                                                        </a>
                                                        <?
                                                    }
                                                }
                                            }

                                            if($studios[0] != ""){
                                                ?>
                                                <br>
                                                <?
                                            }

                                            foreach($genres as $genre){
                                                if($genre != ""){
                                                    $get_genre_name = Main::get("anime_genres", "id = $genre");
                                                    if($get_genre_name){
                                                        $each_genre_name = $get_genre_name[0]["name"];

                                                        if($censured == true && $get_genre_name[0]["id"] == 12){
                                                            ?>
                                                            <div class="genre"><?= $each_genre_name?></div>
                                                            <?
                                                        }else{
                                                            ?>
                                                            <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                                                <div class="genre"><?= $each_genre_name?></div>
                                                            </a>
                                                            <?
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?
                                    if($censured != true){
                                        $synopsis = Main::get("anime_other_details", "id_anime = $id");
                                        if($synopsis){
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?> has_sinopsis" target="<?= $id?>"><?= $name?></div>
                                            </a>

                                            <div class="synopsis_btn <?= $class?>" target="<?= $id?>">
                                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            </div>

                                            <div class="synopsis <?= $class?> disp_none" target="<?= $id?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                <div class="synopsis_inner_text"><?= $synopsis[0]["synopsis_mal"]?></div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="overlay <?= $class?>"><?= $name?></div>
                                            </a>
                                            <?
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                        <?
                    }

                }
                ?>
            </div>
        </div>
    </div>
</div>

<div id="btn_open_sorting">
    <i class="fa fa-sliders" aria-hidden="true"></i>
</div>

<div id="sorting_bar">
    <div class="filters_season">

        <?
        $array_stations = array("-[A]Winter", "-[B]Spring", "-[C]Summer", "-[D]Fall");
        $array_years = array();

        $sql = "SELECT season FROM pages WHERE id_category = 1 AND status = 1";
        $sql .= " GROUP BY season ORDER BY season DESC";
        $get_seasons = Main::sql($sql);
        foreach ($get_seasons as $season){
            $year = $season["season"];
            $array_years[] = reset(explode("-", $year));
        }
        $array_years = array_unique($array_years);
        //sort($array_years)
        ?>

        <div class="each_filter">
            <select class="select_stations" name="stations">
                <option value="">Select Season</option>
                <?
                foreach($array_stations as $station){
                    $station_name = end(explode("]", $station));
                    ?>
                    <option value="<?= $station?>" <?= ($station_url == $station) ? "selected" : ""?> style="color: green !important;"><?= $station_name?></option>
                    <?
                }
                ?>

            </select>
        </div>

        <div class="each_filter">
            <select class="select_years" name="years">
                <option value="">Select Year</option>
                <?
                foreach($array_years as $year){
                    ?>
                    <option value="<?= $year?>" <?= ($year_url == $year) ? "selected" : ""?> style="color: green !important;"><?= $year?></option>
                    <?
                }
                ?>

            </select>
        </div>

    </div>
</div>

<?
include 'fim.php';
?>



