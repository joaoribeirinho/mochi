<head>

    <meta charset="utf-8" />

    <?
    $get_url = $_SERVER["REQUEST_URI"];
    $get_url = str_replace("/", "", $get_url);
    $get_url2 = str_replace("_", " ", $get_url);
    $pagina = ucwords($get_url2);
    $pagina = reset(explode("?", $pagina));

    if($pagina == "Anime"){
        $id_url = $_GET["id"];
        $anime = Pages::get_by_id($id_url);
        $anime_name = $anime["name"];
        $pagina = $anime_name;
    }elseif($pagina == "Nakama"){
        $id_url = $_GET["id"];
        $nakama = Blogs::get_user_by_id($id_url);
        $nakama_name = strtoupper($nakama["user"]);
        $nakama_name = " - " . $nakama_name;
    }
    ?>

    <title>Mochi | <?= $pagina . $nakama_name?></title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="Mochi - Anime List" name="description" />
    <meta content="Mochi" name="author" />
    <meta content="Mochi, Anime, List" name="keywords" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#292929">

    <!-- FACEBOOK metas -->
    <meta property="og:title" content="Mochi"/>
    <meta property="og:image" content="https://jr-dev.pt/images/logo.png"/>
    <meta property="og:site_name" content="Mochi"/>
    <meta property="og:description" content="Mochi - Anime List"/>

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="js/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="js/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />

    <?/*<link href="js/plugins/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />*/?>
    <link href="js/plugins/owlcarousel/owl.carousel.css" rel="stylesheet" />

    <!-- FAVICON -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Sunflower:300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="js/plugins/ClassyEdit/css/jquery.classyedit.css" />

    <?/*DataTables*/?>
    <?/*check: https://datatables.net*/?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <link href="css/cookie_bar.css" rel="stylesheet" type="text/css"/>
    <link href="css/custom.css<?= $versao?>" rel="stylesheet" type="text/css"/>


    <?
    if($pagina == "Login" || $pagina == "Logout"){
        ?>
        <link href="css/login.css<?= $versao?>" rel="stylesheet" type="text/css"/>
        <?
    }elseif ($pagina == "Welcome"){
        ?>
        <link href="css/login_reg.css<?= $versao?>" rel="stylesheet" type="text/css"/>
        <?
    }
    ?>

    <link href="css/animate.css" rel="stylesheet" type="text/css"/>

    <script src="js/modernizr.custom.56918.js" type="text/javascript"></script>
    <script src="js/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>

</head>

<body>
    <div class="whole_page">

    <?
    include 'cookies_bar.php';

    if(Blogs::check_login() == true) {
        $login = true;

        $user_logged = $_COOKIE["anime_log"];
        $get_user_logged = Blogs::get_users("key_log LIKE '%[$user_logged];%' AND status = 1");
        $get_user_logged_id = $get_user_logged[0]["id"];

        $user = $get_user_logged[0];
    }

    if($pagina != "Login" && $pagina != "Logout" && $pagina != "Extension"){
        include 'menu.php';
    }

    if($pagina == "Welcome"){

        $rand_banner = Banners::get("id_category = 1 AND status = 1", "RAND()", array(0, 1));
        $rand_banner = $rand_banner[0];
        $image = Banners::get_image($rand_banner["id"]);
        $image_path = Banners::image_path($image[0]["image"]);
        ?>
        <style>
            body{
                background-image: url('<?= $image_path?>') !important;
            }
        </style>
        <?
    }


    ?>
