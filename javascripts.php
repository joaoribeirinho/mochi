<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS(REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="/js/plugins/respond.min.js"></script>
<![endif]-->


<script type="text/javascript" src="js/plugins/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript" src="https://unpkg.com/popper.js@1.15.0/dist/umd/popper.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/js/bootstrap.js"></script>

<?/*new bootstrap*/?>
<!--<script type="text/javascript" src="js/plugins/bootstrap/new/js/bootstrap.min.js"></script>-->

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript" src="js/plugins/hover-dropdown.js"></script>
<script type="text/javascript" src="js/plugins/back-to-top.js"></script>

<script src="js/plugins/bxslider/jquery.bxslider.min.js"></script>
<script src="js/plugins/owlcarousel/owl.carousel.min.js"></script>
<script src="js/plugins/owlcarousel/owl.carousel2.thumbs.js"></script>

<script type="text/javascript" src="js/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<!-- simple text editor -->
<script src="js/plugins/ClassyEdit/js/jquery.classyedit.js"></script>

<?/*DataTables*/?>
<?/*check: https://datatables.net*/?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript" src="js/cookie_bar.js"></script>
<script src="js/main.js<?= $versao?>"></script>
<script src="js/wow.min.js"></script>



<?
$page = $_SERVER["REDIRECT_URL"];
$page = str_replace("/", "", $page);
$page = reset(explode("?", $page));



?>


