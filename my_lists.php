<?
include 'inicio.php';

if($login == false){
    go_to("welcome?url=my_lists");
}else{

    //details of user
    $user_id = $user["id"];
    $user_img = $user["image"];
    $username = $user["user"];
    $user_img_path = Blogs::user_image_path($user_img);


    //USER PREFERENCES
    $preferences = Main::get("blogs_users_preferences", "id_user = $user_id");
    $theme = $preferences[0]["theme"];
    $list_type = $preferences[0]["list_type"];


    if($_GET["list"]){
        $active_list_id = $_GET["list"];
    }else{
        $active_list_id = "1";
    }

    ?>
    <div class="fakebanner" style="background-image: url('images/kimi2.jpg')">
        <div class="active_label"><?= $username?></div>
    </div>

    <div class="bloco first_bloco">
        <div class="container">
            <div class="row">
                <div class="lists_names">
                    <?
                    $get_lists = Main::get("anime_lists");
                    foreach($get_lists as $list){
                        $list_id = $list["id"];
                        $list_name = $list["name"];

                        switch ($list["id"]){
                            case "1":
                                $bg = "matcha";
                                break;
                            case "2":
                                $bg = "durian";
                                break;
                            case "3":
                                $bg = "iris";
                                break;
                            case "4":
                                $bg = "redbeanpaste";
                                break;
                            default:
                                $bg = "matcha";
                                break;
                        }

                        if($active_list_id == $list_id){
                            $active = "active";
                        }else{
                            $active = "";
                        }

                        ?>
                        <span class="each_list <?= $bg?> <?= $active?>" id_list="<?= $list_id?>"><?= $list_name?></span>
                        <?
                    }
                    ?>
                </div>
            </div>

            <?
            foreach($get_lists as $list){
                $list_id = $list["id"];

                switch ($list["id"]){
                    case "1":
                        $bg = "matcha";
                        break;
                    case "2":
                        $bg = "durian";
                        break;
                    case "3":
                        $bg = "iris";
                        break;
                    case "4":
                        $bg = "redbeanpaste";
                        break;
                    default:
                        $bg = "matcha";
                        break;
                }

                if($active_list_id == $list_id){
                    $active = "";
                }else{
                    $active = "disp_none";
                }

                if($list_id == 1){
                    $mobile_disp_none = array(
                        "list_id" => $list_id,
                        "number" => "mobile_disp_none",
                        "image" => "mobile_disp_none",
                        "name" => "",
                        "type" => "mobile_disp_none",
                        "score" => "",
                        "episodes" => "",
                        "actions" => "",
                        "actions_eps" => "",
                    );
                }elseif($list_id == 2){
                    $mobile_disp_none = array(
                        "list_id" => $list_id,
                        "number" => "mobile_disp_none",
                        "image" => "",
                        "name" => "",
                        "type" => "mobile_disp_none",
                        "score" => "mobile_disp_none",
                        "episodes" => "mobile_disp_none",
                        "actions" => "",
                        "actions_eps" => "mobile_disp_none",
                    );
                }elseif($list_id == 3){
                    $mobile_disp_none = array(
                        "list_id" => $list_id,
                        "number" => "mobile_disp_none",
                        "image" => "",
                        "name" => "",
                        "type" => "mobile_disp_none",
                        "score" => "",
                        "episodes" => "mobile_disp_none",
                        "actions" => "",
                        "actions_eps" => "mobile_disp_none",
                    );
                }elseif($list_id == 4){
                    $mobile_disp_none = array(
                        "list_id" => $list_id,
                        "number" => "mobile_disp_none",
                        "image" => "mobile_disp_none",
                        "name" => "",
                        "type" => "mobile_disp_none",
                        "score" => "mobile_disp_none",
                        "episodes" => "",
                        "actions" => "",
                        "actions_eps" => "",
                    );
                }

                ?>
                <div class="row list_table <?= $active?>" id_list="<?= $list_id?>">
                    <table id="table_id_<?= $list_id?>" data-order='[[ 0, "asc" ]]' class="table_id_datatables <?= $bg?> responsive">
                        <thead>
                            <tr>
                                <th class="<?= $mobile_disp_none["number"]?>">Number</th>
                                <th class="<?= $mobile_disp_none["image"]?>">Image</th>
                                <th class="<?= $mobile_disp_none["name"]?>">Name</th>
                                <th class="<?= $mobile_disp_none["type"]?>">Type</th>
                                <th class="<?= $mobile_disp_none["score"]?>">Score</th>
                                <th class="<?= $mobile_disp_none["episodes"]?>">Episodes</th>
                                <th class="<?= $mobile_disp_none["actions"]?>">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?
                            $i = 1;
                            $get_animes = Main::get("blogs_users_animes", "id_user = $user_id AND id_list = $list_id", "id DESC");
                            foreach($get_animes as $get_anime){
                                $id_anime = $get_anime["id_anime"];
                                $eps_seen = $get_anime["eps_seen"];
                                $score = $get_anime["score"];

                                $anime = Pages::get_by_id($id_anime);
                                $name = $anime["name"];
                                $type = $anime["type"];
                                $total_eps = $anime["nb_eps"];
                                $total_eps = reset(explode(" ", $total_eps));

                                $image = Pages::get_image($id_anime,"description ASC");

                                if($image){
                                    $image_path = Pages::image_path($image[0]["image"]);
                                }else{
                                    $image_path = "images/no_image.jpg";
                                }

                                $anime_link = "anime?id=" . $id_anime;
                                ?>
                                <tr class="row_anime" anime="<?= $id_anime?>">
                                    <td class="<?= $mobile_disp_none["number"]?>"><?= $i?></td>
                                    <td class="<?= $mobile_disp_none["image"]?>">
                                        <a href="<?= $anime_link?>">
                                            <img style="height:50px" src="<?= $image_path?>" />
                                        </a>
                                    </td>

                                    <td class="no_hover listed_anime_name <?= $mobile_disp_none["name"]?>">
                                        <a href="<?= $anime_link?>"><?= $name?></a>
                                    </td>

                                    <td class="listed_anime_type <?= $mobile_disp_none["type"]?>"><?= $type?></td>

                                    <td class="relative <?= $mobile_disp_none["score"]?>">
                                        <select class="personal_score" name="personal_score" id_anime="<?= $id_anime?>">
                                            <option disabled <?= ($score == "") ? "selected" : "" ; ?> value="">Score</option>
                                            <option class="good" <?= ($score == "10") ? "selected" : "" ; ?> value="10">(10) Masterpiece</option>
                                            <option class="good" <?= ($score == "9") ? "selected" : "" ; ?> value="9">(9) Great</option>
                                            <option class="good" <?= ($score == "8") ? "selected" : "" ; ?> value="8">(8) Very Good</option>
                                            <option class="average" <?= ($score == "7") ? "selected" : "" ; ?> value="7">(7) Good</option>
                                            <option class="average" <?= ($score == "6") ? "selected" : "" ; ?> value="6">(6) Fine</option>
                                            <option class="average" <?= ($score == "5") ? "selected" : "" ; ?> value="5">(5) Average</option>
                                            <option class="bad" <?= ($score == "4") ? "selected" : "" ; ?> value="4">(4) Bad</option>
                                            <option class="bad" <?= ($score == "3") ? "selected" : "" ; ?> value="3">(3) Very Bad</option>
                                            <option class="bad" <?= ($score == "2") ? "selected" : "" ; ?> value="2">(2) Horrible</option>
                                            <option class="bad" <?= ($score == "1") ? "selected" : "" ; ?> value="1">(1) Appalling</option>
                                        </select>

                                        <div class="select_overlay" id_anime="<?= $id_anime?>">
                                            <i class='fa fa-spinner fa-spin'></i>
                                        </div>
                                    </td>

                                    <td class="eps <?= $mobile_disp_none["episodes"]?>">
                                        <span>
                                            <input type="text" class="ep_target" name="ep_target" anime="<?= $id_anime?>" value="<?= $eps_seen?>" data-toggle="tooltip" data-placement="top" title="Press 'Enter' to save" />
                                        </span>
                                        <span> / </span>
                                        <span><?= ($total_eps == "Unknown") ? "?" : $total_eps?></span>
                                    </td>

                                    <td class="actions <?= $mobile_disp_none["actions"]?>">

                                        <i class="fa fa-minus-circle durian sub_ep <?= $mobile_disp_none["actions_eps"]?>" aria-hidden="true" anime="<?= $id_anime?>" data-toggle="tooltip" data-placement="top" title="Subtract Episode"></i>
                                        <i class="fa fa-plus-circle matcha add_ep <?= $mobile_disp_none["actions_eps"]?>" aria-hidden="true" anime="<?= $id_anime?>" data-toggle="tooltip" data-placement="top" title="Add Episode"></i>

                                        <br class="disp_none mobile_disp_block <?= $mobile_disp_none["actions_eps"]?>">

                                        <i class="fa fa-retweet iris switch_lists" aria-hidden="true" anime="<?= $id_anime?>" data-container="body" data-toggle="popover" data-placement="top"></i>
                                        <i class="fa fa-times-circle redbeanpaste delete_anime" aria-hidden="true" anime="<?= $id_anime?>" data-toggle="tooltip" data-placement="top" title="Remove Anime"></i>

                                        <div class="popover_content_wrapper" anime="<?= $id_anime?>" style="display: none">
                                            <?
                                            $get_lists = Main::get("anime_lists");
                                            foreach($get_lists as $list){
                                                $list_id = $list["id"];
                                                $list_name = $list["name"];

                                                switch ($list["id"]){
                                                    case "1":
                                                        $bg = "matcha_t";
                                                        break;
                                                    case "2":
                                                        $bg = "durian_t";
                                                        break;
                                                    case "3":
                                                        $bg = "iris_t";
                                                        break;
                                                    case "4":
                                                        $bg = "redbeanpaste_t";
                                                        break;
                                                    default:
                                                        $bg = "matcha_t";
                                                        break;
                                                }

                                                if($list_id != $get_anime["id_list"]){
                                                    ?>
                                                    <div class="popover_select <?= $bg?>" anime="<?= $id_anime?>" id_list="<?= $list_id?>"><?= $list_name?></div>
                                                    <?
                                                }
                                            }
                                            ?>
                                        </div>

                                    </td>

                                </tr>
                                <?
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?
            }
            ?>

            <div class="row">
                <div id="target"></div>
            </div>

        </div>
    </div>
    <?

}


include 'fim.php';
?>
