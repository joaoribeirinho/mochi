<?
include 'inicio.php';
?>

<div class="fakebanner" style="background-image: url('images/kimi2.jpg')">
    <?
    $id_url = $_GET["id"];
    if($id_url){
        $get = Main::get("anime_studios", "id = $id_url");
        $name = $get[0]["name"];
        ?>
        <div class="active_label"><?= $name?></div>
        <?
    }
    ?>
</div>

<div class="bloco first_bloco">
    <div class="container">
        <div class="all_anime all_studios" id="studios">

            <div class="row">
                <?
                //calculating total of pages
                //calculating total of pages
                $where = "studios_id LIKE '%;" . $id_url . ";' OR studios_id LIKE '" . $id_url . ";%' OR studios_id LIKE '%;" . $id_url . ";%' OR studios_id = '" . $id_url . "'";
                $count_animes = Pages::get("status = 1 AND id_category = 1 AND $where", "name ASC");
                $count_animes = count($count_animes);

                if ($_GET['pageno']) {
                    $pageno = $_GET['pageno'];
                } else {
                    $pageno = 1;
                }

                if ($_GET['tpp']) {
                    $no_of_records_per_page = $_GET['tpp'];
                } else {
                    $no_of_records_per_page = 9;
                }

                $offset = ($pageno - 1) * $no_of_records_per_page;
                $total_pages = ceil($count_animes / $no_of_records_per_page);
                //calculating total of pages
                //calculating total of pages

                $where = "studios_id LIKE '%;" . $id_url . ";' OR studios_id LIKE '" . $id_url . ";%' OR studios_id LIKE '%;" . $id_url . ";%' OR studios_id = '" . $id_url . "'";
                $get_animes = Pages::get("status = 1 AND id_category = 1 AND $where", "name ASC",  array($offset, $no_of_records_per_page));
                foreach($get_animes as $anime){

                    $name = $anime["name"];
                    $id = $anime["id"];
                    $type = $anime["type"];
                    $studios = explode(";", $anime["studios_id"]);
                    $genres = explode(";", $anime["genres_id"]);

                    $image = Pages::get_image($id,"description ASC");

                    if($image){
                        $image_path = Pages::image_path($image[0]["image"]);
                    }else{
                        $image_path = "images/no_image.jpg";
                    }


                    switch($type) {
                        case "TV":
                            $class = "matcha";
                            break;
                        case "ONA":
                            $class = "durian";
                            break;
                        case "OVA":
                            $class = "sesame";
                            break;
                        case "Special":
                            $class = "redbeanpaste";
                            break;
                        case "Movie":
                            $class = "iris";
                            break;
                        default:
                            $class = "matcha";
                    }

                    //check if there is hentai to block
                    $censured = false;
                    foreach($genres as $genre){
                        if($genre == 12){
                            $censured = true;
                        }
                    }

                    /*check data if anime on any list*/
                    if($login){
                        $user_id = $user["id"];
                        $check_if_on_any_list = Main::get("blogs_users_animes", "id_user = $user_id AND id_anime = $id");
                        $is_on_list = $check_if_on_any_list[0]["id_list"];

                        if($is_on_list){
                            $list_name = Main::get("anime_lists", "id = $is_on_list");
                            $list_name = $list_name[0]["name"];
                        }else{
                            $list_name = "";
                        }
                    }


                    if($id){
                        ?>
                        <div class="col-md-4 col-sm-6">
                            <div class="row" style="overflow:unset;">
                                <div class="each_anime no_hover">
                                    <div class="col-md-6 col-xs-6">
                                        <?
                                        if($censured == true){
                                            ?>
                                            <div class="image" style="background-image: url('<?= $image_path?>')">
                                                <div class="overlay_18">
                                                    <img src="images/censured.png" />
                                                </div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <a href="anime?id=<?= $id?>">
                                                <div class="image" style="background-image: url('<?= $image_path?>')">
                                                    <?
                                                    if($list_name){
                                                        switch($list_name) {
                                                            case "Watching":
                                                                $class = "matcha";
                                                                break;
                                                            case "Interested In":
                                                                $class = "durian";
                                                                break;
                                                            case "Finished":
                                                                $class = "iris";
                                                                break;
                                                            case "On Hold":
                                                                $class = "redbeanpaste";
                                                                break;
                                                            default:
                                                                $class = "disp_none";
                                                        }

                                                        if(!$class){
                                                            $class = "disp_none";
                                                        }

                                                        ?>
                                                        <div class="on_list <?= $class?>"><?= $list_name?></div>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                            </a>
                                            <?
                                        }
                                        ?>
                                    </div>

                                    <div class="col-md-6 col-xs-6">
                                        <div class="body">

                                            <div class="type <?= $class?>"><?= $type?></div>

                                            <?
                                            foreach($studios as $studio){
                                                if($studio != ""){
                                                    $get_studio_name = Main::get("anime_studios", "id = $studio");
                                                    if($get_studio_name){
                                                        $each_studio_name = $get_studio_name[0]["name"];
                                                        ?>
                                                        <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                                            <div class="studio"><?= $each_studio_name?></div>
                                                        </a>
                                                        <?
                                                    }
                                                }
                                            }

                                            if($studios[0] != ""){
                                                ?>
                                                <br>
                                                <?
                                            }

                                            foreach($genres as $genre){
                                                if($genre != ""){
                                                    $get_genre_name = Main::get("anime_genres", "id = $genre");
                                                    if($get_genre_name){
                                                        $each_genre_name = $get_genre_name[0]["name"];

                                                        if($censured == true && $get_genre_name[0]["id"] == 12){
                                                            ?>
                                                            <div class="genre"><?= $each_genre_name?></div>
                                                            <?
                                                        }else{
                                                            ?>
                                                            <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                                                <div class="genre"><?= $each_genre_name?></div>
                                                            </a>
                                                            <?
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?
                                    if($censured != true){
                                        $synopsis = Main::get("anime_other_details", "id_anime = $id");
                                        if($synopsis){
                                            ?>
                                            <div class="overlay has_sinopsis" target="<?= $id?>">
                                                <a href="anime?id=<?= $id?>" title="<?= $name?>"><?= $name?></a>
                                            </div>

                                            <div class="synopsis_btn" target="<?= $id?>">
                                                <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            </div>

                                            <div class="synopsis disp_none" target="<?= $id?>">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                <div class="synopsis_inner_text"><?= $synopsis[0]["synopsis_mal"]?></div>
                                            </div>
                                            <?
                                        }else{
                                            ?>
                                            <div class="overlay">
                                                <a href="anime?id=<?= $id?>" title="<?= $name?>"><?= $name?></a>
                                            </div>
                                            <?
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                        </div>
                        <?
                    }

                }
                ?>
            </div>

            <div class="row">
                <div class="col-sm-6">Found: <?= $count_animes?> result<?= ($count_animes > 1) ? "s" : "" ; ?> (<?= $total_pages?> pages).</div>

                <div class="col-sm-6 right pagination_master">

                    <ul class="pagination">
                        <li class="disabled">
                            <span class="<?= ($pageno <= 1) ? "disabled" : "link_pag" ; ?>" pageno="1" id_studio="<?= $id_url?>"  tpp="<?= $no_of_records_per_page?>">First</span>
                        </li>

                        <li class="<?= ($pageno <= 1) ? "disabled" : "" ; ?>">
                            <span class="<?= ($pageno <= 1) ? "disabled" : "link_pag" ; ?>" pageno="<?= ($pageno <= 1) ? "#" : $pageno - 1 ; ?>" id_studio="<?= $id_url?>" tpp="<?= $no_of_records_per_page?>">Prev</span>
                        </li>

                        <li class="disabled active_pag">
                            <span> - <?= $pageno?> - </span>
                        </li>

                        <li class="<?= ($pageno >= $total_pages) ? "disabled" : "" ; ?>">
                            <span class="<?= ($pageno >= $total_pages) ? "disabled" : "link_pag" ; ?>" pageno="<?= ($pageno >= $total_pages) ? "#" : $pageno + 1 ; ?>" id_studio="<?= $id_url?>" tpp="<?= $no_of_records_per_page?>">Next</span>
                        </li>

                        <li class="<?= ($pageno >= $total_pages) ? "disabled" : "" ; ?>">
                            <span class="<?= ($pageno >= $total_pages) ? "disabled" : "link_pag" ; ?>" pageno="<?= $total_pages?>" id_studio="<?= $id_url?>" tpp="<?= $no_of_records_per_page?>">Last</span>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
</div>


<?
include 'fim.php';
?>
