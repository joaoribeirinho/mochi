<?
include 'inicio.php';

?>

<div class="fakebanner22"></div>

<style>

    @media screen and (min-width: 993px){
        .menu_holder {
            position: fixed;
            background-color: rgba(41, 41, 41, 0.5);
            padding: 0;
        }
    }

</style>

<?
if($user["id"] == 1){
    $order = "id DESC";
}else{
    $order = "RAND()";
}

$banners = Banners::get("id_category = 1 AND status = 1", $order, array(0, 10));
?>

<div class="banner">
    <ul id="slider_home<?= (count($banners) == 1) ? "_not" : ""?>">
        <?
        foreach($banners as $banner){
            $image = Banners::get_image($banner["id"]);
            $image_path = Banners::image_path($image[0]["image"]);

            $id = $banner["id"];
            $link_anime = "anime?id=" . $banner["link"];

            $source = Main::get("additional_contents", "additional_id = 1 AND module_content_id = $id");
            $link_source = $source[0]["content"];

            ?>
            <li style="background-image: url('<?= $image_path?>')" source="<?= $link_source?>">
                <div class="banner_content_box no_hover">

                    <?
                    if($image[1]["image"]){
                        $anime_logo_path = Banners::image_path($image[1]["image"]);
                        ?>
                        <a href="<?= $link_anime?>">
                            <img src="<?= $anime_logo_path?>" />
                        </a>
                        <?
                    }else{
                        ?>
                        <a href="<?= $link_anime?>">
                            <h3><?= $banner["name"]?></h3>
                        </a>
                        <?
                    }
                    ?>


                </div>
            </li>
            <?
        }
        ?>
    </ul>

    <div class="teste disp_none">
        <i class="fa fa-angle-down" aria-hidden="true"></i>
    </div>
</div>

<div class="bloco">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="homepage_title">Recent Feedback</div>
            </div>
        </div>
        <br><br>

        <div class="sub_holder">
            <?
            if($login == true){

                //details of user
                $user_id = $user["id"];

                //get preferences
                $get_preferences = Main::get("blogs_users_preferences", "id_user = $user_id");
            }

            $feedbacks = Main::get("blogs_comments", "status = 1", "created_at DESC LIMIT 0, 6");
            if($feedbacks){
                foreach($feedbacks as $index => $feedback){
                    $feedback_id = $feedback["id"];
                    $anime_id = $feedback["id_blog"];

                    $content = $feedback["content"];
                    if(strlen($content) > 130){
                        $content = substr($content,0, 127) . "...";
                    }

                    $blog_id_user = $feedback["id_user"];

                    $blog_user = Blogs::get_user_by_id($blog_id_user);
                    $blog_user_id = $blog_user["id"];
                    $blog_user_name = $blog_user["user"];
                    $blog_user_image = $blog_user["image"];
                    $blog_user_image = Blogs::user_image_path($blog_user_image);

                    $anime = Pages::get_by_id($anime_id);
                    $anime_name = $anime["name"];
                    $anime_type = $anime["type"];

                    switch($anime_type) {
                        case "TV":
                            $class = "matcha";
                            break;
                        case "ONA":
                            $class = "durian";
                            break;
                        case "OVA":
                            $class = "sesame";
                            break;
                        case "Special":
                            $class = "redbeanpaste";
                            break;
                        case "Movie":
                            $class = "iris";
                            break;
                        default:
                            $class = "matcha";
                    }

                    $get_banner = Banners::get("link = $anime_id AND status = 1");
                    if($get_banner){
                        $id = $get_banner[0]["id"];
                        $banner = Banners::get_image($id);
                        $banner = Banners::image_path($banner[0]["image"]);
                        $source = Main::get("additional_contents", "additional_id = 1 AND module_content_id = $id");
                        $link_source = $source[0]["content"];
                    }else{
                        $banner = "images/kimi2.jpg";
                    }

                    //$image = Pages::get_image($anime_id,"description ASC");
                    //$image_path = $image[0]["image"];

                    $start_date = $feedback["created_at"];
                    $today_date = date("Y-m-d H:i:s");
                    $diff = abs(strtotime($today_date) - strtotime($start_date));
                    $years = floor($diff / (365*60*60*24));
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                    if($days >= 1){
                        if($days == 1){
                            $age = $days . " day ago";
                        }else{
                            $age = $days . " days ago";
                        }
                    }else{

                        $diff = strtotime($today_date) - strtotime($start_date);
                        $hours = floor($diff / ( 60 * 60 ));

                        if($hours >= 1){
                            if($hours == 1){
                                $age = $hours . " hour ago";
                            }else{
                                $age = $hours . " hours ago";
                            }
                        }else{
                            $age = "less than an hour ago";
                        }

                    }
                    if($months >= 1){
                        if($months == 1){
                            $age = $months . " month ago";
                        }else{
                            $age = $months . " months ago";
                        }
                    }
                    if($years >= 1){
                        if($years == 1){
                            $age = $years . " year ago";
                        }else{
                            $age = $years . " years ago";
                        }
                    }

                    if($index == 1 || $index == 4){
                        $not_middle = "";
                    }else{
                        $not_middle = "not_middle";
                    }

                    $upvotes = Blogs::get_total_comments_likes($feedback_id);
                    $upvotes = $upvotes["COUNT(id)"];

                    if($upvotes == ""){
                        $upvotes = 0;
                    }

                    if($login){
                        $check_if_user_has_like = Blogs::get_comments_likes($feedback_id, "id_user = $user_id");

                        if($check_if_user_has_like){
                            $vote_class = "fa-thumbs-up";
                        }else{
                            $vote_class = "fa-thumbs-o-up";
                        }

                    }else{
                        $vote_class = "fa-thumbs-o-up";
                    }

                    ?>
                    <div class="col-md-4">
                        <div class="single_feedback <?= $not_middle?>">
                            <div class="anime_img" style="background-image: url('<?= $banner?>')" onclick="location.href='anime?id=<?= $anime_id?>';">
                                <div class="feedback_author">
                                    <div class="author">

                                        <div class="author_img" style="background-image: url('<?= $blog_user_image?>')"></div>

                                        <div style="margin: 5px 0 0 15px;">

                                            <div class="no_hover">
                                                <a href="nakama?id=<?= $blog_user_id?>">@<?= $blog_user_name?></a>
                                            </div>

                                            <div class="<?= $class?>"><?= $anime_type?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="info no_hover">

                                <a href="anime?id=<?= $anime_id?>">
                                    <div class="category_name <?= $class?>"><?= $anime_name?></div>
                                </a>

                                <div class="desc"><?= $content?></div>

                                <div class="details">
                                    <div class="col-xs-4 upvotes"><?= $upvotes?> <i class="fa <?= $vote_class?>" aria-hidden="true"></i></div>
                                    <div class="col-xs-8 date"><?= $age?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                }
            }
            ?>
        </div>
    </div>
</div>

<div class="bloco" style="padding-top:0">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="homepage_title">Updated Animes</div>
            </div>
        </div>
        <br><br>

        <div class="updated_holder">
            <?
            $updates = Main::get("anime_other_details", false, "updated_at DESC LIMIT 0, 6");
            if($updates){
                foreach($updates as $index => $updated){
                    $row_id = $updated["id"];
                    $anime_id = $updated["id_anime"];
                    $synopsis = $updated["synopsis_mal"];
                    if(strlen($synopsis) > 130){
                        $synopsis = substr($synopsis,0, 127) . "...";
                    }

                    $anime = Pages::get_by_id($anime_id);
                    $anime_name = $anime["name"];
                    $anime_type = $anime["type"];
                    $episodes = $anime["nb_eps"];

                    if($episodes == 1){
                        $episodes = $episodes . " episode";
                    }else{
                        $episodes = $episodes . " episodes";
                    }

                    switch($anime_type) {
                        case "TV":
                            $class = "matcha";
                            break;
                        case "ONA":
                            $class = "durian";
                            break;
                        case "OVA":
                            $class = "sesame";
                            break;
                        case "Special":
                            $class = "redbeanpaste";
                            break;
                        case "Movie":
                            $class = "iris";
                            break;
                        default:
                            $class = "matcha";
                    }

                    $get_banner = Banners::get("link = $anime_id AND status = 1");
                    if($get_banner){
                        $id = $get_banner[0]["id"];
                        $banner = Banners::get_image($id);
                        $banner = Banners::image_path($banner[0]["image"]);
                        $source = Main::get("additional_contents", "additional_id = 1 AND module_content_id = $id");
                        $link_source = $source[0]["content"];
                    }else{
                        $banner = "images/kimi2.jpg";
                    }

                    //$image = Pages::get_image($anime_id,"description ASC");
                    //$image_path = $image[0]["image"];

                    if($index == 1 || $index == 4){
                        $not_middle = "";
                    }else{
                        $not_middle = "not_middle";
                    }

                    ?>
                    <div class="col-md-4">
                        <div class="single_updated <?= $not_middle?>">
                            <div class="anime_img" style="background-image: url('<?= $banner?>')" onclick="location.href='anime?id=<?= $anime_id?>';"></div>

                            <div class="info no_hover">

                                <a href="anime?id=<?= $anime_id?>">
                                    <div class="anime_name <?= $class?>"><?= $anime_name?></div>
                                </a>

                                <div class="desc"><?= $synopsis?></div>

                                <div class="details">
                                    <div class="col-xs-4 type"><?= $anime_type?></div>
                                    <div class="col-xs-8 episodes"><?= $episodes?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                }
            }
            ?>
        </div>
    </div>
</div>

<?

include 'fim.php';
?>



