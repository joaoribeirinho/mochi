<div class="bloco footer">
    <div class="container">

        <div class="col-md-4 center">
            <div class="footer_logo center">
                <a href="home">
                    <img src="images/logo.png" />
                </a>
            </div>
        </div>

        <div class="col-md-4">
            <div class="footer_politicas">
                <div class="txt_texto_footer smaller">
                    <a href="schedule">Schedule</a>
                    <br>
                    <a href="ideas">Ideas</a>
                    <br>
                    <a href="help">Help - Tickets</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="footer_politicas">
                <div class="txt_texto_footer smaller">
                    <a href="privacy_policy">Privacy Policy</a>
                    <br>
                    Developed by <a href="https://jr-dev.pt/home" style="text-decoration: underline">JR</a>
                </div>
            </div>
        </div>

    </div>
</div>
