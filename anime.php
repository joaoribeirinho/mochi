<?
include 'inicio.php';

$id_url = $_GET["id"];
if($id_url){
    $anime = Pages::get_by_id($id_url);
    $id_mal = $anime["id_mal"];
    $name = $anime["name"];
    $eng_name = $anime["eng_name"];
    $total_eps = $anime["nb_eps"];

    $start_date = $anime["start_date"];
    $date = explode("-", $start_date);
    $year = $date[0];
    $month = $date[1];
    $day = $date[2];
    switch($month){
        case "01";
            $start_date = "Jan " . $day;
            break;
        case "02";
            $start_date = "Feb " . $day;
            break;
        case "03";
            $start_date = "Mar " . $day;
            break;
        case "04";
            $start_date = "Apr " . $day;
            break;
        case "05";
            $start_date = "May " . $day;
            break;
        case "06";
            $start_date = "Jun " . $day;
            break;
        case "07";
            $start_date = "Jul " . $day;
            break;
        case "08";
            $start_date = "Aug " . $day;
            break;
        case "09";
            $start_date = "Sep " . $day;
            break;
        case "10";
            $start_date = "Oct " . $day;
            break;
        case "11";
            $start_date = "Nov " . $day;
            break;
        case "12";
            $start_date = "Dec " . $day;
            break;
        default;
            $start_date = $anime["start_date"];
            break;
    }

    $season = $anime["season"];
    $season_of_year = end(explode("]", $season));
    $year = reset(explode("-[", $season));

    $season =  $season_of_year . " " . $year;

    $view_updated = $anime["views"] + 1;
    $fields = array(
        "views" => $view_updated,
    );
    $add_view = Pages::update($fields, $id_url);

    $image = Pages::get_image($id_url,"description ASC");

    if($image){
        $image_path = Pages::image_path($image[0]["image"]);
    }else{
        $image_path = "images/no_image.jpg";
    }

    //$get_banner = Main::get("pages_images", "id_page = $id_url AND description = 'profile_banner'");
    $get_banner = Banners::get("link = $id_url AND status = 1");
    if($get_banner){
        $id = $get_banner[0]["id"];
        $banner = Banners::get_image($id);
        $banner = Banners::image_path($banner[0]["image"]);
        $source = Main::get("additional_contents", "additional_id = 1 AND module_content_id = $id");
        $link_source = $source[0]["content"];
    }else{
        $banner = "images/kimi2.jpg";
    }

    $type = $anime["type"];
    switch($type) {
        case "TV":
            $class = "matcha";
            break;
        case "ONA":
            $class = "durian";
            break;
        case "OVA":
            $class = "sesame";
            break;
        case "Special":
            $class = "redbeanpaste";
            break;
        case "Movie":
            $class = "iris";
            break;
        default:
            $class = "matcha";
    }

    $studios = explode(";", $anime["studios_id"]);
    $genres = explode(";", $anime["genres_id"]);


    //check if there is hentai to block
    $censured = false;
    foreach($genres as $genre){
        if($genre == 12){
            $censured = true;
        }
    }

    $weekday = date("l");

    if($weekday == "Friday"){

        $check = Main::get("anime_other_details", "id_anime = $id_url");
        if($check){
            //exists

            $today = date("Y-m-d");
            $was_updated_at = reset(explode(" ", $check[0]["updated_at"]));
            if($today == $was_updated_at){
                //exists - been updated
                //exists - do nothing

                $synopsis = $check[0]["synopsis_mal"];
                $mal_score = $check[0]["score_mal"];
                $mal_score_trim = trim(reset(explode(".", $mal_score)));
                $youtube_link = $check[0]["youtube_trailer"];
            }else{
                //not updated
                //update

                //https://simplehtmldom.sourceforge.io/
                $html = file_get_html('https://myanimelist.net/anime/' . $id_mal);
                //FAITHFULL
                //$item['title'] = $html->find('span[itemprop="name"]', 0)->plaintext;
                //$item['img'] = $html->find('img[itemprop="image"]', 0)->src;
                $eng_name = $html->find('span.title-english', 0)->plaintext;
                $synopsis = $html->find('span[itemprop="description"]', 0)->plaintext;
                $mal_score = $html->find('div.score', 0)->plaintext;
                $mal_score_trim = trim(reset(explode(".", $mal_score)));

                //https://developers.google.com/youtube/v3/docs/search/list
                $API_key = "AIzaSyBfkiNYG4lAkUjoqFXGVfLmoZJDTSvOCeE";
                $search_query = urlencode($name) . "+trailer";
                $maxResults = 1;
                $link = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" . $search_query . "&maxResults=" . $maxResults . "&key=" . $API_key;
                $videoList = json_decode(file_get_contents($link));
                $item = $videoList->items[0];

                if(isset($item->id->videoId)){
                    //$youtube_link = "https://www.youtube.com/embed/" . $item->id->videoId . "?autoplay=1";
                    $youtube_link = "https://www.youtube.com/embed/" . $item->id->videoId . "?rel=0";
                }else{
                    $youtube_link = "";
                }

                $data = date("Y-m-d H:i:s");

                $eng_name_fields = array(
                    "eng_name" => $eng_name,
                    "updated_at" => $data,
                );
                $update_anime = Pages::update($eng_name_fields, $id_url);

                $fields = array(
                    "score_mal" => trim($mal_score),
                    "youtube_trailer" => $youtube_link,
                    "synopsis_mal" => $synopsis,
                    "updated_at" => $data,
                );
                $update = Main::update_where("anime_other_details", $fields, "id_anime = $id_url");

                if($update){
                    $msg = "<div class=\"update_msg\">Weekly update. Thank you for helping us keep this anime up to date!</div>";
                }
            }
        }else{
            //not exists
            //add

            //https://simplehtmldom.sourceforge.io/
            $html = file_get_html('https://myanimelist.net/anime/' . $id_mal);
            //FAITHFULL
            //$item['title'] = $html->find('span[itemprop="name"]', 0)->plaintext;
            //$item['img'] = $html->find('img[itemprop="image"]', 0)->src;
            $eng_name = $html->find('span.title-english', 0)->plaintext;
            $synopsis = $html->find('span[itemprop="description"]', 0)->plaintext;
            $mal_score = $html->find('div.score', 0)->plaintext;
            $mal_score_trim = trim(reset(explode(".", $mal_score)));

            //https://developers.google.com/youtube/v3/docs/search/list
            $API_key = "AIzaSyBfkiNYG4lAkUjoqFXGVfLmoZJDTSvOCeE";
            $search_query = urlencode($name) . "+trailer";
            $maxResults = 1;
            $link = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" . $search_query . "&maxResults=" . $maxResults . "&key=" . $API_key;
            $videoList = json_decode(file_get_contents($link));
            $item = $videoList->items[0];

            if(isset($item->id->videoId)){
                $youtube_link = "https://www.youtube.com/embed/" . $item->id->videoId . "?rel=0";
            }else{
                $youtube_link = "";
            }

            $data = date("Y-m-d H:i:s");

            $eng_name_fields = array(
                "eng_name" => $eng_name,
                "updated_at" => $data,
            );
            $update_anime = Pages::update($eng_name_fields, $id_url);

            $fields = array(
                "id_anime" => $id_url,
                "score_mal" => trim($mal_score),
                "youtube_trailer" => $youtube_link,
                "synopsis_mal" => $synopsis,
                "updated_at" => $data,
            );
            $add = Main::add("anime_other_details", $fields);

            if($add){
                $msg = "<div class=\"update_msg\">Weekly update. Thank you for helping us keep this anime up to date!</div>";
            }
        }
    }else{
        //not friday

        $check = Main::get("anime_other_details", "id_anime = $id_url");
        if($check){
            //exists

            $synopsis = $check[0]["synopsis_mal"];
            $mal_score = $check[0]["score_mal"];
            $mal_score_trim = trim(reset(explode(".", $mal_score)));
            $youtube_link = $check[0]["youtube_trailer"];

        }else{
            //not exists
            //add

            //https://simplehtmldom.sourceforge.io/
            $html = file_get_html('https://myanimelist.net/anime/' . $id_mal);
            //FAITHFULL
            //$item['title'] = $html->find('span[itemprop="name"]', 0)->plaintext;
            //$item['img'] = $html->find('img[itemprop="image"]', 0)->src;
            $eng_name = $html->find('span.title-english', 0)->plaintext;
            $synopsis = $html->find('span[itemprop="description"]', 0)->plaintext;
            $mal_score = $html->find('div.score', 0)->plaintext;
            $mal_score_trim = trim(reset(explode(".", $mal_score)));

            //https://developers.google.com/youtube/v3/docs/search/list
            $API_key = "AIzaSyBfkiNYG4lAkUjoqFXGVfLmoZJDTSvOCeE";
            $search_query = urlencode($name) . "+trailer";
            $maxResults = 1;
            $link = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" . $search_query . "&maxResults=" . $maxResults . "&key=" . $API_key;
            $videoList = json_decode(file_get_contents($link));
            $item = $videoList->items[0];

            if(isset($item->id->videoId)){
                $youtube_link = "https://www.youtube.com/embed/" . $item->id->videoId . "?rel=0";
            }else{
                $youtube_link = "";
            }

            $data = date("Y-m-d H:i:s");

            $eng_name_fields = array(
                "eng_name" => $eng_name,
                "updated_at" => $data,
            );
            $add_to_anime = Pages::update($eng_name_fields, $id_url);

            $fields = array(
                "id_anime" => $id_url,
                "score_mal" => trim($mal_score),
                "youtube_trailer" => $youtube_link,
                "synopsis_mal" => $synopsis,
                "updated_at" => $data,
            );
            $add = Main::add("anime_other_details", $fields);

            if($add){
                $msg = "<div class=\"update_msg\">You were the first to open this anime. Thank you for helping us keep it up to date!</div>";
            }
        }
    }

    if($mal_score_trim <= 4){
        $mal_score_color = "redbeanpaste";
        $score_desc = "Bad";
    }elseif($mal_score_trim >= 5 && $mal_score_trim <= 7){
        $mal_score_color = "durian";
        $score_desc = "Average";
    }elseif($mal_score_trim >= 8){
        $mal_score_color = "matcha";
        $score_desc = "Good";
    }else{
        $mal_score_color = "none";
        $score_desc = "";
    }

    if($login == true){
        //details of user
        $user_id = $user["id"];

        //get preferences
        $get_preferences = Main::get("blogs_users_preferences", "id_user = $user_id");

        $anime_page_type = $get_preferences[0]["anime_page_type"];
        if($anime_page_type == 2){
            ?>
            <script>
                $(document).ready(function(){
                    $("body").css("background-image", "url(<?= $banner?>)");
                });
            </script>
            <style>
                .anime_detail{
                    background-color: #d0d0d0;
                }
            </style>
            <?
        }


        /*check data if anime on any list*/
        $check_lists = Main::get("blogs_users_animes", "id_user = $user_id AND id_anime = $id_url");
        if($check_lists){
            $on_a_list = true;
            $eps_seen = $check_lists[0]["eps_seen"];
            $personal_score = $check_lists[0]["score"];
        }


    }

    $link_mal = "https://myanimelist.net/anime/" . $id_mal;
    $link_nyaa = "https://nyaa.si/?f=0&c=0_0&q=" . str_replace(" ", "+", $name) . "+1080";
    $link_filler_list = "https://www.animefillerlist.com/shows/" . str_replace(" ", "-", strtolower(str_replace(":", "", $name)));
    $link_aniwatch = "https://aniwatch.me/anime/" . $anime["id_aniwatch"];


}else{
    go_to("animes");
}

if($link_source){
    ?>
    <!--Image Source:: <?= $link_source?> -->
    <?
}





?>

<div class="fakebanner" style="background-image: url('<?= $banner?>')"><?= $msg?></div>

<div class="bloco first_bloco">
    <div class="container">
        <div class="row anime_detail">

            <div class="col-sm-3 no_hover">
                <a href="<?= $image_path?>" class="fancybox" rel="<?= $name?>">
                    <div class="image2" style="background-image: url('<?= $image_path?>')">
                        <?
                        if($login){
                            $check_if_on_any_list = Main::get("blogs_users_animes", "id_user = $user_id AND id_anime = $id_url");
                            $is_on_list = $check_if_on_any_list[0]["id_list"];

                            if($is_on_list){
                                $list_name = Main::get("anime_lists", "id = $is_on_list");
                                $list_name = $list_name[0]["name"];
                            }

                            ?>
                            <div id_list="1" class="type_list_label matcha <?= ($list_name == "Watching") ? "" : "disp_none"?>">Watching</div>
                            <div id_list="2" class="type_list_label durian <?= ($list_name == "Interested In") ? "" : "disp_none"?>">Interested In</div>
                            <div id_list="3" class="type_list_label iris <?= ($list_name == "Finished") ? "" : "disp_none"?>">Finished</div>
                            <div id_list="4" class="type_list_label redbeanpaste <?= ($list_name == "On Hold") ? "" : "disp_none"?>">On Hold</div>
                            <?
                        }
                        ?>

                    </div>
                </a>
                <br>

                <?
                if($login){
                    $check_if_on_any_list = Main::get("blogs_users_animes", "id_user = $user_id AND id_anime = $id_url");
                    $is_on_list = $check_if_on_any_list[0]["id_list"];

                    switch ($is_on_list){
                        case "1":
                            $icon = "<i class=\"fa fa-eye\" icon=\"fa-eye\" aria-hidden=\"true\"></i>";
                            break;
                        case "2":
                            $icon = "<i class=\"fa fa-search\" icon=\"fa-search\" aria-hidden=\"true\"></i>";
                            break;
                        case "3":
                            $icon = "<i class=\"fa fa-check-circle\" icon=\"fa-check-circle\" aria-hidden=\"true\"></i>";
                            break;
                        case "4":
                            $icon = "<i class=\"fa fa-stop\" icon=\"fa-stop\" aria-hidden=\"true\"></i>";
                            break;
                        default:
                            $icon = "<i class=\"fa fa-bookmark-o\" icon=\"fa-bookmark-o\" aria-hidden=\"true\"></i>";
                            break;
                    }
                    ?>

                    <div id="add_to_list">
                        <div class="btn_add_to_list closed"><?= (!isset($is_on_list)) ? "Add to" : "Update"?> List <?= $icon?></div>
                        <div class="lists disp_none">
                            <?
                            $get_lists = Main::get("anime_lists");
                            foreach($get_lists as $list){
                                $list_id = $list["id"];
                                $list_name = $list["name"];

                                switch ($list_id){
                                    case "1":
                                        $bg = "matcha";
                                        $icon = "<i class=\"fa fa-eye\" icon=\"fa-eye\" aria-hidden=\"true\"></i>";
                                        break;
                                    case "2":
                                        $bg = "durian";
                                        $icon = "<i class=\"fa fa-search\" icon=\"fa-search\" aria-hidden=\"true\"></i>";
                                        break;
                                    case "3":
                                        $bg = "iris";
                                        $icon = "<i class=\"fa fa-check-circle\" icon=\"fa-check-circle\" aria-hidden=\"true\"></i>";
                                        break;
                                    case "4":
                                        $bg = "redbeanpaste";
                                        $icon = "<i class=\"fa fa-stop\" icon=\"fa-stop\" aria-hidden=\"true\"></i>";
                                        break;
                                    default:
                                        $bg = "";
                                        $icon = "";
                                        break;
                                }
                                ?>
                                <div class="select_list <?= $bg?>" anime="<?= $id_url?>" id_list="<?= $list_id?>"><?= $list_name?>  <?= $icon?></div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                    <br>
                    <?
                }
                ?>

                <div class="anime_help_btns">
                    <!--MAL-->
                    <div class="no_hover">
                        <a href="<?= $link_mal?>" target="_blank">
                            <div class="btn_mal">Check on MAL</div>
                        </a>
                    </div>
                    <br>

                    <!--NYAA-->
                    <div class="no_hover">
                        <a href="<?= $link_nyaa?>" target="_blank">
                            <div class="btn_nya">Nyaa</div>
                        </a>
                    </div>
                    <br>

                    <!--FILLER-->
                    <div class="no_hover">
                        <a href="<?= $link_filler_list?>" target="_blank">
                            <div class="btn_filler">Filler List</div>
                        </a>
                    </div>
                    <br>

                    <?
                    if($anime["id_aniwatch"] != ""){
                        ?>
                        <!--ANIWATCH-->
                        <div class="no_hover">
                            <a href="<?= $link_aniwatch?>" target="_blank">
                                <div class="btn_mal">Watch on AniWatch</div>
                            </a>
                        </div>
                        <br>
                        <?
                    }

                    if($login){
                        ?>
                        <!--RELATIONS-->
                        <div class="no_hover">
                            <a href="relations?id=<?= $id_url?>">
                                <div class="btn_filler">Relations</div>
                            </a>
                        </div>
                        <br>
                        <?
                    }

                    if($youtube_link != ""){
                        ?>
                        <!--TRAILER-->
                        <div class="btn_trailer closed">Watch YT Trailer</div>
                        <br>
                        <?
                    }
                    ?>
                </div>

                <div class="anime_help_btns_rowed">
                    <table>
                        <tr>
                            <td class="btn_mal">
                                <a href="<?= $link_mal?>" target="_blank">MAL</a>
                            </td>

                            <td class="btn_nya">
                                <a href="<?= $link_nyaa?>" target="_blank">NY</a>
                            </td>

                            <td class="btn_filler">
                                <a href="<?= $link_filler_list?>" target="_blank">FL</a>
                            </td>

                            <?
                            if($anime["id_aniwatch"] != ""){
                                ?>
                                <td class="btn_mal">
                                    <a href="<?= $link_aniwatch?>" target="_blank">AW</a>
                                </td>
                                <?
                            }

                            if($youtube_link != ""){
                                ?>
                                <td class="btn_trailer2">YT</td>
                                <?
                            }
                            ?>
                        </tr>
                    </table>
                </div>

            </div>

            <div class="col-sm-9">
                <div class="anime_info">

                    <div class="name"><?= $name?> <span class="<?= $class?>"><?= $type?></span></div>
                    <div class="eng_name"><?= $eng_name?></div>
                    <div class="start_date"><?= $start_date?> - <?= $season?></div>
                    <br>

                    <div class="score <?= $mal_score_color?>" data-toggle="tooltip" data-placement="right" title="<?= $score_desc?>"><?= trim($mal_score)?></div>
                    <br>

                    <div class="all_studios no_hover">
                        <?
                        foreach($studios as $studio){
                            if($studio != ""){
                                $get_studio_name = Main::get("anime_studios", "id = $studio");
                                if($get_studio_name){
                                    $each_studio_name = $get_studio_name[0]["name"];
                                    ?>
                                    <a href="studio?id=<?= $get_studio_name[0]["id"]?>">
                                        <span class="studio">
                                            <span><?= $each_studio_name?></span>
                                        </span>
                                    </a>
                                    <?
                                }
                            }
                        }
                        ?>
                    </div>
                    <br>

                    <div class="all_genres no_hover">
                        <?
                        foreach($genres as $genre){
                            if($genre != ""){
                                $get_genre_name = Main::get("anime_genres", "id = $genre");
                                if($get_genre_name){
                                    $each_genre_name = $get_genre_name[0]["name"];

                                    if($censured == true && $get_genre_name[0]["id"] == 12){
                                        ?>
                                        <span class="genre">
                                            <span><?= $each_genre_name?></span>
                                        </span>
                                        <?
                                    }else{
                                        ?>
                                        <a href="genre?id=<?= $get_genre_name[0]["id"]?>">
                                            <span class="genre">
                                                <span><?= $each_genre_name?></span>
                                            </span>
                                        </a>
                                        <?
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                    <br>

                    <?
                    if($on_a_list){
                        ?>
                        <div class="on_a_list_holder">
                            <div class="on_a_list disp_inline_flex">
                                <select class="personal_score" name="personal_score" id_anime="<?= $id_url?>">
                                    <option disabled <?= ($personal_score == "") ? "selected" : "" ; ?> value="">Score</option>
                                    <option class="good" <?= ($personal_score == "10") ? "selected" : "" ; ?> value="10">(10) Masterpiece</option>
                                    <option class="good" <?= ($personal_score == "9") ? "selected" : "" ; ?> value="9">(9) Great</option>
                                    <option class="good" <?= ($personal_score == "8") ? "selected" : "" ; ?> value="8">(8) Very Good</option>
                                    <option class="average" <?= ($personal_score == "7") ? "selected" : "" ; ?> value="7">(7) Good</option>
                                    <option class="average" <?= ($personal_score == "6") ? "selected" : "" ; ?> value="6">(6) Fine</option>
                                    <option class="average" <?= ($personal_score == "5") ? "selected" : "" ; ?> value="5">(5) Average</option>
                                    <option class="bad" <?= ($personal_score == "4") ? "selected" : "" ; ?> value="4">(4) Bad</option>
                                    <option class="bad" <?= ($personal_score == "3") ? "selected" : "" ; ?> value="3">(3) Very Bad</option>
                                    <option class="bad" <?= ($personal_score == "2") ? "selected" : "" ; ?> value="2">(2) Horrible</option>
                                    <option class="bad" <?= ($personal_score == "1") ? "selected" : "" ; ?> value="1">(1) Appalling</option>
                                </select>

                                <div class="select_overlay" id_anime="<?= $id_url?>">
                                    <i class='fa fa-spinner fa-spin'></i>
                                </div>

                                <div class="personal_eps <?= ($is_on_list == 3) ? "disp_none" : ""?>">
                                    <span>
                                        <input type="text" class="ep_target" name="ep_target" anime="<?= $id_url?>" value="<?= $eps_seen?>" />
                                    </span>
                                    <span> / </span>
                                    <span><?= $total_eps?></span>

                                    <span class="actions">
                                        <i class="fa fa-minus-circle durian sub_ep" aria-hidden="true" anime="<?= $id_url?>" title="Subtract Episode"></i>
                                        <i class="fa fa-plus-circle matcha add_ep" aria-hidden="true" anime="<?= $id_url?>" title="Add Episode"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <?
                    }
                    ?>



                    <div class="synopsis youtube_video_scroll_target">
                        <div class="synopsis_title">About</div>
                        <div style="padding: 0 10px;"><?= ($synopsis == "") ? "This title doesn't have information... Yet!" : $synopsis?></div>
                    </div>
                    <br><br>

                    <?
                    if($youtube_link != ""){
                        ?>
                        <div class="youtube_video_scroll_target2"></div>
                        <div class="youtube_video">
                            <iframe src="<?= $youtube_link?>" frameborder="0" allowfullscreen></iframe>
                            <br><br><br>
                        </div>
                        <?
                    }


                    $prequels = Main::get("additional_contents", "additional_id = 2 AND module_content_id = $id_url");
                    $prequels = $prequels[0]["content"];
                    $prequels = explode(";", $prequels);

                    $sequels = Main::get("additional_contents", "additional_id = 3 AND module_content_id = $id_url");
                    $sequels = $sequels[0]["content"];
                    $sequels = explode(";", $sequels);

                    $others = Main::get("additional_contents", "additional_id = 4 AND module_content_id = $id_url");
                    $others = $others[0]["content"];
                    $others = explode(";", $others);

                    if($prequels[0] != "" || $sequels[0] != "" || $others[0] != ""){
                        ?>
                        <div class="similar_animes">
                            <div class="title">Related Animes</div>
                            <br>

                            <div class="row">
                                <?
                                $related_label = "Prequel";
                                foreach($prequels as $prequel){
                                    $prequel = str_replace("[", "", $prequel);
                                    $prequel = str_replace("]", "", $prequel);
                                    if($prequel != ""){
                                        $anm = Pages::get_by_id($prequel);
                                        $other_image = Pages::get_image($prequel,"description ASC");
                                        $other_image_path = $other_image[0]["image"];
                                        switch($anm["type"]) {
                                            case "TV":
                                                $class = "matcha_t";
                                                break;
                                            case "ONA":
                                                $class = "durian_t";
                                                break;
                                            case "OVA":
                                                $class = "sesame_t";
                                                break;
                                            case "Special":
                                                $class = "redbeanpaste_t";
                                                break;
                                            case "Movie":
                                                $class = "iris_t";
                                                break;
                                            default:
                                                $class = "matcha_t";
                                        }
                                        ?>
                                        <div class="col-md-3">
                                            <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                <a href="anime?id=<?= $anm["id"]?>">
                                                    <div class="related_label"><?= $related_label?></div>
                                                    <div class="other_overlay">
                                                        <div class="other_name">
                                                            <span><?= $anm["name"]?></span>
                                                            <br><br>
                                                            <span class="other_type <?= $class?>"><?= $anm["type"]?></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <?
                                    }
                                }

                                $related_label = "Sequel";
                                foreach($sequels as $sequel){
                                    $sequel = str_replace("[", "", $sequel);
                                    $sequel = str_replace("]", "", $sequel);
                                    if($sequel != ""){
                                        $anm = Pages::get_by_id($sequel);
                                        $other_image = Pages::get_image($sequel,"description ASC");
                                        $other_image_path = $other_image[0]["image"];
                                        switch($anm["type"]) {
                                            case "TV":
                                                $class = "matcha_t";
                                                break;
                                            case "ONA":
                                                $class = "durian_t";
                                                break;
                                            case "OVA":
                                                $class = "sesame_t";
                                                break;
                                            case "Special":
                                                $class = "redbeanpaste_t";
                                                break;
                                            case "Movie":
                                                $class = "iris_t";
                                                break;
                                            default:
                                                $class = "matcha_t";
                                        }
                                        ?>
                                        <div class="col-md-3">
                                            <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                <a href="anime?id=<?= $anm["id"]?>">
                                                    <div class="related_label"><?= $related_label?></div>
                                                    <div class="other_overlay">
                                                        <div class="other_name">
                                                            <span><?= $anm["name"]?></span>
                                                            <br><br>
                                                            <span class="other_type <?= $class?>"><?= $anm["type"]?></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <?
                                    }
                                }

                                $related_label = "Others";
                                foreach($others as $other){
                                    $other = str_replace("[", "", $other);
                                    $other = str_replace("]", "", $other);
                                    if($other != ""){
                                        $anm = Pages::get_by_id($other);
                                        $other_image = Pages::get_image($other,"description ASC");
                                        $other_image_path = $other_image[0]["image"];
                                        switch($anm["type"]) {
                                            case "TV":
                                                $class = "matcha_t";
                                                break;
                                            case "ONA":
                                                $class = "durian_t";
                                                break;
                                            case "OVA":
                                                $class = "sesame_t";
                                                break;
                                            case "Special":
                                                $class = "redbeanpaste_t";
                                                break;
                                            case "Movie":
                                                $class = "iris_t";
                                                break;
                                            default:
                                                $class = "matcha_t";
                                        }
                                        ?>
                                        <div class="col-md-3">
                                            <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                <a href="anime?id=<?= $anm["id"]?>">
                                                    <div class="related_label"><?= $related_label?></div>
                                                    <div class="other_overlay">
                                                        <div class="other_name">
                                                            <span><?= $anm["name"]?></span>
                                                            <br><br>
                                                            <span class="other_type <?= $class?>"><?= $anm["type"]?></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <?
                                    }
                                }

                                /*$two_piece_name = $name;
                                $two_piece_name = explode(" ", $two_piece_name);
                                $two_piece_name = $two_piece_name[0] . " " . $two_piece_name[1];

                                $similar = Pages::get("name LIKE '%$two_piece_name%' AND status = 1", "start_date ASC");
                                foreach ($similar as $other){
                                    if($other["id"] != $id_url){
                                        $other_image = Pages::get_image($other["id"],"description ASC");
                                        $other_image_path = $other_image[0]["image"];

                                        switch($other["type"]) {
                                            case "TV":
                                                $class = "matcha_t";
                                                break;
                                            case "ONA":
                                                $class = "durian_t";
                                                break;
                                            case "OVA":
                                                $class = "sesame_t";
                                                break;
                                            case "Special":
                                                $class = "redbeanpaste_t";
                                                break;
                                            case "Movie":
                                                $class = "iris_t";
                                                break;
                                            default:
                                                $class = "matcha_t";
                                        }

                                        ?>
                                        <div class="col-md-3">
                                            <div class="other_img no_hover" style="background-image: url('<?= $other_image_path?>')">
                                                <a href="anime?id=<?= $other["id"]?>">
                                                    <div class="other_overlay">
                                                        <div class="other_name">
                                                            <span><?= $other["name"]?></span>
                                                            <br><br>
                                                            <span class="other_type <?= $class?>"><?= $other["type"]?></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <?

                                    }
                                }*/
                                ?>
                            </div>
                        </div>
                        <br>
                        <?
                    }
                    ?>


                    <div class="comments">

                        <?
                        if($login){
                            ?>
                            <div class="comment insert">

                                <?
                                $user_img = $user["image"];

                                if($user_img == ""){
                                    $user_img_path = "images/user_unknown.png";
                                }else{
                                    $user_img_path = Blogs::user_image_path($user_img);
                                }
                                ?>

                                <div class="relative">
                                    <div class="parent_comment_user">
                                        <div class="parent_comment_user_img" style="background-image: url('<?= $user_img_path?>')"></div>
                                    </div>

                                    <div class="parent_comment">
                                        <div class="parent_comment_content">
                                            <form id="form_comment" method="post" action="">
                                                <input type="hidden" name="id_anime" value="<?= $id_url?>" />

                                                <textarea rows="3" class="form-control classy-editor" name="comment">Add feedback...</textarea>
                                                <div id="comment_response"></div>
                                            </form>
                                        </div>
                                        <div class="details">
                                            <div class="username">by: <?= $user["user"]?></div>

                                            <i class="fa fa-paper-plane btn_submit_comment" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                        }


                        $get_parent_comments = Blogs::get_comments($id_url, "status = 1");
                        foreach ($get_parent_comments as $parent_comment){
                            $parent_comment_id = $parent_comment["id"];

                            $comment_user = Blogs::get_user_by_id($parent_comment["id_user"]);
                            $comment_user_name = $comment_user["user"];
                            $comment_user_img = $comment_user["image"];

                            if($comment_user_img == ""){
                                $comment_user_img_path = "images/user_unknown.png";
                            }else{
                                $comment_user_img_path = Blogs::user_image_path($comment_user_img);
                            }

                            $start_date = $parent_comment["created_at"];
                            $today_date = date("Y-m-d H:i:s");
                            $diff = abs(strtotime($today_date) - strtotime($start_date));
                            $years = floor($diff / (365*60*60*24));
                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                            if($days >= 1){
                                if($days == 1){
                                    $age = $days . " day ago";
                                }else{
                                    $age = $days . " days ago";
                                }
                            }else{

                                $diff = strtotime($today_date) - strtotime($start_date);
                                $hours = floor($diff / ( 60 * 60 ));

                                if($hours >= 1){
                                    if($hours == 1){
                                        $age = $hours . " hour ago";
                                    }else{
                                        $age = $hours . " hours ago";
                                    }
                                }else{
                                    $age = "less than an hour ago";
                                }

                            }
                            if($months >= 1){
                                if($months == 1){
                                    $age = $months . " month ago";
                                }else{
                                    $age = $months . " months ago";
                                }
                            }
                            if($years >= 1){
                                if($years == 1){
                                    $age = $years . " year ago";
                                }else{
                                    $age = $years . " years ago";
                                }
                            }

                            $upvotes = Blogs::get_total_comments_likes($parent_comment_id);
                            $upvotes = $upvotes["COUNT(id)"];

                            if($upvotes == ""){
                                $upvotes = 0;
                            }

                            if($login){
                                $check_if_user_has_like = Blogs::get_comments_likes($parent_comment_id, "id_user = $user_id");

                                if($check_if_user_has_like){
                                    $vote_class = "fa-thumbs-up remove_like";
                                }else{
                                    $vote_class = "fa-thumbs-o-up add_like";
                                }

                                $attr = "id_comment=\"" . $parent_comment_id . "\"";

                            }else{
                                $vote_class = "fa-thumbs-o-up";
                                $attr = "";
                            }


                            ?>
                            <div class="comment" id="comment_<?= $parent_comment_id?>" id_comment="<?= $parent_comment_id?>">
                                <div class="disp_inline_flex">
                                    <div class="parent_comment_user">
                                        <div class="parent_comment_user_img" style="background-image: url('<?= $comment_user_img_path?>')"></div>
                                    </div>

                                    <div class="parent_comment">
                                        <div class="parent_comment_content"><?= $parent_comment["content"]?></div>
                                        <div class="details">
                                            <div class="username">by: <?= $comment_user_name?></div>
                                            <div class="date"><?= $age?></div>

                                            <?
                                            if($login){
                                                ?>
                                                <div class="upvotes" id_comment="<?= $parent_comment_id?>"><?= $upvotes?></div>
                                                <i class="fa <?= $vote_class?>" aria-hidden="true" <?= $attr?>></i>
                                                <?
                                            }else{
                                                ?>
                                                <div class="upvotes" style="right:5px;"><?= $upvotes?> upvotes</div>
                                                <?
                                            }
                                            ?>
                                        </div>

                                        <?
                                        if($login){
                                            if($parent_comment["id_user"] == $user_id){
                                                ?>
                                                <div class="delete_comment" id_comment="<?= $parent_comment_id?>">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                                <?
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                        ?>
                    </div>




                    <div id="target"></div>

                    <!--end of col-->
                    <!--end of col-->
                    <!--end of col-->
                </div>
            </div>

        </div>

    </div>
</div>

<div id="btn_open_sorting">
    <i class="fa fa-sliders" aria-hidden="true"></i>
</div>

<div id="sorting_bar">
    <div class="filters">

        <div class="each_filter" onclick="location.href='edit_submission?anime=<?= $id_url?>';">Submit Edit</div>

    </div>
</div>




<?
include 'fim.php';
?>



