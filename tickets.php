<?
include 'inicio.php';

if(Blogs::check_login() == false){
    alert("You need to be logged in");
    go_to("welcome?url=help");
}else{

    //details of user
    $cookie = $_COOKIE["anime_log"];
    $user = Blogs::get_users("key_log = '$cookie' AND status = '1'");

    $user_id = $user[0]["id"];
    $username = $user[0]["user"];

    if($username == "SEAFOREST95"){
        ?>
        <div class="fakebanner" style="background-image: url('images/kimi2.jpg')">
            <div class="active_label">Tickets</div>
        </div>

        <div class="bloco" id="tickets_page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="homepage_title">Tickets to be Handled</div>
                    </div>
                </div>
                <br>

                <div class="row list_table">
                    <table id="table_id_not_handled" data-order='[[ 0, "desc" ]]' class="table_id_datatables iris responsive">
                        <thead>
                            <tr>
                                <th class="disp_none">Prioritizer</th>
                                <th>Number</th>
                                <th>Urgency</th>
                                <th>Problem</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        <?
                        $get_tickets = Main::get("anime_tickets", "status = '0'");
                        foreach($get_tickets as $ticket){
                            $ticket_id = $ticket["id"];
                            $problem_urgency = $ticket["problem_urgency"];

                            if($problem_urgency == "Low"){
                                $prioritizer = 1;
                            }elseif($problem_urgency == "Medium"){
                                $prioritizer = 2;
                            }elseif($problem_urgency == "High"){
                                $prioritizer = 3;
                            }

                            $problem_title = $ticket["problem_title"];

                            if($problem_title == "Other..."){
                                $problem_title = $ticket["other_problem_title"];
                            }

                            $status = $ticket["status"];
                            $created_at = $ticket["created_at"];
                            ?>
                            <tr class="row_anime" ticket="<?= $ticket_id?>">
                                <td class="disp_none"><?= $ticket_id?></td>
                                <td><?= $ticket_id?></td>
                                <td><?= $problem_urgency?></td>
                                <td><?= $problem_title?></td>
                                <td><?= $status?></td>
                                <td><?= $created_at?></td>

                                <td class="actions no_hover">
                                    <a href="ticket?id=<?= $ticket_id?>">
                                        <i class="fa fa-plus-circle iris" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Open"></i>
                                </td>
                            </tr>
                            <?
                        }

                        ?>
                        </tbody>
                    </table>
                </div>

                <br>
                <br>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="homepage_title">Tickets already Handled</div>
                    </div>
                </div>
                <br>

                <div class="row list_table">
                    <table id="table_id_handled" data-order='[[ 0, "desc" ]]' class="table_id_datatables iris responsive">
                        <thead>
                            <tr>
                                <th class="disp_none">Prioritizer</th>
                                <th>Number</th>
                                <th>Urgency</th>
                                <th>Problem</th>
                                <th>Updated At</th>
                                <th>Updated By</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                        <?
                        $get_tickets = Main::get("anime_tickets", "status = '1'");
                        foreach($get_tickets as $ticket){
                            $ticket_id = $ticket["id"];
                            $problem_urgency = $ticket["problem_urgency"];

                            if($problem_urgency == "Low"){
                                $prioritizer = 1;
                            }elseif($problem_urgency == "Medium"){
                                $prioritizer = 2;
                            }elseif($problem_urgency == "High"){
                                $prioritizer = 3;
                            }

                            $problem_title = $ticket["problem_title"];

                            if($problem_title == "Other..."){
                                $problem_title = $ticket["other_problem_title"];
                            }

                            $updated_at = $ticket["updated_at"];
                            $updated_by = $ticket["updated_by"];
                            ?>
                            <tr class="row_anime" ticket="<?= $ticket_id?>">
                                <td class="disp_none"><?= $ticket_id?></td>
                                <td><?= $ticket_id?></td>
                                <td><?= $problem_urgency?></td>
                                <td><?= $problem_title?></td>
                                <td><?= $updated_at?></td>
                                <td><?= $updated_by?></td>

                                <td class="actions no_hover">
                                    <a href="ticket?id=<?= $ticket_id?>">
                                        <i class="fa fa-plus-circle iris" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Open"></i>
                                    </a>
                                </td>
                            </tr>
                            <?
                        }

                        ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <?
    }else{
        alert("Only who is worthy can enter...");
        go_to("home");
    }
}


include 'fim.php';
?>
