<?
include 'inicio.php';

if($_POST["form_json"]){
    $json = $_POST["json"];
    $json = json_decode($json);
    $array_of_fails = array();

    foreach ($json as $anime){
        $id = $anime->fake_id;
        $name = $anime->name->original;
        $start_date = $anime->start_date;

        if($start_date){
            $year = explode(" ", $start_date);
            $year = end($year);

            if($year >= 2000){
                $name = str_replace("'", "\'", $name);
                $mochi = Pages::get("name = '$name'");
                $mochi_id = $mochi[0]["id"];

                if($mochi_id){
                    $watch_type = $anime->personal_list->watch_type;
                    switch($watch_type){
                        case 1:
                            //watching
                            $watch_type = 1;
                            $last_ep_seen = $anime->personal_list->last_ep_seen;
                            break;
                        case 2:
                            //finished
                            $watch_type = 3;
                            $last_ep_seen = $mochi[0]["nb_eps"];
                            break;
                        case 3:
                            //interested in
                            $watch_type = 2;
                            $last_ep_seen = 0;
                            break;
                        case 5:
                            //on hold
                            $watch_type = 4;
                            $last_ep_seen = $anime->personal_list->last_ep_seen;
                            break;
                    }

                    if($last_ep_seen == "?" || $last_ep_seen == null){
                        $last_ep_seen = 0;
                    }

                    $score = $anime->personal_list->score;

                    $check_in_list = Main::get("blogs_users_animes", "id_user = 1 AND id_anime = $mochi_id");
                    if($check_in_list){
                        //already in my lists => update
                        $check_in_list_id = $check_in_list[0][1];

                        $fields = array(
                            "id_user" => 1,
                            "id_anime" => $mochi_id,
                            "id_list" => $watch_type,
                            "eps_seen" => $last_ep_seen,
                            "score" => $score,
                            "updated_at" => date("Y-m-d H:i:s"),
                        );
                        /*$update = Main::update("blogs_users_animes", $fields, $check_in_list_id);

                        if(!$update){
                            array_push($array_of_fails, $id . " == " . $name . " == " . $mochi_id . " == FAILED TO UPDATE");
                        }*/
                    }else{
                        //no in my lists => create

                        $fields = array(
                            "id_user" => 1,
                            "id_anime" => $mochi_id,
                            "id_list" => $watch_type,
                            "eps_seen" => $last_ep_seen,
                            "score" => $score,
                            "created_at" => date("Y-m-d H:i:s"),
                        );
                        /*$add = Main::add("blogs_users_animes", $fields, true);

                        if(!$add){
                            array_push($array_of_fails, $id . " == " . $name . " == " . $mochi_id . " == FAILED TO ADD");
                        }*/

                    }
                }else{
                    array_push($array_of_fails, $year . " == " . $name . " == DOESNT EXIST");
                }
            }else{
                array_push($array_of_fails, $year . " == " . $name . " == DOESNT EXIST");
            }
        }
    }
    asort($array_of_fails);
    debug($array_of_fails);
}



?>

<div class="bloco">
    <div class="container">

        <form id="form_json" method="post" action="">

            <input type="hidden" name="form_json" value="true"/>

            <div class="row">
                <div class="col-md-12">
                    <textarea rows="16" class="form-control" name="json" placeholder="JSON..."></textarea>
                </div>

                <div class="col-md-12">
                    <button type="submit">Import</button>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div id="response"></div>
                </div>
            </div>
        </form>

    </div>
</div>


<?
include 'fim.php';
?>



